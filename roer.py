# everything as constants, because that's easier
supertree = False
url = 'url'
author = 'author'
title = 'title'
license = 'license'
me = 'http://purl.org/net/jmora'
agg = 'aggregation'

outputfile = 'kyrieros.ttl'

# licenses
creative = 'http://creativecommons.org/licenses/by-nc-sa/2.0/'
lgpl = 'https://www.gnu.org/licenses/lgpl.html'
mit = 'http://opensource.org/licenses/MIT'
apache = 'http://www.apache.org/licenses/LICENSE-2.0.html'

# the Research Objects

analysis = {url: 'http://purl.org/net/jmora/mappings/fao/analysis', title: 'Efficient Inference-aware RDB2RDF Query Rewriting', agg: [], license: creative}
fanalysis = {url: 'http://purl.org/net/jmora/kyrie/mappings/analysis', title: 'Files for the paper Efficient Inference-aware RDB2RDF Query Rewriting', agg: [], license: creative}
optimisation = {url: 'http://purl.org/net/jmora/kyrie/optimisations/evaluation', title: 'Evaluation results for query rewriting', agg: [], license: creative}
benchmark = {url: 'http://purl.org/net/jmora/kyrie/benchmark', title: 'Benchmark results for query rewriting', agg: [], license: creative}
qanalysis = {url: 'http://purl.org/net/jmora/kyrie/queries/analysis', title: 'Analysis of the queries used in the query rewriting benchmark', agg: [], license: creative}
etest = {url: 'http://purl.org/net/jmora/kyrie/extensional/evaluation', title: 'EBox impact on Query Rewriting Evaluation', agg: [], license: creative}
kyrie = {url: 'http://github.com/jmora/kyrie', title: 'kyrie', agg: [], license: lgpl}
kyrie2 = {url: 'http://bitbucket.com/jmora/kyrie', title: 'kyrie', agg: [], license: lgpl}
kyriea = {url: 'http://github.com/jmora/kyrie-extra', title: 'kyrie ', agg: [], license: mit}
kyriea2 = {url: 'http://bitbucket.com/jmora/kyrie-extra', title: 'kyrie', agg: [], license: mit}

ros = [analysis, fanalysis, optimisation, benchmark, qanalysis, etest, kyrie, kyrie2, kyriea, kyriea2]

thesis = {url: 'http://purl.org/net/jmora/kyrie', title: 'Query Rewriting Optimisation Techniques for Ontology-Based Data Access', agg: ros[:], license: creative}

ros.append(thesis)


# those were the constants, now the templates

def header():
  return '''@prefix dc: <http://purl.org/dc/terms/> .
@prefix ore: <http://www.openarchives.org/ore/terms/> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix ro: <http://purl.org/wf4ever/ro#> .
@prefix schema: <http://schema.org/> .
@prefix xhv: <http://www.w3.org/1999/xhtml/vocab#> .
@base <http://purl.org/net/jmora/researchobjects> .
'''

def makero(ro):
  if supertree or ro[agg]:
    return '''
<{url}> a ro:ResearchObject;
\tdc:creator <{author}>;
\tdc:title "{title}"@en;
\tschema:creator <{author}>;
\tschema:name "{title}"@en;
\tore:aggregates <{aggregation}>;
\tore:isDescribedBy <>;
\txhv:license <{license}>;
\tprov:wasAttributedTo <{author}> .
'''.format(**ro)
  return '''
<{url}> a ro:Resource;
\tdc:creator <{author}>;
\tdc:title "{title}"@en;
\tschema:creator <{author}>;
\tschema:name "{title}"@en;
\txhv:license <{license}>;
\tprov:wasAttributedTo <{author}> .
'''.format(**ro)

def footer():
  return '''
<> dc:creator <{author}>;
\tschema:creator <{author}>;
\tprov:wasAttributedTo <{author}> .
'''.format(author = me)

def generation(ros):
  yield header()
  for ro in ros:
    yield makero(ro)
  yield footer()
  
def precompute(ros):
  for ro in ros:
    ro[author] = me
    if supertree or ro[agg]:
      ro['aggregation'] = '>,\n\t\t<'.join(map(lambda x: x[url], ro[agg]) if ro[agg] else [ro[url]])
  return ros

if __name__ == "__main__":
  with open(outputfile, 'w') as o:
    for l in generation(precompute(ros)):
      o.write(l)
  
