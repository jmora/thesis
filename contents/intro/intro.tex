
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Ontology Based Data Access (OBDA) is an information integration paradigm that consists on superimposing a conceptual layer as a view to an underlying information system.
This conceptual layer abstracts away from how that information is maintained in the data layer and provides inference capabilities.
In OBDA, the conceptual layer is represented using ontologies, while the data layer is commonly supported by relational databases \citep{calvanese2007-1}.

\begin{ocorchocut}
OBDA approaches present the advantage that users can pose queries that benefit from the expressiveness and inference provided by ontologies, while still relying on underlying systems that may be better suited to store large amounts of data (e.g. databases), to query real time data from sensors, to provide data by means of APIs, among other examples.
\end{ocorchocut}

\begin{jmoracut}
OBDA approaches present the advantage that users can pose queries that benefit from the expressiveness and inference provided by ontologies, while still relying on underlying systems that may be better suited to store large amounts of data (e.g. databases), obtain the information from other sources (e.g. mediators), query real time data from sensors, or may just want to be preserved as trustworthy legacy systems (e.g. banking).
\end{jmoracut}

\begin{forpony}
There are several advantages associated to OBDA approaches. Ontologies provide inference capabilities, normally with some limited expressiveness to keep the computational cost low.  Other data sources provide other capabilities depending on the type of the data source. Among these capabilities we can find efficient storage of big amounts of data (e.g. relational databases and NoSQL solutions \citep{cure2012,priyatna2013}), real time access to data (e.g. streams \citep{Calbimonte2010} and web services), or just a convenient way to store data in some files (e.g. XML and Excel spreadsheets).

Among these possibilities for data storage the one that has been used to a greater extent in the state of the art are relational databases. A proof of this is that OBDA stands for ontology-based data access and ontology-based database access in a interchangeably way in many works in the related literature. This is due to two different factors. The first one is their predominance as legacy data sources, OBDA enhances these legacy databases by adding an inference layer without altering the database and without breaking the compatibility with other legacy systems that may use these databases. The second factor is the efficiency of databases and their traditional convenience to use them to store materialized data to be accessed with OBDA.
\end{forpony}

\begin{noponieshere}
OBDA has been mainly applied to the combination of description logic TBoxes with relational databases, which are the predominant type of data sources.
Therefore, OBDA stands for ontology-based data access and ontology-based database access in a interchangeably way.
However, it is not uncommon to find other works focused on providing OBDA support for other types of data sources, such as data streams \citep{Calbimonte2010}, spreadsheets \citep{priyatna2013}, REST APIs, etc. 
\end{noponieshere}
\begin{ocorchocut}
In all these cases, mappings are commonly used to specify the relationships between the ontologies and the schema used by the underlying data sources.
These mappings are normally called database-to-ontology mappings, in general, or RDB2RDF mappings, when the resulting instances are transformed (in a materialized or virtual manner) to RDF.
In the latter case, R2RML \citep{das2012} is a W3C recommendation that allows defining such type of mappings.
These mappings are used in the \textit{query translation} stage of the query answering process, which is detailed later in this section.
\end{ocorchocut}
\begin{jmoracut}
In any case, regardless of the inference provided by the ontology, it is common to decouple the vocabularies in ontology and data sources for different reasons depending on the particular scenario which may range from convenient to necessary. This is not necessary in materialized solutions that can choose the schema for the accessed data or ontologies that are developed ad-hoc for the OBDA access to some data. However, ontologies are meant to be a an explicit specification of a shared conceptualization \citep{gruber1995}, therefore it is still convenient to allow the independent evolution of ontologies and the schema of the accessed data. This is specially important considering (a) the shared nature of ontologies, (b) that they can be linked to other ontologies (for enhanced inference capabilities), and (c) that linked ontologies may evolve independently of the OBDA system on the Internet.

This decoupling is usually done with an additional stage, besides of query rewriting, namely ontology translation. This translation uses a set of mappings that specify the relationships between the ontologies and the schema used by the underlying data sources. Traditionally the data sources have been databases, and thus these mappings are best known as database-to-ontology mappings, in general, or RDB2RDF mappings, when the resulting instances are transformed (in a materialized or virtual manner) to RDF. In the latter case, R2RML \citep{das2012} is a W3C recommendation that allows defining such type of mappings. 
\end{jmoracut}

OBDA has been traditionally used in information integration scenarios \citep{Wache2001,paton2000}, where the use of a global schema for information integration purposes favoured the adoption of ontologies in the role of this global schema.
By using an ontology as a global schema, information integration is empowered, due to the reusable and linked nature associated with ontologies.
\begin{jmoracut}
The use of ontologies in such a context paves the way for a global information integration scenario as Linked Data may be considered \citep{heath2011}, by adding explicit semantics to the data silos in the deep web, which allows the automatic reuse of the data contained in these silos.
\end{jmoracut}
Using ontologies for the global schema provides also additional query capabilities, allowing inferences on the query to provide richer answers, similarly to how deductive object-oriented databases work (as already pointed out in \citep{calvanese2007-1}).
In short we can say that in an OBDA scenario we use an ontology to rewrite a query into a rewritten query that can be used directly over the data source to obtain the results expanded with the expressiveness from the ontology.

However, expressiveness comes at a cost in terms of computational complexity, and thus OBDA has been traditionally an expensive feature which deemed only interesting when the challenges to tackle were on par with the cost of the solution.
% Here I "should" have written (to the end of the paragraph) "A greater expressiveness means that the resulting query will normally differ from the original query by producing a more complex query as the result of the query rewriting." I don't want to do that.
A greater expressiveness means that the resulting query will normally differ more from the original query.
This will usually have two consequences. 
First, more operations may need to be carried out in the rewriting process.
This means that the computational complexity of the rewriting is increased.
Second, a more complex query may be produced as the result of query rewriting.
This means that the underlying data management system will receive a more complex query that will be harder to process.

\begin{jmoracut}
In the next chapter we will see how early
\end{jmoracut}
\begin{ocorchocut}
Early
\end{ocorchocut}
approaches in OBDA focused on enabling access to data sources with the use of ontologies, drawing inspiration from expert systems and deductive databases.
The state of the art in OBDA changed with the first-order rewritability property of some languages.
Intuitively this property means that we can use an ontology to rewrite a query over this ontology and a data source into an equivalent first order query exclusively over the data source.
Here equivalence means that the answers returned are the same set of answers, namely the ``certain answers'' as we will see.
These first-order queries are specially interesting because they can be converted to first-order languages like SQL.
A more detailed explanation of this property is presented in section \ref{sec:qrgeneral}.

With this property, the focus was set on query rewriting as one of the key aspects in OBDA.
Including query rewriting as a standard part of OBDA allows separating the ontology semantics from the rest of the system, for instance the translation using mappings.
By using query rewriting, the query answering problem can be divided into several stages, as depicted in figure \ref{fig:wholeprocessfig}.
These stages are the following ones:

\begin{itemize}

\item{\textbf{Query rewriting}. In this stage, ontology-based queries are rewritten into queries that consider the inferences that can be done with the ontology.}

\item{\textbf{Query translation}. In this stage the previous queries are transformed into the query language (or API) that complies with the schema of the underlying  data sources, so that they can be evaluated by their corresponding query evaluation systems. This stage makes use of the aforementioned mappings.}

\item{\textbf{Query evaluation}. The generated queries or API calls are evaluated or executed, and results are obtained according to the underlying data source schema.}

\item{\textbf{Result translation}. The results obtained from the evaluation are translated into the original ontology-based schema, so that they can be interpreted by the original issuer of the ontology-based queries.}

\end{itemize}

\begin{figure}[!htb]%
\centering
\begin{tikzpicture}[node distance = 25mm and 35mm , thick, every node/.style={transform shape}, auto, on grid=true]
\node [block] (rewriting) {\textbf{\textsc{query rewriting}}};
\node [ontology, left= of rewriting] (ontology) {ontology};
\node [cloud, right= of rewriting] (rewrittenquery) {rewritten query};
\node [doc, below= of rewriting] (mappings) {Mappings};
\node [cloud, above= of rewriting] (query) {query};
\node [block, right= of mappings] (translation) {query translation};
\node [cloud, right= of translation] (translatedquery) {translated query};
\node [block, below= of translatedquery] (execution) {query execution};
\node [database, below= of execution] (database) {data source};
\node [cloud, below= of translation] (results) {results};
\node [block, below= of mappings] (translationres) {results translation};
\node [cloud, below= of translationres] (transresults) {translated results};
\path [line] (query) -- (rewriting);
\path [line] (ontology) -- (rewriting);
\path [line] (rewriting) -- (rewrittenquery);
\path [line] (rewrittenquery) -- (translation);
\path [line] (translation) -- (translatedquery);
\path [line] (translatedquery) -- (execution);
\path [line] (execution) -- (database);
\path [line] (database) -- (execution);
\path [line] (execution) -- (results);
\path [line] (results) -- (translationres);
\path [line] (translationres) -- (transresults);
\path [line] (mappings) -- (translation);
\path [line] (mappings) -- (translationres);
\end{tikzpicture}
\caption{OBDA process with the usual stages for query rewriting.}%
\label{fig:wholeprocessfig}%
\end{figure}

%\begin{ocorchocut}
%From a very general point of view we can see that the three main aspects of query rewriting that can be modified are the same that can be modified in any other process.
%As usual, the aspects that can be modified in query rewriting are: the input accepted, the output generated and the details of the process to obtain the output from the input.
% While this may seem basic and obvious, this will be also be useful for the analysis that will be performed in the latter chapters.
%We will consider these aspects into greater detail in later chapters.
%\end{ocorchocut}
%\begin{jmoracut}
%We can consider three main aspects in the analysis of a process: the input, the output and the process itself.
%An optimisation in the query rewriting process should improve the input accepted, the process itself or its results. Improving the process means reducing the required computational resources, like memory or number of threads, but the focus is normally set exclusively on the query rewriting time.  Improving the output of the process means producing some rewritten query that underlying systems can answer more easily. Whether a rewritten query is easy to answer depends on several characteristics. The characteristics that are normally considered can be divided in two main groups, those related with the size of the query and those related with the shape of the query. Most of the optimisations in query rewriting have improved the process or the results by restricting the input. The improvement in the process has usually been done in terms of the time required to obtain an answer. The improvement in the result usually occurs in terms of the number of Datalog clauses that compose the rewritten query. The restrictions in the input normally consist on restricting the ontological expressiveness that the system can handle, although some systems allow further optimisations if additional information is provided, for example this additional information may provide from the mappings.
%\end{jmoracut}

%\begin{jmoracut}
%We have described a query rewriting system as in an OBDA scenario as a system that produces a rewritten query $q_\Sigma$ from an ontology $\Sigma$ and a query $q$ to use it over the data source $D$, however an additional step may be needed. This ideal scenario assumes that the terms used in the data source are the same as in the ontology, which is the case if the data source or the ontology are designed ad-hoc for the system providing OBDA \citep{abadi2007,lubyte2009,sakr2010}. As more data sources are made available (specially in the context of open data movements) and ontologies are released on the wild, this OBDA scenario becomes less usual and less interesting
%when compared to reusing ontologies \citep{simperl2009} and providing a better access to open data as linked data \citep{heath2011}.
%Other authors opt for materializing data sources into databases or ABoxes \citep{kontchakov2010,rodriguez-muro2012}. This approach presents several inconveniences depending on the context, the most obvious are probably consistency preserving in non-static databases, storage of duplicated big data and latency introduced in stream processing. Thus materializing is an option out of the scope of the current work.
%
%Therefore we focus on the case where we can find a set of legacy data sources that already use some terminology, and a set of ontologies that are reused according to good practices in ontology engineering \citep{suarez-figueroa2012}. In this context, mappings are normally used by OBDA approaches to enable the translation from the ontological language to the data source language.
%\end{jmoracut}

% There may be latter stages, as query translation, that consider mappings and ontological terms, but these stages do not deal with axioms and inference.
In this context, the focus of most of the work done so far in OBDA was set on exploring the logic families that are FO-rewritable, the properties of these logics and the algorithms that could be built for them, leaving aside the rest of the OBDA system.
The evolution of this new branch in the state of the art was mostly qualitative, 
\begin{ocorchocut}
exploring what could be done with the different logics.
The focus was set on the theoretical complexity of the algorithms, with the efficiency in practice in a background role.
\end{ocorchocut}
\begin{jmoracut}
what is normal for the early stages of a new branch in research.
Due to the incipient maturity in the area, we dare to explore elements in a OBDA system that are related with query rewriting and to analyse query rewriting from a quantitative point of view.
\end{jmoracut}
Additionally the focus was set on query rewriting, as an isolated stage of the whole process.
We will see that considering other elements in the OBDA system can benefit the query rewriting process and its results.

\begin{comment}
This paper is structured as follows: In section \ref{sec:soa} we provide some background about the state of the art, identifying the main limitations and our proposed improvements. In section \ref{sec:algo} we introduce the algorithms involved in the proposed approach as a guiding structure for the optimisations presented.

In section \ref{sec:opt} we describe those optimisations that can be done in the query rewriting process without reducing the expressiveness from $\mathcal{ELHIO}^\neg$.

In section \ref{sec:map} we describe the optimisations that can be carried out in a wider scenario, having information about the mappings that exist between the ontology and the data sources.

In section \ref{sec:eva} we evaluate the results obtained. Finally, in section \ref{sec:con} we draw some conclusions from the work performed and outline some additional improvements that could be performed in the rewriting process.
\end{comment}

In particular, a fundamental aspect of any OBDA system are mappings.
Mappings are normally declarative and provide a description of the schemas that are mapped.
More precisely they map two schemata and describe one of them in terms of the other, as we will see in section \ref{sec:ultraback}.
This description provides valuable information about how rewritten queries may behave when posed to the underlying system.
With this we can perform further optimisations, as we will see in chapter \ref{sec:map}.
\begin{eboxdependent}
Moreover, additional information can be extracted from a set of mappings, such as an EBox.
An EBox describes the containment dependencies between the extension (in an ABox) of the predicates\footnote{We will refer to concepts and properties in a TBox as ``predicates'', for convention with other logics, e.g. first-order logic.} in a TBox.
Once an EBox has been described, the metainformation that it contains can be used for further optimisations, as we will see in chapter \ref{chp:ebox}.
\end{eboxdependent}


So far, query rewriting approaches have focused on the query rewriting stage, considering only the query and the ontology.
% seriously, if I keep rewriting things they are not going to be true anymore, more and more approaches are going to appear doing something like this.
% in short, it does not pay off trying to write something very properly when the problem is that it IS not very proper.
% better do proper things and write them in any reasonable way. The focus should be on the work, not on the writing after it, right?
In
\begin{jmoracut}
chapter \ref{sec:map}
\end{jmoracut}
\begin{ocorchocut}
our work
\end{ocorchocut}
we propose eliminating the predicates that are not mapped according to a set of mappings.
We perform this elimination first in the logic program and then in the Datalog program, reducing the time needed to process the queries in the following stages.
This reduction in the processing time is usually obtained through a reduction in the size of the queries in the stage where the optimisations are applied and in the following stages.

\begin{jmoracut}
In chapter \ref{sec:opt}
\end{jmoracut}
\begin{ocorchocut}
Besides,
\end{ocorchocut}
we perform some optimisations strictly from an engineering point of view.
The research in this case focuses on the algorithmic aspects of the process, mostly focusing on aspects of automated resolution.
As a consequence, the inputs and outputs to the process remain the same but the time needed to perform this process is significantly reduced.
This is described in chapter~\ref{sec:opt}.

\begin{eboxdependent}
Finally, 
in chapter \ref{chp:ebox}
we consider not only the mappings as in chapter \ref{sec:map}, but also the ABox dependencies contained in the EBox.
This allows eliminating predicates that are not mapped and predicates whose mappings or their corresponding values are contained in some other predicate already considered.
With this we can eliminate some clauses in the generated output, either Datalog or a union of conjunctive queries, without eliminating any answer to the query, as in previous optimisations.
\end{eboxdependent}

Contrarily to other approaches that preserve the structure defined in figure \ref{fig:wholeprocessfig} we will see how OBDA using query rewriting can be modified.
We will see this through the chapters and the variations of this figure in each chapter.
We will see that the modifications in the architecture require adaptations in the query rewriting step, presenting new challenges.
We will also see that with these challenges there are opportunities and room for improvement.
On the one hand we will be able to improve the query rewriting process, making it faster.
On the other hand we will be able to improve the rewritten queries, obtaining shorter queries that return the same set of answers.
With this, we have demonstrated the importance of considering the whole scenario of query rewriting and set the basis for a further exploration of the possibilities that arise in a broader picture.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
