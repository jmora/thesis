
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Evaluation for the optimisations in the presence of mappings} \label{sec:meva}

For the evaluation of the system two geographical ontologies have been used, both developed and used in the context of independent projects. The first ontology is \textit{hydrOntology} \citep{Vilches-Blazquez2007}, with 155 concepts and expressiveness $\mathcal{SHIN(D)}$. \textit{HydrOntology} has been used in several projects and mapped with several geographical databases of the Instituto Geogr\'{a}fico Nacional (IGN)\footnote{\url{http://www.ign.es}}, generating several RDB2RDF mapping files, which, being available, have been used for the tests. More precisely, the databases mapped here are three:

\begin{itemize}
	\item The National Atlas data source, with 32 mappings that provide 1,100 hydrographical instances for an overview of Spain's human and physical environment.
	\item The Numerical Cartographic Database (BCN200) with 57 mappings that provide 60,000 toponyms related to hydrographical instances.
	\item EuroGlobalMap (EGM), produced in cooperation with the National Mapping Agencies of Europe, with 32 mappings that provide 3,500 Spanish hydrographical toponyms.
\end{itemize}

The second ontology is \textit{PhenomenOntology} \citep{Gomez-Perez2008}. In this case among the phenomena that could be covered only the module regarding transportation networks has been used, which provides 66 concepts, having a common ancestor in \textit{``Red'' (``Network'')} which is the concept used for the test queries whose results are shown in table \ref{tab:results}. In the case of \textit{PhenomenOntology}, only one file with 18 mappings is used.

Both of these two ontologies are expressed in OWL, without imposing any kind of restriction, thus some of the axioms have to be discarded in the beginning of the process, as described in section \ref{sec:ontoprune}, as they do not fall into the $\elhio$ expressiveness.
The mapping files are R2O mappings \citep{barrasa2004}.

{%\renewcommand{\arraystretch}{1.5}
\renewcommand{\tabcolsep}{0.1cm}
\begin{table}[!htb]
%\rowcolors{1}{white}{lightgray}
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|}\hline
\multicolumn{2}{|l|}{Ontologies}&\multicolumn{7}{ c|}{HydrOntology}&\multicolumn{3}{ c|}{Phenomenontology} \\ \hline
\multicolumn{2}{|l|}{Information}&\multicolumn{7}{ c|}{686 clauses, 405 ignored statements}&\multicolumn{3}{ c|}{66 c., 114 i.s.} \\ \hline
\multicolumn{2}{|l|}{Mappings}&None&\multicolumn{2}{ c|}{BCN200}&\multicolumn{2}{ c|}{Atlas}&\multicolumn{2}{ c|}{EGM}&None&\multicolumn{2}{ c|}{Unique} \\ \hline
\multicolumn{2}{|l|}{Mappings \#}&0&\multicolumn{2}{ c|}{57}&\multicolumn{2}{ c|}{32}&\multicolumn{2}{ c|}{32}&0&\multicolumn{2}{ c|}{18} \\ \hline
\multicolumn{2}{|l|}{Prune method}&A&H&L&H&L&H&L&A&H&L \\ \hline
Mode&Phase&\multicolumn{10}{ c|}{Number of clauses generated after each phase} \\ \hline
\multirow{4}{*}{N}&Prune&535&323&445&287&415&336&429&66&66&66 \\ 
  &Saturation&412&25&50&17&24&19&31&66&60&60 \\ 
  &Unfolding&2333&25&46&17&22&19&28&64&14&14 \\ 
  &Prune&2333&25&46&17&22&19&28&64&14&14 \\ \hline
\multirow{4}{*}{G}&Prune&535&323&445&287&415&336&429&66&66&66 \\ 
  &Saturation&407&25&49&15&24&19&30&66&60&60 \\ 
  &Unfolding&894&25&45&15&22&19&27&64&14&14 \\ 
  &Prune&688&25&45&15&22&19&27&64&14&14 \\ \hline
\multirow{4}{*}{F}&Prune&535&323&445&287&415&336&429&66&66&66 \\ 
  &Saturation&407&25&49&15&24&19&30&66&60&60 \\ 
  &Unfolding&2245&25&45&15&22&19&27&64&14&14 \\ 
  &Prune&911&25&45&15&22&19&27&64&14&14 \\ \hline \hline
N&Total time (ms)&2735&172&625&156&359&250&625&17&15&15 \\ \hline
G&Total time (ms)&2250&172&512&156&360&218&532&31&16&16 \\ \hline
F&Total time (ms)&3719&172&531&156&344&235&531&31&16&16 \\ \hline
\end{tabular}
\smallskip
\caption{Results of the evaluation for the kyrie algorithm}
\label{tab:results}
\end{table}}

The results\footnote{Available at \url{purl.org/net/jmora/kyrie/mappings/analysis}} are summarised in table  \ref{tab:results}.
They vary depending on the method used.
The first letter is for the methods in the original REQUIEM, `N' for ``naive'', `F' for ``full forwarding'' and `G' for ``greedy'', the second letter stands for the modification applied as described in this paper, in this case `A' stands for ``all'', since all predicates are kept independently of the mappings.
`H' will stop the prune on the first predicate that is mapped (the highest), as all the values that are correct for that predicate are assumed to be retrievable from the RDB2RDF mappings, as described in section \ref{sec:ontoprune}.
Finally `L' will stop the prune at the ``lowest'' mapped predicate.
This last approach will keep all the mapped predicates in the ontology after pruning it, since they could be complementary to some extent, again as described in section \ref{sec:ontoprune}.

The query posed to the system for comparison purposes is a general query covering a good part of the ontology.
In the case of \textit{hydrOntology} the query is simply \(Q(x) \leftarrow Aguas(x)\), i.e. ``Water'', which covers as a superclass most of the taxonomy present in \textit{hydrOntology}.
Similarly, in the case of \textit{PhenomenOntology} the query used is \(Q(x) \leftarrow Red(x)\).
Being a module about transportation networks, the concept ``Red'' (``Network'') is the most general one.
Both queries cover most of the mappings and ontologies used for the tests, providing a good approximation of what could be expected in a different context.

As we can see in the table, the reduction in the number of classes (and hence in the number of queries submitted) is very important, especially in the case of Atlas, which has the same number of mappings than EGM but these allow less combinations in the rewritings. It is also noteworthy that the number of clauses is unaltered after the last prune phase, which has less clauses to prune due to the prunes performed previously.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
