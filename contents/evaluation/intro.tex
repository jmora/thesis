
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this chapter we analyse the dimensions and assets used in the evaluation of the systems in the state of the art.
Then we evaluate the hypotheses presented in section~\ref{sec:hypotheses}.

%\section{Analysis of dimensions and assets to be used for benchmarking} 

\section{Dimensions in query rewriting evaluation} \label{sec:hetero}  \label{sec:anal}

We will first analyse the main differences among different query rewriting approaches, so as to be able to obtain the main dimensions and challenges that should be considered when comparing among them:

\subsection{Determine the ontology language expressiveness}

We have already seen that query rewriting in an OBDA context uses at least a TBox $\tbox$ to transform a query $\query$ into a rewritten query $\query_\tbox$.
This operation normally uses Horn clauses to model both the TBox as a Datalog TBox $\dtbox$ and the query $\query$ as a conjunctive clause or a UCQ of clauses, with the usual rewritten query $\query_\dtbox$.
The main characteristics of this process are the computational cost of the rewriting and the complexity of the rewritten query, both of which depend on the expressiveness of the ontology.
We have also briefly described the main logics used in the state of the art in section \ref{sec:logics}.
Each logic has a different expressiveness, which determines the coverage of the ontology that can be performed when converting it from a description logic language into a set of clauses in Datalog, Datalog$\pm$ or FOL.
The conversion to FOL happens in Rapid, REQUIEM and kyrie.
Given the languages that they give support to, these systems consider expressions of the form $\exists A_1 \sqsubseteq R.A_2$.
These expressions generate Skolem functions, what implies the production of FOL clauses that are later processed and converted to Datalog.
Nyaya considers as well the existential quantifier in Datalog$\pm$.
 

\subsection{Characterise the impact of reduced expressiveness in each approach}
As discussed above, one of the main differences among systems is the expressiveness of the TBox $\tbox$ (or ontology $\ontology$) that can be handled by the approach in the conversion into a set of clauses\footnote{Note that other forms of knowledge representation could potentially be used but the state of the art so far focuses on Horn fragments in DL.} $\dtbox$.
In fact, it normally happens that some axioms of the original ontology may be even lost or discarded in this transformation process.
This can lead to unnecessarily complex queries, incomplete results and even incorrect results:

\begin{itemize}
	\item Some systems may not be able to handle the additional expressiveness, what means the loss of completeness {\wrt} that expressiveness and the loss of some answers.

	\item Non-recursive Datalog is a precondition for some systems.
 These systems may enter infinite loops or produce incomplete answers when the axioms in some ontology lead to the production of recursive Datalog.
For example, for certain ontologies, the rewriting process in Nyaya times out before producing some result for all the queries tested.
Other systems like Presto target non-recursive SQL rewritings, and therefore some answers may be lost in the process.
Finally, the remainder of the systems choose different strategies to preserve the recursive predicates, and in such cases the completeness of the answers depends on the capabilities of the underlying systems to process recursive queries. 

	\item The subsumption of some clauses may be implied by some axioms out of the handled expressiveness.
 Thus clauses that could have been eliminated upon checking that subsumption will be preserved in the rewritten query.
 This does not impact the correctness of the results, but it may imply longer and more redundant rewritings that may increase the load in underlying modules in the OBDA system.
 
	\item Negations (or disjunctions) are not used for query rewriting by any of the systems analysed.
This means that the rewritten query may contain clauses that are contradictory and will not obtain answers (they could be eliminated).
If the data source is not consistent with these negations then incorrect answers may be obtained, according to the semantics of the TBox.

\end{itemize}

The impact of the lost expressiveness has not been considered in previous evaluations.
 And to the best of our knowledge, there is no evaluation about which is the usual expressiveness in the ontologies that can be found and reused to enable OBDA.
 

\subsection{Determine the complexity of rewritten queries}
Evaluation of query rewriting systems has mainly focused so far on the most comparable cases: conjunctive queries and ontologies whose expressiveness is at the intersection of the expressiveness of all the systems to be compared.
Most systems allow choosing whether the output of the query rewriting process should be a Datalog program or a UCQ.
If Datalog is produced then this Datalog is always non-recursive for FOL-reducible logics.
If the expressiveness is in the aforementioned intersection of all systems, then rewritten queries are ensured to be complete and correct {\wrt} this expressiveness.
In this case the only difference that can be found among different UCQs, for the same ontology and query, is in terms of subsumed clauses or atoms that are not removed from the final result.
Datalog rewritings may vary more due to different ways in which subqueries can be arranged.
This heterogeneity makes the evaluation process significantly complex.


Furthermore, there is neither standard nor a set of tools to convert the Datalog or the UCQs obtained with these systems to actual queries to perform on a data source.
As we have seen in section \ref{sec:related} some systems are integrated in OBDA systems and some are not, but the modular design and implementation does not come with a modular evaluation that can compare them properly.
A workaround for this limitation has been proposed in the evaluation of Nyaya by measuring more carefully the UCQs generated (Datalog is not considered in this evaluation).
More precisely, they propose considering the number of clauses, the number of atoms and the number of joins.
However, it is not specified whether the number of atoms is the total number of atoms or the number of distinct atoms.
Atom repetition may have a big impact on the execution of the query depending on whether
1) the query translation uses some kind of temporary tables to store intermediate results or
2) repeated atoms are translated to as many queries as times they are related.
However, the differences among systems are small in the resulting UCQs due to the lack of freedom in this structure.
UCQs are flat and (unlike Datalog) the results are basically equal in all systems, unless some subsumed atoms or clauses are kept in the UCQ or there is some loss of completeness or correctness.
This should not be the case for any of these systems if the ontologies used do not exceed the expressiveness that they can handle.


\subsection{Determine the impact of the complexity of original queries} The behaviour of query rewriting systems may also vary greatly depending on the original queries that are posed to the systems.
 Among the main characteristics to be considered for queries we can cite:

\begin{itemize}
	\item length of the queries, i.e. predicates in the body of the query.
	\item types of variables and number of variables of each type.
 Variables may be present in queries in different roles:
	\begin{itemize}
		\item distinguished variables (those in the head of the query).
		\item existential variables (non distinguished variables that appear only in one predicate).
		\item join variables (non distinguished variables that appear in at least two predicates).
	\end{itemize}
	\item length of the property paths in the queries.
	\item hanging or closed property paths.
 Hanging property paths leads to existential variables, closed property paths lead to distinguished variables.
	\item separability of some parts of the query as independent subqueries.
\end{itemize}

A set of queries has traditionally been used for evaluations in the state of the art.
 However, this set of queries has not been adequately characterised in terms of their realism or the coverage of the casuistry.
 

\subsection{Usage of additional information for the query rewriting process}
Besides ontologies and queries, some systems add the possibility to use additional information for the query rewriting process (e.g., the use of an EBox in Prexto and {\kyriethree}).
Such additional information may provide potentially further optimisations of the process and results.
However, as such additional information may be very ad-hoc for each system, it is difficult to compare results among systems that use and do not use it.
Furthermore, in the case of EBoxes, for instance, their application in realistic scenarios is still largely unknown, what makes it harder to evaluate the impact of EBoxes in the rewriting (process and results).
 %Additional information can help to the evolution of the area, improvement of the results and avoid stagnation, however it requires estimations about how this information may be and its impact on the results.


%Finally the last group of difficulties in the evaluation of these systems resides in the evaluation of the complexity of the results.
The factors to consider for this matter are the \emph{characteristics of the rewritten queries} and how they relate with the \emph{characteristics of the system} that should process them and the actual data that is stored and queried.
There is neither a standard nor a set of tools to convert the Datalog or the UCQs obtained with these systems to actual queries to perform on a data source.
%As we have seen in section \ref{sec:related}, some systems are integrated in OBDA systems and some are not, but the modular design and implementation does not come with a modular evaluation that can compare properly these systems.
%A workaround for this limitation has been proposed in the evaluation of Nyaya by measuring more carefully the UCQs generated (Datalog is not considered in this evaluation).
%More precisely they propose considering the number of clauses, the number of atoms and the number of joins.
%However the differences among systems are small in the resulting UCQs due to the lack of freedom in this structure, UCQs are flat and unlike Datalog the results are basically equal in all systems, unless some subsumed atoms or clauses are kept in the UCQ or there is some loss of completeness or correctness, which is not the case for any of these systems if the ontologies used do not exceed the expressiveness that they can handle.


\section{Assets used for evaluations} \label{sec:ass}

Despite the limitations mentioned in the previous section, the need for a proper evaluation has been shaping a common framework for evaluation.
 This framework provides a set of ontologies and queries, but lacks other elements as an ABox to consider how different optimisations or differences in the queries could translate to answers and results.
 Moreover, to the best of our knowledge there are no estimations about the representativeness of the set of ontologies and queries {\wrt} the characteristics of actual OBDA systems as implemented in practice.

Despite the difficulties and challenges presented above, several approaches have been formulated for the comparison of query rewriting approaches.
However, there is a large heterogeneity in these approaches, what claims for the need to come up with a common evaluation framework to allow better comparisons and to provide sufficient information as well for system developers to improve their systems, as in any general benchmarking process.


Some of the seminal work can be attributed to the perfect reformulation proposal from Calvanese \citep{calvanese2007}, which is evaluated only in theoretical terms with respect to complexity, completeness and correctness.

%, which perfectly fits the game changer nature of the approach and the qualitative differences with the previous state of the art.
 
P\'erez-Urbina compared his approach with perfect reformulation by using a set of ontologies that have become common across evaluations in this area: 

\begin{itemize}
	\item Adolena (A).
 An ontology developed to allow OBDA for the South African National Accessibility Portal \citep{keet2008}.
 This is the largest ontology in this set of ontologies.

  \item path1 (P1) and path5 (P5).
 Two synthetic ontologies to help understand (and show) the ``impact of the reduction step'' in REQUIEM.
 Despite their apparent simplicity, rewriting times for Datalog in these ontologies are significant.

	\item StockExchange (S).
 A ontology that captures information about European Union financial institutions. This ontology has been later used \citep{rodriguez-muro2008} as a driving example to explain OBDA and how users may benefit from it.

	\item University (U).
A DL-Lite$_R$ version of LUBM \citep{guo2005}.
LUBM focuses on the ABox more than the TBox.
On the one hand this allows systems like Clipper to use the ABox for further evaluation of rewritten queries.
On the other hand it has a rather flat TBox \citep{rodriguez-muro2012}, what means that the rewritings are simple.
Simple rewritings are produced in short times and the rewritten queries are also short, as we will see in the next section.

	\item Vicodi (V).
An ontology about European history developed in the EU-funded VICODI project \citep{nagypal2005}.

\end{itemize}

This set of ontologies was expanded with AX, P5X, and UX, where some of the previous ontologies included auxiliary predicates.
These auxiliary predicates replace the existential predicates by applying the encoding required by the previous approach \citep{calvanese2007}.
This encoding significantly increases the size of the ontologies and the time required for the rewriting, as the data in the evaluation reflects.

All previous ontologies are accompanied by a set of five queries for each of them.
 

For the evaluation of Presto these sets of queries are incremented up to seven queries for each of the ontologies.
The ontologies are also incremented with the ontologies from Kontchakov \citep{kontchakov2009}, more examples from the LUBM benchmark (besides of U and UX) and a newly created ontology.
The ontologies from Kontchakov are:

\begin{itemize}
	\item Galen-Lite.
 The \textit{DL-Lite}$_{core}$ approximation of the well-known medical ontology Galen \citep{rogers1996}.
 The interest in this ontology is mainly in its taxonomy, where few axioms involve roles.

	\item Core.
 A \textit{DL-Lite}$_{core}$ representation of (a fragment of) a supply-chain management system used by the bookstore chain Ottakar's, now rebranded as Waterstone's.
 Contrary to Galen, the taxonomy in Core is small but it contains a rich set of axioms about roles.

\end{itemize}

Later approaches like Rapid, Nyaya, Clipper and kyrie use the aforementioned ontologies and queries, and keep on using them for evaluation purposes.
Prexto is only compared with Presto by means of a small unnamed ontology to show the impact that the optimisations in Prexto may have, which is done more as an example than as an empirical evaluation.
In the case of kyrie some newly created ontologies named AXE, AXEb, P5XE and UXE are used.
These ontologies expand AX, P5X and UXE by including additional axioms that fall in the expressiveness of $\elhio$, which is not covered in less expressive systems.
Again the purpose of this evaluation is showing how these axioms may impact results, regardless of realism or empirical significance, specially by comparing the performance with REQUIEM wich can handle the {\elhio} expressiveness.

Upon a closer analysis of the ontologies and the queries we can see that they are fairly heterogeneous.
For example rewritings may vary greatly in time and size, depending on the characteristics of the ontology and the query as we have mentioned in the previous section.
Finally, there is not a well-founded analysis of whether these ontologies are representative enough to cover all the challenges and characteristics that we have discussed in the previous section.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
