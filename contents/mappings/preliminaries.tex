
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Preliminaries}\label{sec:mappre}

In this section we introduce some notions that will be useful to explain the contents of the chapter as well as future chapters.


\begin{comment}
Formally, we assume that a clause $\clause$ can only be translated if all its predicates are mapped according to some set of mappings
$\mappings$, simply:
$$translatable(\clause) \iff \forall \predicate \in \clause . \predicate \in mappedPredicates(\mappings).$$
With $q_{\dtbox,\mappings}$ as the translation of the UCQ $q_\dtbox$ using the set of mappings $\mappings$, we name the set of translatable clauses in $q_\dtbox$ as:
$$t(q_\dtbox) = \set{\clause \in q_\dtbox \mid translatable(\clause)}$$
For a given query translation mechanism ``$translation$'', these translatable clauses are the only ones that will be translated and thus the only ones that have impact on the translated query:
$$q_{\dtbox,\mappings} = translation(q_\dtbox) = translation(t(q_\dtbox))$$
% This ``translation'' mechanism is dependent on the mapping language and not in the scope of this work.
Therefore, for our purposes, considering the set of predicates in the ontology that are mapped is enough to determine whether a specific clause in the rewriting will be translatable or not.
Knowing the clauses that are translatable allows targeting the translatable set of clauses in the rewriting and optimising the query rewriting process for the generation of the translatable clauses.
\end{comment}

\begin{definition}\label{prop:clausecontributions}
$\contrib{\obda}{\clause}$ \textbf{Contributions of a clause $\clause$ in an OBDA system $\obdadef$}.
Let $\predicate$ be the predicate in the head of $\clause$, we define the contributions of $\clause$ on $\obda$ as the set: \[
\contrib{\obda}{\clause} = \set{\tuple{\constant} \mid \exists \unifier{}.\contribsset{\clause}}
\] Where $\unifier{}$ is a substitution of the variables in $\clause$ with the constants $\tuple{\constant}$.
Please note that $\obda \models \unifier{\body{\clause}}$ means that $\obdaunion \models \unifier{\body{\clause}}$, i.e. the values for the contribution may be implied by other clauses in $\dtbox$.
\end{definition}

\begin{comment}
% We may consider the mappings to be the ``translational'' part of the OBDA system, analogously to the previously mentioned intensional and extensional parts. If the predicates in $\dtbox$ and $\data$ correspond in an immediate way then no mappings are needed and the set of mappings is empty. For brevity we may group the intensional, extensional and translational parts of some OBDA system into a context to which we will usually refer as $\obda$.
\begin{definition}\label{prop:predvalues}
For a given OBDA system $\obdadef$, the \textbf{values for a predicate} $\predicate$ will be $\values{\obda}{\predicate} = \set{\tuple{\constant} \mid \obda \models \predicate(\tuple{\constant})}$. 
%A predicate $\predicate \in \dtbox$ will take the values \draft{I'm not completely happy about the definitions of extensional definition and intensional definition, which are both mine completely, they may change in a near future, suggestions are more than welcome} $\contrib^e_i$ in its extensional definition $e{\predicate} = \bigcup_i \contrib^e_i$ through the mappings $m_j$ iff that the translation $\tau$ for the predicate $\predicate$ produces $\contrib^p_e$, in short $\tau(m_j, \data, \predicate) \leadsto \predicate(\contrib^e_i)$, and $\data \cup \mappings \models \predicate(\contrib^e_i)$. A predicate $\predicate \in \dtbox$ will take the values $\contrib^\clause_i$ in its intensional definition $i{\predicate} = \bigcup_i \contrib^\clause_i$ iff there is some clause $c_j$ such that $\predicate = head(c_j) \land \dtbox \cup \data \cup \mappings \models \predicate(\contrib^\clause_i) \leftarrow body(c_j)$.
\begin{jmoracut}
For the sake of simplicity we may omit the reference to the mappings and refer to the OBDA system as $\langle \dtbox, \data\rangle$.
\end{jmoracut}
% A predicate $\predicate \in \dtbox$ will take the values $\values_{\obda}{\predicate} = \set{\contrib | \obda \models \predicate(\contrib)}$.
In our context for a given OBDA system $\values{\obdatriple}{\predicate} = \set{\tuple{\constant} \mid \obdatriple \models \predicate(\tuple{\constant})}$.
\end{definition}
\begin{definition}\label{prop:evaluesivalues}
We can analogously define the \textbf{extensional and intensional values for a predicate} for a given OBDA system $\obdadef$ and predicate $\predicate$, respectively as $\evalues{\obda}{\predicate}$ and $\ivalues{\obda}{\predicate}$.
These values will satisfy the property that $\values{\obda}{\predicate} = \evalues{\obda}{\predicate} \cup \ivalues{\obda}{\predicate}$.
In some cases it is assumed that for every predicate $\predicate$ either $\evalues{\obda}{\predicate}  = \emptyset$ or $\ivalues{\obda}{\predicate} = \emptyset$.
We do not impose that constraint here, not only both of these sets may not be empty but also they may overlap, i.e. possibly $\exists \predicate. \evalues{\obda}{\predicate} \cap \evalues{\obda}{\predicate} \neq \emptyset$.
These two sets of values can be defined respectively as:

\[ \evalues{\obdatriple}{\predicate} = \set{\tuple{\constant} \mid \data \cup \mappings \models \predicate(\tuple{\constant})}\] 
\[ \ivalues{\obdatriple}{\predicate} = \set{\tuple{\constant} \mid \exists \unifier{}, \clause \in \dtbox, \tuple{\constant}_1 \in \values{\obdatriple}{\predicate_1}, \ldots, \tuple{\constant}_n \in \values{\obdatriple}{\predicate}. \unifier{\clause} = \paren{\predicate(\tuple{\constant}) \leftarrow \predicate_1(\tuple{\constant}_1) \land \ldots \land \predicate_n(\tuple{\constant}_n)} }\]

Where $\unifier{}$ is the most general unifier (MGU) applied to $\clause$, from the variables in $\clause$ to the values in $\tuple{\constant}, \tuple{\constant}_1, \ldots, \tuple{\constant}_n$.

For any predicate $\predicate$ in a OBDA system $\obdadef$ such that $\predicate$ has no intensional definition in the program $\dtbox$ we have that its values $\values{}{}$ over a data source $\data$ are $\values{\obdatriple}{\predicate} = \evalues{\data}{\predicate}$.
That is, there is no rule $\clause$ in $\dtbox$ with $\predicate$ in the head to derive more values and thus $\ivalues{\obdatriple}{\predicate}= \emptyset$.
\end{definition}


%For the sake of readability when the context is clear it may be omitted and $\contrib_{\langle \dtbox, \data, \mappings \rangle}(\clause)$ may be shortened to $\contrib(\clause)$.
From the previous definitions we can see that $\forall \obda. \forall \clause \in \obda. \contrib{\obda}{\clause} \subseteq \values{\obda}(head(\clause))$ and also $\forall \obda. \forall \predicate \in head(\clause) \in \obda. \ivalues{\obda}{\predicate} = \bigcup_i \contrib{\obda}{\clause_i} \forall \clause_i \in \set{ \clause_i \in \obda \mid \phead{\clause_i} = \predicate }$ or otherwise $\forall \obda. \forall \phead{\clause}, \clause \in \obda. \forall \tuple{\constant} \in \ivalues{\obda}{\predicate}. \exists \clause \in \obda. \tuple{\constant} \in \contrib{\obda}{\clause}$. Finally we can obviously see that for any pair of clauses $\clause_a: A(\tuple{x}) \leftarrow \atom_1(\tuple{x_1}) \land \ldots \land \atom_n(\tuple{x_n}) \land \clause_1(\tuple{x_{n+1}}) \ldots \land \clause_m(\tuple{x_{n+m}}))$ and $\clause_b: A(\tuple{x}) \leftarrow \atom_1(\tuple{x_1}) \land \ldots \land \atom_n(\tuple{x_n}) \land \data_1(\tuple{x_{n+1}}) \ldots \land \data_l(\tuple{x_{n+l}}))$ we have that $\forall \obda \contrib{\obda}(\clause_a) \subseteq \contrib{\obda}{\clause_b} \iff \set{\tuple{\constant}_a \mid \obda \models \clause_a\unifier{}_a} \subseteq \set{\tuple{\constant}_b \mid \obda \models \unifier{}_b\clause_b} \iff \set{\tuple{\constant}_a \mid \obda \models \unifier{}_c (\clause_1(\tuple{x_1}) \land \ldots \land \clause_m(\tuple{x_m}))} \subseteq \set{\tuple{\constant}_d \mid \obda \models (\data_1(\tuple{x_1}) \land \ldots \land \data_l(\tuple{x_l}))\unifier{}_d}$ where the different $\unifier{}_i$ are MGUs from the $\tuple{x_i}$ variables referred to the corresponding $\tuple{\constant_i}$.
\end{comment}


\begin{definition}\label{prop:predicatevalues}
$\values{\obda}{\predicate}$ \textbf{Values for a predicate $\predicate$ on $\obda$}.
For a given OBDA system $\obdadef$, we define as the values for a predicate $\predicate$ in $\obda$ as the set: \[\values{\obda}{\predicate} = \set{\tuple{\constant} \mid \obda \models \predicate(\tuple{\constant})}\]
% Where $\dtbox$ is a set of clauses derived from the ontology $\ontology$ and $\dabox$ is the ABox as obtained from a data source $\data$ through a set of mappings $\mappings$ (as defined in \cref{sec:detailedobda}).

Moreover, the values for a predicate $\predicate$ on $\obda$, i.e. $\values{\obda}{\predicate}$, are divided into the extensional values $\evalues{\obda}{\predicate}$ and the intensional values $\ivalues{\obda}{\predicate}$, so that $
\values{\obda}{\predicate} = \evalues{\obda}{\predicate} \cup \ivalues{\obda}{\predicate}
$, defined as: \[
\evalues\obda{\predicate} = \set{\tuple{\constant} \mid \vabox \models \predicate(\tuple{\constant}) }
\]  \[
\ivalues{\obda}{\predicate} = \set{
\tuple{\constant} \mid
\exists \clause, \unifier{}. \clause \in \dtbox \land \unifier{\head{\clause}} = \predicate(\tuple{\constant}) \land \tuple{\constant} \in \contrib{\obda}{\clause}
}
\]
Where $\unifier{}$ is the most general unifier (MGU) applied to $\head{\clause}$, from the variables in $\clause$ to the constants in $\tuple{\constant}$.
\end{definition}

Intuitively, the intensional values for a predicate $\predicate$ are the contributions of the clauses where $\predicate$ is in the head, while the contributions of a clause are a projection and selection of the values for the predicates in its body.
Note that the intersection between the extensional and the intensional values of a predicate may not be empty.

Let us consider an example composed uniquely of the following two clauses:
\begin{itemize}
\item $\clause_1$: \code{Professor(x) :- AssociateProfessor(x)}, and 
\item $\clause_1$: \code{Professor(x) :- AssistantProfessor(x)}.
\end{itemize}
We may have the following facts in our ABox ($\vaboxdef$):
\begin{itemize}
\item \code{AssociateProfessor(\person{1})}, \code{AssociateProfessor({\person 2})},
\item \code{AssistantProfessor(\person{3})},
\item \code{Professor(\person{1})}, \code{Professor(\person{3})}, and \code{Professor(\person{4})}.
\end{itemize}
In this example:
\begin{enumerate}
	\item The extensional values for \code{Professor} on $\obda$ are: $\evalues{\obda}{Professor}$ = \code{\person{1}}, \code{\person{3}} and \code{\person{4}}.
  \item The intensional values for \code{Professor} on $\obda$ are: $\ivalues{\obda}{Professor}$ = \code{\person{1}}, \code{\person{2}} and \code{\person{3}}.
  \item The values for the predicate $\values{\obda}{Professor}$ are the union of both sets: \code{\person{1}}, \code{\person{2}}, \code{\person{3}} and \code{\person{4}}.
  \item The contributions from clause $\clause_1$, i.e. $\contrib{\obda}{\clause_1}$, are \code{\person{1}} and \code{\person{2}}.
  \item The contributions from clause $\clause_2$, i.e. $\contrib{\obda}{\clause_2}$, are just one: \code{\person{3}}.
\end{enumerate}

Note that the intensional values of a predicate $\predicate$ are defined according to the contributions of the clauses that have that $\predicate$ in their heads.
At the same time, the contributions of a clause depend on the values of the predicates in its body.
More precisely, we can say that the contributions of a clause $\clause$ are a \emph{projection} to the variables in the head of the clause of the \emph{product} of the values of the predicates in the body of that clause $\clause$.
This consideration can be made from a general mathematical perspective or from a relational algebra perspective and it is just the normal semantics for Datalog and logic~\citep{abiteboul1995}.
However, this is worth noting for a more clear understanding of how the contributions of clauses and the values for predicates (both in an OBDA system) relate to each other.
