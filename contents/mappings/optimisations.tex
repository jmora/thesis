
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Optimisations}

In this section we describe in more detail the mapping-based optimisations that can be applied to query rewriting algorithms.
In particular, we focus on how these optimisations work in the kyrie system, which at this point differs from REQUIEM mainly on these optimisations.

\subsection{Mapping-based reachability}\label{sec:ontoprune}

Reachability usually refers to the predicates in the ontology that are reachable (in a backward chaining fashion) from some given query $\query$.
Predicates that are reachable can potentially participate in the answers for $\query$.
When considering reachability and the description of some mappings we are going to focus on the reachability of the mappings and thus the instances (results) provided by them.
The reason for this is that predicates that are not mapped cannot provide any results in an OBDA scenario since results are actually provided by the evaluation of the mappings.
If a predicate is not mapped then any clause containing that predicate will not participate in the answers of the query.

We consider reachability in two different manners depending on the type of mappings that are considered for the system, i.e. whether they are complete or not.
We consider a mapping for a concept to be complete if it provides all individuals for a concept and its subconcepts.
\begin{itemize}
  \item If mappings are complete (corresponding to a virtual complete ABox) then the set of individuals $\value_i$ provided by any mapping $\mapping_i$ that is reachable from another one $\mapping_j$ will be contained ($\value_i \subseteq \value_j$) in the set of individuals $\value_j$ provided by the latter mapping $\mapping_j$.
        Therefore, the search for reachable mappings ends as soon as a mapped predicate is found. %, the search ends with the first mapped predicate.
  \item If mappings are not guaranteed to be complete, then all reachable mappings could potentially add answers to the query and all of them should be included.
        Therefore the reachability should explore clauses (exactly) up to the last mapped predicate.
\end{itemize}

These two different ways in which mappings can be used are respectively explained in algorithms \ref{alg:kreados} and \ref{alg:krea}.
Intuitively, the reachability testing algorithm constructs a tree of reachable predicates in a backward chaining fashion.
The root of this tree is the query predicate, and it expands as we visit the predicates that appear in the body of clauses containing previously visited predicates.
Considering such a tree, both variants of the reachability testing algorithm will still preserve some predicates that are not mapped.
The H (to the highest, more strict) variant of the reachability test will preserve non-mapped predicates between the query and the first mapped predicate.
The L (to the lowest, more inclusive) variant will preserve the predicates between the query and the last mapped predicate.
These predicates need to be preserved for the next resolution stage, and will be eliminated afterwards, as explained in the next section.


\subsection{Resolution with mappings}\label{sec:rkmap}

Normally, query rewriting systems may produce a Datalog program or a UCQ.
This Datalog program will generally include predicates that are not mapped, despite the removal of some of these predicates with the reachability test, in the case of kyrie.
These predicates are preserved due to the clauses that they participate in and the inferences that are possible with those clauses.
Therefore, we can intuitively see that if we perform those inferences then we will be able to remove the clauses that participate in them, as they will be redundant. 
By removing these clauses, the predicates that are not mapped will be removed with them.

To do this, we use a selection function such that (1) body atoms are selected if and only if they are not mapped and (2) head atoms are selected if they are not mapped and no body predicate is selected.
This resolution could generate an infinite set of clauses if the predicates that are not mapped form a cycle and generate new clauses.
To prevent these cycles we mark as mapped some predicate for each cycle.
This is done heuristically selecting first the predicates that are in several cycles to minimise the set of predicates that are marked as mapped.
Predicates that are not auxiliary are prioritised to be preserved, as specified in algorithm \ref{alg:findRecursiveMaps}.
This keeps some predicates that are not mapped in the Datalog and prevents an infinite inference by breaking the cycles formed by non-mapped predicates.

For example, if the predicate \code{Person} is not mapped then we can perform the following inference:

\begin{center}
\centering\small
\begin{tabular}{r l}
\code{Q(x)}&\code{ <-  Person(x)}\\
\code{Person(x)}&\code{ <-  Student(x)}\\
\(\therefore\) \code{Q(x)}&\code{ <-  Student(x)}
\end{tabular}
\end{center}

And after this inference we can remove both premises and keep the consequent if this has no impact on the reachability of other mapped predicates that may provide additional answers.
Intuitively, we can consider for correctness the two steps involved in this stage.
First, the deduced clauses are added, the deduction is correct and adding the deduced clauses is also correct according to \citep{pettorossi1994} (R1 - Unfolding).
% \margincomment{pettorossi es interesante, lo mismo habria que mencionar algo de esto en la seccion de background, porque es parecido a lo que hacemos en oposicion de lo que hacen en rapid, presto, nyaya, etc.}
Once these clauses have been added we can see that the values for the head predicate (\code{\q}) in the first clause are the same as in the third plus the extensional values for \code{Person(x)}.
However, if \code{Person} is not mapped then these values are the empty set and thus the third clause provides the same values as the first clause.
% This redundancy means by proposition \ref{lem:irrelevance}???? that we can remove either one of those two clauses.
Therefore, either one of those two clauses can be removed.
If we remove the first one and do so for every clause containing \code{Person} in the body then we can remove all predicates with \code{Person} as the head predicate, according to \citep{pettorossi1994} (R5 - Definition Elimination).

The efficiency gain for this kind of resolution is evaluated in section \ref{sec:meva}.

\begin{comment}
In the previous stage some non mapped predicates have been kept in the head of the clauses, as well as some recursive predicates that may no longer be recursive due to the removal of functional terms for the production of the Datalog program. With the introduction of the query some non mapped predicates may be found in the body of the query clauses. Resolution can be used to remove these predicates as far as it is possible to avoid infinite cycles, as the ones referred to in section \ref{sec:pmap}. In this stage, after saturation has been performed and we have a Datalog program, there are no functional symbols, thus the type of infinite cycles that can appear in this context is only the first type previously mentioned, the introduction of new individuals due to free variables.

To remove non mapped predicates in this Datalog program we propose a new resolution stage, again with query and ontology clauses separated, with a very simple selection function. In this case, body predicates are selected in the query clauses and head predicates are selected in ontology clauses iff they are not mapped, no other predicates are selected. Again infinite cycles are avoided if present by choosing one non mapped predicate in each cycle and not selecting that predicate for resolution.

This way, every inference made will remove one unmapped atom in the body of a clause, unifying it with the head of another clause, whose body will contain only mapped predicates, thus generating a clause that has at least one unmapped atom less than the base clause of the inference. After saturation has been performed using this selection function and no new clauses can be generated the clauses that contain unmapped predicates can be removed safely, without removing any valid answer with them.

Intuitively, we can simply see that for a given Datalog query $\query_\dtbox$ if some predicate $\predicate$ in the body of some clause $\clause$ in the Datalog query $\clause \in \query_\dtbox$ is not mapped then all values of $\predicate$ are intensional, corresponding to the contributions of some clause.
Therefore, so we can replace the appearances of $\predicate$ in the bodies in which it appears with the bodies of the clauses that have $\predicate$ as their head predicate, and then remove all clauses that contain $\predicate$.
??? proof in propositions
% $\forall \varphi \in \Phi^{c_i}_{\Sigma} \exists c_j \in \Sigma$ such that $asdfgasdfg$ where $\predicate$ is the head predicate, $\forall a \in ans(p, \mathcal{P}). \exists c \in \mathcal{P}. a \in ans(p_i, \mathcal{P}) \forall p_i \in body(c)$.
Since predicates that are not mapped are intensional and not extensional, these predicates cannot be translated nor contribute with answers on their own.
Removing non mapped predicates is safe if the inferences that can be done with them are preserved, performing these inferences before removing them guarantees these inferences will not be missed after the removal of these predicates, as proposition ??? proves. \draft{tengo que escribir la proposition 1.}

In this stage we can find an example equivalent to the one presented for the previous stage, in this case:

\begin{center}
\small
\begin{tabular}{r l}
\code{Q(x)}&\code{ <-  Person(x), cures(y, x)}\\
\code{Person(x)}&\code{ <-  Cutaneous(y), isAppliedBy(y, x)}\\
\(\therefore\) \code{Q(x)}&\code{ <-  Cutaneous(y), cures(z,x), isAppliedBy(y, x)}
\end{tabular}
\end{center}


In the previous example we obtained as a result the removal of the first clause, in this example the first clause is replaced by a query clause, which is new and may contain non mapped predicates in its body, as it is the case in this example. After all inferences enabled by the previously specified selection function have been performed both clauses, 1 and 2, can be removed, since the third clause provides all the answers that both previous clauses could provide.

This stage is proposed in \kyrietwo as an independent resolution stage, performed over the preliminary Datalog program to obtain a Datalog program composed solely on mapped predicates and potentially some non mapped predicates required to preserve some cycle, and prevent information loss, in the case of recursive Datalog.
\end{comment}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
