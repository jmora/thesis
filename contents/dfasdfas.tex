
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Query subsumption check}\label{sec:sub}

Subsumption checks consist on checking whether a part of the query that is being rewritten is subsumed by another.
When this happens one of the two parts can be removed, resulting in a shorter query. Subsumption checks are usually performed by checking pairs of clauses, checking whether one subsumes the other and removing the clause that is subsumed.
Another usual optimisation operation, clause condensation, is performed by checking intra-clause subsumption, that is whether an atom subsumes another.
In this case the subsuming atom is removed, since they are grouped by conjunctions, as opposed to the disjunctions that we find between clauses.

In the case of atom subsumption all previous approaches perform a similar check.
REQUIEM checks atom subsumption in the \texttt{condensation} optimisation. In the case of Rapid this is done with the \textit{shrinking} resolution rule.
Nyaya does something similar with the factorization step. Finally, Presto has the function \texttt{DeleteRedundantAtoms}.%, naturally with a properly descriptive name. 

With respect to the clause subsumption check, REQUIEM performs this in a separate stage after the resolution has finished by checking all clauses with a complexity of $O(n^{2})$, when using the ``F'' mode, REQUIEM does also perform a ``full'' subsumption check as described in \citep{bachmair2001}, this means that during resolution newly derived resolvents may be deleted if they are subsumed by old or processed clauses.
However, the subsumption check is not performed the other way around with respect to old and new clauses.
Rapid performs a similar subsumption check, but the generation of subsumed clauses is reduced and more controlled, which allows limiting the check to subsets of the generated clauses.
This check is performed after each unfolding step.
In the case of Nyaya, subsumed clauses are removed with the elimination step, which is optionally performed after every rewriting step.
Presto produces a factorized datalog where subsumption check becomes less tractable, and thus there is no subsumption check between clauses.
However, subsumption between sets of atoms that share some variable is considered with the \textit{most general subsumees}.

We propose an optimisation that consists in performing the subsumption check among all clauses generated, as in REQUIEM, but instead of doing this as a separate stage from resolution, we add subsumption check as part of the resolution process (line \ref{ln:subsumptionLoop} in algorithm \ref{alg:resolii}).
In our context the clever handling of the unfolding sets done in Rapid cannot be added in a straightforward way, since the datalog used is not linear, what means that unfolding sets are not composed of atoms but of conjunctions of atoms.
In our case, new clauses are generated using resolution with free selection and every time a new clause is generated its subsumption is checked and thus subsumed clauses are identified --- and removed --- as soon as a pair (subsuming-subsumed) is generated.

This allows finding clauses that are equivalent and processing them only once. The subsumed clause can be removed and to avoid all the inferences it would take part in the resolution with free selection, removing all the tree of clauses that could be inferred by using that clause and its successors. As a result, all later stages have a lesser load and require less time, including this subsumption check, which in this case checks less clauses since the number of clauses generated is reduced by removing subsumed clauses as soon as possible.

Obviously by preserving satisfiability we preserve the certain answers for that logic program.
These additional subsumption checks that can be performed during resolution can be performed in any resolution stage.
Therefore these subsumption checks can be performed in the preprocessing.
Again the sooner the better, but in this case there is an additional advantage: once the query is available, in the latter resolution process (the base clause is always the query) we know that the head of the generated clauses always belongs to the query.
Therefore subsumption check between clauses can be further restricted to query clauses, excluding ontology clauses in the latter resolution stages where ontology and query clauses are processed separately.

For example given the query \code{Q(?0) <- Course(?0), ImpartedCourse(?0)} and the axiom \code{Course(?0) <- ImpartedCourse(?0)} we obtain \code{Q(?0) <- ImpartedCourse(?0), ImpartedCourse(?0)} which is obviously simplified into \code{Q(?0) <- ImpartedCourse(?0)}.
This query subsumes the original query, which can be discarded, avoiding the generation of other queries like for example \code{Q(?0) <- Student(?1), hasStudent(?0, ?1), ImpartedCourse(?0)} which would have been generated in a resolution step with the original query clause and the previously derived clause \code{Course(?0) <- Student(?1), hasStudent(?0, ?1)}.
By avoiding the further use of this query clause, we prevent the generation of all the clauses that could be derived from it, recursively, avoiding the generation of a full tree of clauses.
More precisely in this example we avoid the generation of all the clauses that involve the taxonomy of students.

This can happen in the preprocessing stage as well.
For another example in the context of the previous section we can consider the clause \code{AUX\$0(?0) <- ImpartedCourse(?0), Student(f0(?0))}, produced from the ontology.
When combined with \code{Student(f0(?0)) <- ImpartedCourse(?0)} we produce \code{AUX\$0(?0) <- ImpartedCourse(?0)}, which subsumes the first clause.
This means we can delete this subsumed clause.
Doing this in the preprocessing stage not only saves time for the inferences, but also for the latter subsumption checks and also keeps the size expansion of the preprocessed ontology low.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
