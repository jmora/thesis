\section{Problem statement}

This work aims at providing theoretical foundations and technical solutions for query rewriting in an OBDA context.
From the analysis of the state of the art in section \ref{sec:limits} we have seen that query rewriting approaches focus on a theoretical frame where some additional considerations may be made.
More precisely we propose to improve the applicability, contextualisation, performance (hence applicability), maintainability and results of query rewriting techniques by:

\begin{enumerate}
	\item Using the information provided by mappings in query rewriting.
  
  Considering this information should improve contextualisation (awareness of mappings), results and performance .% to target those mappings that will produce the set of certain answers.
	\item Using better-engineered methods to better address the problem of query rewriting in OBDA considering its specific characteristics.
  
  Better-engineered methods improve the performance in query rewriting and the maintainability of the software through further modifications.
	\begin{eboxdependent}
	\item Using the information provided by an EBox to target the predicates\footnote{Reminder: We will refer to concepts and properties in a TBox as ``predicates'', corresponding to predicates in FOL and Datalog.} that will produce the set of certain answers.
  
  Using this information should improving contextualisation (awareness of the knowledge in the EBox) and the results of query rewriting.
	\end{eboxdependent}
  \item Focusing on resolution to handle the $\mathcal{ELHIO}$ expressiveness to maintain the applicability of the results both in theory and practice.
  
  By keeping an expressive logic ($\elhio$) and standard procedures (resolution) we expect to preserve maintainability, obtaining solutions that can be used in a variety of systems, at least in $\elhio$ expressiveness and possibly in more expressive logics.
\end{enumerate}


%We can see that the 
%\begin{noeboxdependent}
%two
%\end{noeboxdependent}
%\begin{eboxdependent}
%three
%\end{eboxdependent}
%previous problems correspond to two different ways to address the limitations in the state of the art.
%
%\begin{enumerate}
	%\item The first way consists on considering the elements surrounding the problem at hand, \textit{exploring} a broader frame can help to escape local maxima as in an optimisation algorithm.
	%\item The second way consists on considering the internal properties of the problem at hand, \textit{exploiting} specific characteristics can help to reach greater maxima through fine tuning.
%\end{enumerate}

For the first problem the following research questions are addressed in this work:

\begin{itemize}
	\item{Can the information about mappings be useful for query rewriting in an OBDA context?
	
	OBDA systems use mappings to translate the query from the terms used in the ontology to the terms in the underlying data source.
	Therefore the mappings can provide meta-information about the characteristics of the information contained in the data source.
	However this meta-information is not being used in state of the art systems.
	We explore in chapter \ref{sec:map} the possibilities that mappings offer as a necessary and already existing description of the data sources that is usually declarative.
	}
	\item{Can the information from mappings be used in an efficient way?
	
	The information from the mappings allows generating rewritten queries that the underlying data source can execute more efficiently while obtaining the same results.
	This information may also provide additional constraints in the query rewriting process and considering these constraints may help to reduce the computational load to rewrite the queries.
	Using the information from mappings (if possible, according to the previous question) may be unfeasible in practical terms.
	On the contrary, using the information from mappings may mean not longer times to generate the rewriting but a gain in efficiency.
	}
\end{itemize}

For the second problem we consider the following questions:

\begin{itemize}
  \begin{comment}
  \item{Which are the characteristics of the query rewriting for OBDA problem?
	
	A better knowledge about the characteristics of this problem can provide a better insight.
	We can find characteristics that allow us to customise the query rewriting process to OBDA.
	Query rewriting uses resolution and resolution is very general.
	}
	\end{comment}
	\item{How can we tune current algorithms for query rewriting in OBDA? % to better address the characteristics of this problem?
	
	Most query rewriting techniques are based on resolution.
	Resolution is a very general technique; there are many different rules for resolution and many different calculi that can be used.
	Resolution is also a very useful technique; there are many different purposes for which we can use resolution, such as theorem proving, validation, search for inconsistencies, etc.
	% When doing this in an automatic way using some software there are some additional specificities to address as the specific way in which algorithms are applied or the data structures we may use.
	We may be able to tune these techniques to better address query rewriting by considering its specificity in an OBDA context.
	}
	\item{How can we measure the appropriateness of the solutions to the query rewriting problem?
	
	The evaluation for resolution techniques is primarily formal, in terms of soundness and completeness.
	When performing optimisations in some technique the soundness and completeness should remain unaltered while obtaining better results (optimisations).
	This improvement in the results will usually refer to the process time (efficiency), although it may also refer to the obtained output (e.g. terseness of an equally correct solution).
	Since the correctness is equal for the same assumptions, the evaluation of the optimisations performed in any given algorithm has to be empirical.
	This means using benchmarks to perform this empirical evaluation and assessing the quality of these benchmarks.
	}
\end{itemize}

\begin{eboxdependent}

Finally for the third problem we can consider the following questions:

\begin{itemize}
	\item{Can EBoxes be used in query rewriting?
	
	An EBox contains information about the extension of the ontology predicates in a given data source.
	Similarly to mappings, the information in an EBox can potentially be used to improve the process and results of query rewriting.
	In this case we are interested in using an EBox in a general context within query rewriting in OBDA, in a general way that can be later addressed to potentially any given algorithm.
	}
\end{itemize}

\begin{itemize}
	\item{What is the impact of the different EBoxes?
	
	Probably not all EBoxes are the same.
	We study the characteristics of the different EBoxes and try to draw conclusions about the impact of these characteristics in the query rewriting process and results.
	This could suggest transformations to perform on EBoxes or ABoxes (if possible) so that query rewriting algorithms can obtain a better performance.
	}
\end{itemize}


\end{eboxdependent}

\begin{jmoracut}
Basically we can see from the problems and questions that we focus on two different lines of action.
On the one hand we have a deeper analysis.
This means a better understanding of the characteristics of the problem at hand.
Hypothetically this leads to better query rewriting processes and results by addressing the problem in a way that is more optimal or tuned to its characteristics.
On the other hand we have a broader analysis.
This means a better understanding of external elements that are related with the problem.
These elements may influence or may be influenced by the query rewriting process or the query rewriting results.
Hypothetically this leads to better query rewriting processes and results by considering additional information and factors involved in a broader scope.
\end{jmoracut}

Finally, the fourth problem is transverse and present in all the previous problems.
All previous problems will be tackled without reducing the expressiveness from 