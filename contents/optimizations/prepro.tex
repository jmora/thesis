
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Ontology preprocessing}\label{sec:pre}

We propose a preprocessing stage that consists in performing inferences with ontology clauses before any query is executed.
These inferences only depend on the ontologies used, which are normally more stable than the data sources.
Hence, preprocessing is done only once as part of the configuration and once again every time the ontologies are updated.
Its objective is to save time when queries arrive to our system.
The preprocessing does not include the data in the data sources but only the ontology, contrary to other approaches that include preprocessing in the data (e.g. \citep{kontchakov2010}).
To clarify, this means that we make no assumptions about the data sources, i.e. data in the sources may be as dynamic as data streams~\citep{calbimonte2012}.
Basically, we can see that the rewriting generates a query $\query_\dtbox$ from an original query $\query$ using the TBox $\dtbox$.
As long as there are no changes in $\dtbox$, the rewriting process remains the same and so does the result ($\query_\dtbox$).
This happens regardless of how many changes may happen in the data sources and how much the answers to $\query_\dtbox$ may vary through time.

Among existing OBDA approaches, preprocessing is only done by Venetis \citep{venetis2011}, which focuses on reusing partial results among similar queries.
Some approaches are capable of some form of preprocessing; for example computing the dependency graph in the case of Nyaya.

We have seen in section~\ref{sec:ultraback} how resolution with free selection works.
% The algorithm in REQUIEM uses this kind of resolution until no new clauses can be generated with this strategy, the resolution with this stop condition is considered saturation.
Part of this saturation are inference steps where both premises are clauses in the ontology or derived exclusively from the ontology.
The query is not necessary for these inference steps, therefore they can be performed before the query is available and only once for all the queries.

To understand this optimisation we can simply consider that there are three types of clause combinations that happen in the search for valid resolution steps:
(a) ontology clauses with ontology clauses,
(b) ontology clauses with query clauses and
(c) query clauses with query clauses.
Among these three groups, the first one can be done before any query is available and is the basis for this optimisation.
The second group is the normal resolution performed after the query is available.
Finally, the third group does not contain any valid inference, as a query clause can only be a main premise, not a side premise.
This is also the basis for the optimisation explained in section \ref{sec:cons}.

The resolution corresponding to the preprocessing is done with the algorithm introduced in section \ref{sec:algo} (algorithm \ref{alg:resolii}) by using the same selection function defined previously for REQUIEM, which is correct and complete, with one modification: all clauses that are not subsumed are preserved after the resolution. Clauses containing functional terms, among others, would be discarded at the end of the saturation in REQUIEM, obtaining a Datalog program. In this case, these clauses may still lead to relevant results when the query is available, and thus are preserved until the second type of inferences, with the query, can be performed. For instance we can consider the following resolution step:


%{\begin{table}[htbp]\centering\small
\begin{center}
\begin{tabular}{r c l}
\mdl{Person(?0)}{Student(?0)}\\
\mdl{Student(f0(?0))}{ImpartedCourse(?0)}\\\hline
$\therefore $ \mdl{Person(f0(?0))}{ImpartedCourse(?0)}\\
\end{tabular}
\end{center}
%\end{table}}


Besides this resolution, some additional inferences are performed in a second stage of resolution to reduce the inferences needed later.
As explained in the preprocess algorithm (algorithm \ref{alg:kiipre}), some auxiliary predicates need to be introduced for the previous saturation function to work as expected, selecting appropriately main and side premises.

By removing auxiliary predicates we obtain a Datalog program that is equivalent, and a reduction in the total number of clauses that may potentially be generated
%space of clauses\footnote{By clause space of a Datalog program $\dtbox$ we mean the set of clauses $\{c | \dtbox \vdash c \}$} that we are handling,
since all the clauses that contain some of the removed auxiliary predicates 
can no longer be generated.
%are effectively removed from the clause space handled.
This can be done at this point due to the nature of the following resolution stage and its selection function.
Inferences in the following resolution step involve a query clause and an ontology clause.
This means that the head atom will always be selected for the ontology clause and will be the only atom selected for this type of clauses.
Therefore, we can use this selection function even if we obtain clauses that are not contained in any of the groups previously defined in REQUIEM and obtain a finite resolution for the generation of the Datalog program by separating ontology and query clauses (section \ref{sec:cons}).

To remove auxiliary predicates, the selection function selects the auxiliary predicates excluding one of them for each cycle through auxiliary predicates (as defined in section \ref{sec:wtf}).
This cycle detection is also used for the unfolding stage.
Cycle detection works in a similar way in REQUIEM, producing a linear Datalog in case the unfolding is not possible.
In our case, we reduce the number of different predicates that appear in the head of some clause by using this cycle detection.

For the inferences with auxiliary predicates, we can consider as an example the following axiom:
\begin{center}
$\exists hasStudent.Student\sqsubseteq\exists hasProfessor.Professor$
\end{center}

This axiom produces the clauses:

\begin{center}
\begin{tabular}{r c l}
\mdl{AUX\$0(?0)}{Student(?1), hasStudent(?0,?1)}, \\
\mdl{Professor(f1(?0))}{AUX\$0(?0)} and \\
\mdl{hasProfessor(?0,f1(?0))}{AUX\$0(?0)}\\
\end{tabular}
\end{center}


From these Datalog clauses we can obtain by resolution:

\begin{center}
\begin{tabular}{r c l}
\mdl{Professor(f1(?0))}{Student(?1), hasStudent(?0,?1)} and\\
\mdl{hasProfessor(?0,f1(?0))}{Student(?1), hasStudent(?0,?1)}
\end{tabular}
\end{center}

If the auxiliary predicate \code{AUX\$0(x)} is not chosen for exclusion for any cycle through auxiliary predicates then this can be done for all clauses containing this predicate. The auxiliary predicates have no real correspondence with any predicate in the ontology. Their only function is to allow these inferences.
Once the inferences are performed, the predicates and the clauses that contain them are no longer needed, and can be discarded. Therefore, the production of new clauses is leveraged with the elimination of some clauses, controlling the number of clauses produced after the preprocessing, as can be seen in table \ref{tab:prepro}.

Obviously, newly produced clauses can at the same time produce additional inferences.
Any inference done during the preprocessing stage reduces the number of inferences to be performed in later stages.
As we have seen the inferences are separated into two different saturation stages.
In the first stage, no unary atoms are selected for the inference.
This means that the only inferences that are performed are the inferences that will be needed to produce the Datalog program (i.e. to remove functional terms).
This avoids some types of recursion and limits the inferences in this stage and the size of the preprocessed ontology.
Despite these limits, the generation of results is notably faster due to the preprocessing stage, specially Datalog results, as we will see in section \ref{sec:opteva}.


% However, as we have seen, the inferences that can be done at preprocessing time can go further than that, for instance the axiom $\exists suffers.Condition \sqsubseteq SickPerson$ produces the clause \code{SickPerson(x)  <-  Condition(y), suffers(x,y)}, along with the previously produced \code{suffers(x,f12(x))  <-  AUX\$4(x)} we can infer \code{SickPerson(?0)  <-  AUX\$4(?0), Condition(f12(?0))}, this inference can be done before having any query available and doing it during the preprocessing stage reduces the computations needed when some query is available.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
