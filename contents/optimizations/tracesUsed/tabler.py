#!/usr/bin/python3

def normal(f):
  for l in f:
    try:
      yield '\\mdl{%s}{%s}\\\\\n'%tuple(l.strip().replace('$', '\\$').split('  <-  '))
    except:
      continue
  
def inferences(f):
  counter = 0
  for l in (e.strip().replace('$', '\\$') for e in f):
    try:
      if len(l) < 2:
        counter = 0
        yield '&&\\\\\n'
      else:
        prefix = '$\\therefore $ ' if counter == 2 else ''
        eol = '\\\\\\hline\n' if counter == 1 else '\\\\\n'
        (head, body) = tuple(l.split('  <-  '))
        yield '%s\\mdl{%s}{%s}%s'%(prefix, head, body, eol)
        counter += 1
    except:
      continue
     
 
if __name__ == "__main__":
  process = inferences
  process = normal
  with open('next.txt', 'r') as fin:
    with open('res.txt', 'w') as fout:
      for l in process(fin): 
        fout.write(l)
    
  
    

  
  
  
  
