ignoring role inclusion: hasProfessor TopObjectProperty 
ignoring role inclusion: hasStudent TopObjectProperty 

Ontology and query loaded (18 clauses) time: 1377800925655
	Person(?0)  <-  Student(?0)
	ImpartedCourse(?0)  <-  Student(?1), hasStudent(?0,?1)
	Student(f0(?0))  <-  ImpartedCourse(?0)
	hasStudent(?0,f0(?0))  <-  ImpartedCourse(?0)
	AUX$0(?0)  <-  Student(?1), hasStudent(?0,?1)
	Professor(f1(?0))  <-  AUX$0(?0)
	hasProfessor(?0,f1(?0))  <-  AUX$0(?0)
	Student(?0)  <-  UndergradStudent(?0)
	Course(?0)  <-  Professor(?1), hasProfessor(?0,?1)
	Person(?0)  <-  Professor(?0)
	Person(?0)  <-  PostDoc(?0)
	UndergradStudent(?0)  <-  Bachelor(?0)
	Student(?0)  <-  GradStudent(?0)
	Student(?0)  <-  hasStudent(?1,?0)
	Course(?0)  <-  PotentialCourse(?0)
	GradStudent(?0)  <-  MasterStudent(?0)
	GradStudent(?0)  <-  PhDStudent(?0)
	Q(?0)  <-  Course(?0), hasProfessor(?0,?2), hasStudent(?0,?1)
ImpartedCourse(?0)  <-  Student(?1), hasStudent(?0,?1)
hasStudent(?0,f0(?0))  <-  ImpartedCourse(?0)
ImpartedCourse(?0)  <-  ImpartedCourse(?0), Student(f0(?0))

hasStudent(?0,f0(?0))  <-  ImpartedCourse(?0)
AUX$0(?0)  <-  Student(?1), hasStudent(?0,?1)
AUX$0(?0)  <-  ImpartedCourse(?0), Student(f0(?0))

hasProfessor(?0,f1(?0))  <-  AUX$0(?0)
Course(?0)  <-  Professor(?1), hasProfessor(?0,?1)
Course(?0)  <-  AUX$0(?0), Professor(f1(?0))

hasStudent(?0,f0(?0))  <-  ImpartedCourse(?0)
Student(?0)  <-  hasStudent(?1,?0)
Student(f0(?0))  <-  ImpartedCourse(?0)

hasProfessor(?0,f1(?0))  <-  AUX$0(?0)
Q(?0)  <-  Course(?0), hasProfessor(?0,?2), hasStudent(?0,?1)
Q(?0)  <-  AUX$0(?0), Course(?0), hasStudent(?0,?1)

hasStudent(?0,f0(?0))  <-  ImpartedCourse(?0)
Q(?0)  <-  Course(?0), hasProfessor(?0,?2), hasStudent(?0,?1)
Q(?0)  <-  Course(?0), ImpartedCourse(?0), hasProfessor(?0,?1)

Student(f0(?0))  <-  ImpartedCourse(?0)
AUX$0(?0)  <-  ImpartedCourse(?0), Student(f0(?0))
AUX$0(?0)  <-  ImpartedCourse(?0)

Professor(f1(?0))  <-  AUX$0(?0)
Course(?0)  <-  AUX$0(?0), Professor(f1(?0))
Course(?0)  <-  AUX$0(?0)

hasStudent(?0,f0(?0))  <-  ImpartedCourse(?0)
Q(?0)  <-  AUX$0(?0), Course(?0), hasStudent(?0,?1)
Q(?0)  <-  AUX$0(?0), Course(?0), ImpartedCourse(?0)

hasProfessor(?0,f1(?0))  <-  AUX$0(?0)
Q(?0)  <-  Course(?0), ImpartedCourse(?0), hasProfessor(?0,?1)
Q(?0)  <-  AUX$0(?0), Course(?0), ImpartedCourse(?0)

Inferences: 10
Saturation completed (16 clauses) time: 1377800925661
	ImpartedCourse(?0)  <-  Student(?1), hasStudent(?0,?1)
	AUX$0(?0)  <-  Student(?1), hasStudent(?0,?1)
	Student(?0)  <-  UndergradStudent(?0)
	Course(?0)  <-  Professor(?1), hasProfessor(?0,?1)
	UndergradStudent(?0)  <-  Bachelor(?0)
	Student(?0)  <-  GradStudent(?0)
	Student(?0)  <-  hasStudent(?1,?0)
	Course(?0)  <-  PotentialCourse(?0)
	GradStudent(?0)  <-  MasterStudent(?0)
	GradStudent(?0)  <-  PhDStudent(?0)
	Q(?0)  <-  Course(?0), hasProfessor(?0,?2), hasStudent(?0,?1)
	Q(?0)  <-  AUX$0(?0), Course(?0), hasStudent(?0,?1)
	Q(?0)  <-  Course(?0), ImpartedCourse(?0), hasProfessor(?0,?1)
	AUX$0(?0)  <-  ImpartedCourse(?0)
	Course(?0)  <-  AUX$0(?0)
	Q(?0)  <-  AUX$0(?0), Course(?0), ImpartedCourse(?0)
ImpartedCourse(?0)  <-  Student(?1), hasStudent(?0,?1)
Student(?0)  <-  UndergradStudent(?0)
ImpartedCourse(?0)  <-  UndergradStudent(?1), hasStudent(?0,?1)

AUX$0(?0)  <-  Student(?1), hasStudent(?0,?1)
Student(?0)  <-  UndergradStudent(?0)
AUX$0(?0)  <-  UndergradStudent(?1), hasStudent(?0,?1)

ImpartedCourse(?0)  <-  Student(?1), hasStudent(?0,?1)
Student(?0)  <-  GradStudent(?0)
ImpartedCourse(?0)  <-  GradStudent(?1), hasStudent(?0,?1)

AUX$0(?0)  <-  Student(?1), hasStudent(?0,?1)
Student(?0)  <-  GradStudent(?0)
AUX$0(?0)  <-  GradStudent(?1), hasStudent(?0,?1)

ImpartedCourse(?0)  <-  Student(?1), hasStudent(?0,?1)
Student(?0)  <-  hasStudent(?1,?0)
ImpartedCourse(?0)  <-  hasStudent(?0,?1), hasStudent(?2,?1)

AUX$0(?0)  <-  Student(?1), hasStudent(?0,?1)
Student(?0)  <-  hasStudent(?1,?0)
AUX$0(?0)  <-  hasStudent(?0,?1), hasStudent(?2,?1)

Course(?0)  <-  PotentialCourse(?0)
Q(?0)  <-  Course(?0), hasProfessor(?0,?2), hasStudent(?0,?1)
Q(?0)  <-  PotentialCourse(?0), hasProfessor(?0,?1), hasStudent(?0,?2)

Course(?0)  <-  PotentialCourse(?0)
Q(?0)  <-  AUX$0(?0), Course(?0), hasStudent(?0,?1)
Q(?0)  <-  AUX$0(?0), PotentialCourse(?0), hasStudent(?0,?1)

Course(?0)  <-  PotentialCourse(?0)
Q(?0)  <-  Course(?0), ImpartedCourse(?0), hasProfessor(?0,?1)
Q(?0)  <-  ImpartedCourse(?0), PotentialCourse(?0), hasProfessor(?0,?1)

Q(?0)  <-  AUX$0(?0), Course(?0), hasStudent(?0,?1)
AUX$0(?0)  <-  ImpartedCourse(?0)
Q(?0)  <-  Course(?0), ImpartedCourse(?0), hasStudent(?0,?1)

Q(?0)  <-  Course(?0), hasProfessor(?0,?2), hasStudent(?0,?1)
Course(?0)  <-  AUX$0(?0)
Q(?0)  <-  AUX$0(?0), hasProfessor(?0,?1), hasStudent(?0,?2)

Q(?0)  <-  AUX$0(?0), Course(?0), hasStudent(?0,?1)
Course(?0)  <-  AUX$0(?0)
Q(?0)  <-  AUX$0(?0), hasStudent(?0,?1)

Q(?0)  <-  Course(?0), ImpartedCourse(?0), hasProfessor(?0,?1)
Course(?0)  <-  AUX$0(?0)
Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0), hasProfessor(?0,?1)

AUX$0(?0)  <-  ImpartedCourse(?0)
Q(?0)  <-  AUX$0(?0), Course(?0), ImpartedCourse(?0)
Q(?0)  <-  Course(?0), ImpartedCourse(?0)

Course(?0)  <-  PotentialCourse(?0)
Q(?0)  <-  AUX$0(?0), Course(?0), ImpartedCourse(?0)
Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0), PotentialCourse(?0)

Course(?0)  <-  AUX$0(?0)
Q(?0)  <-  AUX$0(?0), Course(?0), ImpartedCourse(?0)
Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0)

UndergradStudent(?0)  <-  Bachelor(?0)
ImpartedCourse(?0)  <-  UndergradStudent(?1), hasStudent(?0,?1)
ImpartedCourse(?0)  <-  Bachelor(?1), hasStudent(?0,?1)

UndergradStudent(?0)  <-  Bachelor(?0)
AUX$0(?0)  <-  UndergradStudent(?1), hasStudent(?0,?1)
AUX$0(?0)  <-  Bachelor(?1), hasStudent(?0,?1)

GradStudent(?0)  <-  MasterStudent(?0)
ImpartedCourse(?0)  <-  GradStudent(?1), hasStudent(?0,?1)
ImpartedCourse(?0)  <-  MasterStudent(?1), hasStudent(?0,?1)

GradStudent(?0)  <-  PhDStudent(?0)
ImpartedCourse(?0)  <-  GradStudent(?1), hasStudent(?0,?1)
ImpartedCourse(?0)  <-  PhDStudent(?1), hasStudent(?0,?1)

GradStudent(?0)  <-  MasterStudent(?0)
AUX$0(?0)  <-  GradStudent(?1), hasStudent(?0,?1)
AUX$0(?0)  <-  MasterStudent(?1), hasStudent(?0,?1)

GradStudent(?0)  <-  PhDStudent(?0)
AUX$0(?0)  <-  GradStudent(?1), hasStudent(?0,?1)
AUX$0(?0)  <-  PhDStudent(?1), hasStudent(?0,?1)

AUX$0(?0)  <-  ImpartedCourse(?0)
Q(?0)  <-  AUX$0(?0), PotentialCourse(?0), hasStudent(?0,?1)
Q(?0)  <-  ImpartedCourse(?0), PotentialCourse(?0), hasStudent(?0,?1)

Course(?0)  <-  PotentialCourse(?0)
Q(?0)  <-  Course(?0), ImpartedCourse(?0), hasStudent(?0,?1)
Q(?0)  <-  ImpartedCourse(?0), PotentialCourse(?0), hasStudent(?0,?1)

Course(?0)  <-  AUX$0(?0)
Q(?0)  <-  Course(?0), ImpartedCourse(?0), hasStudent(?0,?1)
Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0), hasStudent(?0,?1)

AUX$0(?0)  <-  ImpartedCourse(?0)
Q(?0)  <-  AUX$0(?0), hasProfessor(?0,?1), hasStudent(?0,?2)
Q(?0)  <-  ImpartedCourse(?0), hasProfessor(?0,?1), hasStudent(?0,?2)

AUX$0(?0)  <-  ImpartedCourse(?0)
Q(?0)  <-  AUX$0(?0), hasStudent(?0,?1)
Q(?0)  <-  ImpartedCourse(?0), hasStudent(?0,?1)

AUX$0(?0)  <-  ImpartedCourse(?0)
Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0), hasProfessor(?0,?1)
Q(?0)  <-  ImpartedCourse(?0), hasProfessor(?0,?1)

Course(?0)  <-  PotentialCourse(?0)
Q(?0)  <-  Course(?0), ImpartedCourse(?0)
Q(?0)  <-  ImpartedCourse(?0), PotentialCourse(?0)

Course(?0)  <-  AUX$0(?0)
Q(?0)  <-  Course(?0), ImpartedCourse(?0)
Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0)

AUX$0(?0)  <-  ImpartedCourse(?0)
Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0), PotentialCourse(?0)
Q(?0)  <-  ImpartedCourse(?0), PotentialCourse(?0)

AUX$0(?0)  <-  ImpartedCourse(?0)
Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0)
Q(?0)  <-  ImpartedCourse(?0)

AUX$0(?0)  <-  ImpartedCourse(?0)
Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0), hasStudent(?0,?1)
Q(?0)  <-  ImpartedCourse(?0), hasStudent(?0,?1)

Inferences: 33
Unfolding completed (36 clauses) time: 1377800925670
	ImpartedCourse(?0)  <-  Student(?1), hasStudent(?0,?1)
	AUX$0(?0)  <-  Student(?1), hasStudent(?0,?1)
	Course(?0)  <-  Professor(?1), hasProfessor(?0,?1)
	Q(?0)  <-  Course(?0), hasProfessor(?0,?2), hasStudent(?0,?1)
	Q(?0)  <-  AUX$0(?0), Course(?0), hasStudent(?0,?1)
	Q(?0)  <-  Course(?0), ImpartedCourse(?0), hasProfessor(?0,?1)
	Q(?0)  <-  AUX$0(?0), Course(?0), ImpartedCourse(?0)
	ImpartedCourse(?0)  <-  UndergradStudent(?1), hasStudent(?0,?1)
	AUX$0(?0)  <-  UndergradStudent(?1), hasStudent(?0,?1)
	ImpartedCourse(?0)  <-  GradStudent(?1), hasStudent(?0,?1)
	AUX$0(?0)  <-  GradStudent(?1), hasStudent(?0,?1)
	ImpartedCourse(?0)  <-  hasStudent(?0,?1), hasStudent(?2,?1)
	AUX$0(?0)  <-  hasStudent(?0,?1), hasStudent(?2,?1)
	Q(?0)  <-  PotentialCourse(?0), hasProfessor(?0,?1), hasStudent(?0,?2)
	Q(?0)  <-  AUX$0(?0), PotentialCourse(?0), hasStudent(?0,?1)
	Q(?0)  <-  ImpartedCourse(?0), PotentialCourse(?0), hasProfessor(?0,?1)
	Q(?0)  <-  Course(?0), ImpartedCourse(?0), hasStudent(?0,?1)
	Q(?0)  <-  AUX$0(?0), hasProfessor(?0,?1), hasStudent(?0,?2)
	Q(?0)  <-  AUX$0(?0), hasStudent(?0,?1)
	Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0), hasProfessor(?0,?1)
	Q(?0)  <-  Course(?0), ImpartedCourse(?0)
	Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0), PotentialCourse(?0)
	Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0)
	ImpartedCourse(?0)  <-  Bachelor(?1), hasStudent(?0,?1)
	AUX$0(?0)  <-  Bachelor(?1), hasStudent(?0,?1)
	ImpartedCourse(?0)  <-  MasterStudent(?1), hasStudent(?0,?1)
	ImpartedCourse(?0)  <-  PhDStudent(?1), hasStudent(?0,?1)
	AUX$0(?0)  <-  MasterStudent(?1), hasStudent(?0,?1)
	AUX$0(?0)  <-  PhDStudent(?1), hasStudent(?0,?1)
	Q(?0)  <-  ImpartedCourse(?0), PotentialCourse(?0), hasStudent(?0,?1)
	Q(?0)  <-  AUX$0(?0), ImpartedCourse(?0), hasStudent(?0,?1)
	Q(?0)  <-  ImpartedCourse(?0), hasProfessor(?0,?1), hasStudent(?0,?2)
	Q(?0)  <-  ImpartedCourse(?0), hasStudent(?0,?1)
	Q(?0)  <-  ImpartedCourse(?0), hasProfessor(?0,?1)
	Q(?0)  <-  ImpartedCourse(?0), PotentialCourse(?0)
	Q(?0)  <-  ImpartedCourse(?0)
Pruning completed (7 clauses) time: 1377800925683
	Course(?0)  <-  Professor(?1), hasProfessor(?0,?1)
	Q(?0)  <-  Course(?0), hasProfessor(?0,?2), hasStudent(?0,?1)
	ImpartedCourse(?0)  <-  hasStudent(?0,?1)
	AUX$0(?0)  <-  hasStudent(?0,?1)
	Q(?0)  <-  PotentialCourse(?0), hasProfessor(?0,?1), hasStudent(?0,?2)
	Q(?0)  <-  AUX$0(?0), hasStudent(?0,?1)
	Q(?0)  <-  ImpartedCourse(?0)
Time spent in the process: 28ms
