
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Query subsumption check}\label{sec:sub}

Subsumption checks consist on checking whether a part of the query that is being rewritten is subsumed by another.
When this happens one of the two parts of the query can be removed, resulting in a shorter query.

Subsumption checks are usually performed by checking pairs of clauses, checking whether one subsumes the other and removing the clause that is subsumed.
Another usual optimisation operation, clause condensation, is performed by checking intra-clause subsumption, that is whether an atom subsumes another.
In this case the subsuming atom is removed, since they are grouped by conjunctions, as opposed to the disjunctions that we find between clauses.

In the case of atom subsumption all previous approaches perform a similar check.
\begin{itemize}
\item REQUIEM checks atom subsumption in the ``condensation'' optimisation step.
\item In the case of Rapid this is done with the ``shrinking'' resolution rule.
\item Nyaya performs a similar operation in the ``factorization'' step.
\item Finally, Presto has the function ``DeleteRedundantAtoms'', naturally with a properly descriptive name.
\end{itemize}

In the case of clause subsumption check the situation is similar.
\begin{itemize} 
\item REQUIEM performs this in a separate stage after the resolution has finished by checking all clauses. %with a complexity of $O(n^{2})$.
When using the ``F'' mode, REQUIEM does also perform a ``full'' subsumption check, as described in \citep{bachmair2001}.
This means that during resolution newly derived resolvents may be deleted if they are subsumed by old or processed clauses.
However, the subsumption check is not performed the other way around with respect to old and new clauses.
\item Rapid performs a similar subsumption check, but the generation of subsumed clauses is reduced and more controlled, what allows limiting the check to subsets of the generated clauses.
This check is performed after each unfolding step.
\item In the case of Nyaya, subsumed clauses are removed with the elimination step, which is optionally performed after every rewriting step.
\item Presto produces a factorized Datalog where subsumption check becomes less tractable, and thus there is no subsumption check between clauses.
However, subsumption between sets of atoms that share some variable is considered with the \textit{most general subsumees}.
\end{itemize}

In our context, the clever handling of the unfolding sets done in Rapid cannot be added in a straightforward way, since the Datalog used is not linear, what means that unfolding sets are not composed of atoms but of conjunctions of atoms.
Our optimisation consists in performing the subsumption check among all generated clauses, as in REQUIEM.
However, instead of doing this as a separate stage from resolution, we add subsumption checks as part of the resolution process (line \ref{ln:subsumptionLoop} in algorithm \ref{alg:resolii}).
We formally prove the correctness of this optimisation in section~\ref{sec:optproofs} (proposition~\ref{prop:subsumred}).
More precisely, subsumed clauses can be removed at any time obtaining an equivalent query, i.e. a query equally satisfiable and that provides the same answers.
We evaluate the efficiency of this optimisation along with the other optimisations in section~\ref{sec:opteva}.

With this optimisation, new clauses are generated using resolution with free selection and every time a new clause is generated its subsumption is checked.
Therefore subsumed clauses are identified --- and removed --- as soon as a pair (subsuming-subsumed) is generated.
This allows finding clauses that are equivalent and processing them only once.
The subsumed clause can be safely removed, avoiding all the inferences it would take part in the resolution with free selection, i.e. preventing the generation of the tree of clauses that could be inferred by using that clause and its successors.
As a result, all later stages have a lesser computational load and require less time, including this subsumption check, which in this case checks less clauses since the number of generated clauses is reduced by removing subsumed clauses as soon as possible.
For this reason, early subsumption checks contribute to the efficiency of the rewriting process.
Additionally to that, in early stages of the query rewriting (also known as query expansion) process, there are less clauses in the query, and therefore the subsumption checks are less computationally costly.

%Obviously by preserving satisfiability we preserve the certain answers for that logic program.
Therefore, these subsumption checks that can be performed during resolution can be performed in any resolution stage.
%Therefore, these subsumption checks can be performed in the preprocessing.
Finally, there is an additional advantage in performing some resolution and subsumption checks in the preprocessing stage:
once the query is available, in the latter resolution process (the base clause is always the query) we know that the head of the generated clauses always belongs to the query.
Therefore, subsumption check between clauses can be further restricted to query clauses, excluding ontology clauses in the latter resolution stages where ontology and query clauses are processed separately.
This separation between query and ontology clauses is detailed further in section~\ref{sec:cons}.

%For example given the query \code{Q(?0) <- Course(?0), ImpartedCourse(?0)} and the axiom \code{Course(?0) <- ImpartedCourse(?0)} we obtain \code{Q(?0) <- ImpartedCourse(?0), ImpartedCourse(?0)} which is obviously simplified into \code{Q(?0) <- ImpartedCourse(?0)}.
%This query subsumes the original query, which can be discarded, avoiding the generation of other queries like for example \code{Q(?0) <- Student(?1), hasStudent(?0, ?1), ImpartedCourse(?0)} which would have been generated in a resolution step with the original query clause and the previously derived clause \code{Course(?0) <- Student(?1), hasStudent(?0, ?1)}.
For example we can consider the following resolution step:
\begin{center}
\begin{tabular}{ l r r l}
$\clause_1$:&\mdl{Q(?0)}{Course(?0), ImpartedCourse(?0)}\\
$\clause_2$:&\mdl{Course(?0)}{ImpartedCourse(?0)}\\\hline
$\clause_3$:&\mdl{Q(?0)}{ImpartedCourse(?0), ImpartedCourse(?0)}\\
\end{tabular}
\end{center}

In this case $\clause_3$ is derived and immediately simplified (condensed) into \code{Q(?0) <- ImpartedCourse(?0)}.
As a result, $\clause_1$ is subsumed by $\clause_3$ and immediately discarded for further inferences, avoiding other resolution steps and the generation of other queries, such as:

\begin{center}
\begin{tabular}{ l r r l}
$\clause_1$:&\mdl{Q(?0)}{Course(?0), ImpartedCourse(?0)}\\
$\clause_2$:&\mdl{Course(?0)}{Student(?1), hasStudent(?0, ?1)}\\\hline
$\clause_3$:&\mdl{Q(?0)}{Student(?1), hasStudent(?0, ?1), ImpartedCourse(?0)}\\
\end{tabular}
\end{center}

By avoiding the further use of this query clause, we prevent the generation of all the clauses that could be derived from it, recursively, avoiding the generation of a full tree of clauses.
More precisely in this example we avoid the generation of all the clauses that involve the taxonomy of students.

This can happen in the preprocessing stage as well.
For another example in the context of the previous section we can consider the clause following resolution step:

\begin{center}
\begin{tabular}{ l r r l}
$\clause_4$:&\mdl{AUX\$0(?0)}{ImpartedCourse(?0), Student(f0(?0))}\\
$\clause_5$:&\mdl{Student(f0(?0))}{ImpartedCourse(?0)}\\\hline
$\clause_6$:&\mdl{AUX\$0(?0)}{ImpartedCourse(?0), ImpartedCourse(?0)}\\
\end{tabular}
\end{center}


  %\code{AUX\$0(?0) <- ImpartedCourse(?0), Student(f0(?0))}, produced from the ontology.
% When combined with \code{Student(f0(?0)) <- ImpartedCourse(?0)} we produce \code{AUX\$0(?0) <- ImpartedCourse(?0)}, which subsumes the first clause.
% This means we can delete this subsumed clause.
Again this can be condensed into \code{AUX\$0(?0) <- ImpartedCourse(?0)}.
Analogously to the previous example, $\clause_4$ is subsumed and can be removed immediately.
Doing this in the preprocessing stage does not only save time for the inferences, but also for the latter subsumption checks, reducing the size expansion of the preprocessed ontology.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
