
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Prioritising some inferences}\label{sec:ord}

Different resolution strategies have different specifications with respect to the order in which inferences should be performed. Resolution with free selection may establish some order, depending on the selection function, by prioritizing the selection of some atoms in some sets of clauses, but it does also allow some degree of freedom with respect to which clauses should be resolved first. In this case we add some ordering criteria by using first some clauses in the resolution, with the expectation of producing subsuming clauses earlier and increasing the effect of the optimisation described in section~\ref{sec:sub}.

Two of the state-of-the-art systems indicate the order of the inference steps that are performed during their resolution: REQUIEM and Rapid.
\begin{itemize}
\item REQUIEM uses a selection function, which establishes some order in the inference rules that are applied, but there are no more ordering criteria and hypothetically using some clauses in the resolution could help to reduce the time required.
\item In Rapid clauses are selected more carefully during the resolution process, we can find that: \item \textit{shrinking} and \textit{unfolding} rules are applied alternatively, and shorter query clauses are considered first for unification, since those are the ones that more likely will subsume others.
\end{itemize}

However, to make subsumption checks effective in this scenario, another modification should be included.
REQUIEM selects clauses that have been previously generated for resolution in a FIFO fashion, what causes the exploration of the search space to be similar to breadth first.
An earlier generation of subsuming clauses and thus a greater reduction in the clause space explored can be obtained by selecting previously generated clauses in a LIFO fashion.
Preserving the saturation method in REQUIEM but using first the latest clauses generated, as in depth-first search instead of breadth-first search, provided a good improvement on its own.
The rationale behind this is that:

\begin{itemize}
\item results suggest that depth first search helps to find subsuming clauses earlier.
These clauses are generated after a condensation (atom subsumption in one clause).
This condensation is more likely as a deeper path is traversed in the resolution.
Because a deeper path explores the atoms that can be unified and possibly subsumed in a specific clause.
\item when clause subsumption is detected, branches are pruned.
A subsumed clause means a subsumed branch of clauses.
The less explored the pruned branch is, the more exploration is avoided.
Depth-first search delays the exploration of the remaining branches, which may be prevented, gaining more efficiency.
\end{itemize}

Furthermore, choosing shortest clauses first produces a greater improvement. The rationale for this is that shortest clauses in a UCQ have less atoms in the body and can more easily subsume other clauses, using these clauses first when checking for subsumption prevents more subsumed clauses from being used and thus generated.

This optimisation can be included in any system where the resolution does not have any specific order.
However it must be noted that this optimisation is relevant when combined with the subsumption check every time a new clause is generated, as in the previous section.
In these cases, this optimisation allows rewriting queries faster --- as evaluated in section \ref{sec:eva} --- without refusing to any expressiveness.
If the subsumption check is not performed and subsumed clauses are eliminated after resolution, then the order in which clauses are generated during resolution is irrelevant for any optimisation purposes, since in the end the same number of clauses will be generated: the full saturation.

Additionally, as explained in section \ref{sec:pre}, the inferences that may happen between two ontology clauses can be performed before the query is available, what means that once it is available we can consider only the remaining inferences, which will always use a query clause as the base clause, ordering further the combinations of clauses that are considered.
For example we have seen in previous sections that the next inference happens at an early stage:

%\[
%\begin{align}
%\mbox{\code{SickPerson(x)}}&\mbox{\code{  <-  AUX\$4(x), Condition(f12(x))}}\\
%\mbox{\code{Condition(f12(x))}}&\mbox{\code{  <-  AUX\$4(x)}}\\
%\therefore\mbox{\code{SickPerson(x)}}&\mbox{\code{  <-  AUX\$4(x)}}
%\end{align}
%\]


\begin{center}
\begin{tabular}{r c l}
\mdl{AUX\$0(?0)}{ImpartedCourse(?0), Student(f0(?0))}\\
\mdl{Student(f0(?0))}{ImpartedCourse(?0)}\\
$\therefore$ \mdl{AUX\$0(?0)}{ImpartedCourse(?0)}
\end{tabular}
\end{center}

Had different clauses and atoms in them been selected this other inferences similar to the next one could have been performed.

\begin{center}
\begin{tabular}{r c l}
\mdl{AUX\$0(?0)}{ImpartedCourse(?0), Student(f0(?0))}\\
\mdl{Student(?0)}{GradStudent(?0)}\\
$\therefore$ \mdl{AUX\$0(?0)}{ImpartedCourse(?0), GradStudent(f0(?0))}
\end{tabular}
\end{center}

Basically the whole taxonomy for students would have been explored.
All the clauses involved in this taxonomy produce longer clauses that are subsumed by the already mentioned \code{AUX\$0(?0) <- ImpartedCourse(?0)}.
This would produce a tree of subsumed clauses if the inference continued using that clause instead of the shorter \code{AUX\$0(?0) <- ImpartedCourse(?0)}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
