%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% In this work we focus on $\mathcal{ELHIO}^\neg$, handled by REQUIEM, and propose a series of optimisations that can be carried out in the rewriting process.
In this chapter we propose a series of optimisations that can be performed during the rewriting process.
As explained in chapter \ref{sec:obj} we focus on the $\mathcal{ELHIO}$ expressiveness.
By focusing on this very expressive logic we expect the optimisations performed here to be generalisable to a broad range of less expressive logics and possibly some more expressive logics.
Despite this expressiveness, the efficiency of the algorithm obtained is comparable to those addressed at query rewriting with less expressive logics.
% Additionally to aforementioned optimisations, we propose a series of optimisations to perform on the rewriting process and its results when considering the mappings available for a specific pair of an ontology and a data source. The results obtained are comparable to those obtained in previous approaches for less expressive logics.
These optimisations are implemented in the system \kyrietwo.

% With the approaches aforementioned, their properties, and the properties defined for the context we work into, we proceed to describe several optimisations that we propose in this process, with the intention of performing query rewriting faster, while maintaining the expressiveness. 

% In this context this approach as well as previous approaches pay special attention to efficiency.
Query rewriting for first-order reducible logics is from its origin meant to be efficient, focusing on restricting the inference to the TBox --- usually small in size when compared with the ABox --- and producing a query for a relational database to obtain the answers on the ABox.
Therefore, OBDA systems rely on using relational database technology, which is normally considered as more mature, stable and efficient.
Queries are only restricted by the set of terms available in the TBox.
Because of the combinatorial explosion that this represents (exemplified in section \ref{sec:mconcp}) and the arbitrary length of queries, materialization of all queries and answers is unfeasible.
Queries have to be rewritten and resolved at run-time. 
In addition, query answering is often interactive, i.e. involving a human in the loop; thus reduced response times are desired and expected for these systems.

Similarly to efficiency, the more expressiveness the better, since this is the main feature of a query rewriting system that includes an ontology when compared with the database that answers the rewritten query.
However expressiveness comes at a cost in efficiency.
Most expressive logics imply a greater computational complexity to be rewritten, sometimes requiring also a more expressive target language, for instance rewriting recursive Datalog programs into a finite UCQ is dependent on the ABox.
In this context we propose a series of optimisations that are applicable to query rewriting in the context of the $\mathcal{ELHIO}^{\neg}$ expressiveness.
%, which is among the most expressive logics proposed to date for this type of query rewriting.

In our case, we reduce the query rewriting time by performing some of the tasks needed for the rewriting before any query is handled to the system.
We also avoid the generation of subsumed clauses by the combination of some optimisations, which consist on checking clause subsumption as soon as possible and producing shorter clauses first.
We produce shorter clauses first and reduce the time required to check clause subsumption by condensing clauses and ordering the clauses depending on their length.
Additionally, clauses are separated into two groups, candidates for main premise and candidates for side premise, when possible, with an additional efficiency gain.

Analogously to the previous chapter, these optimisations change the general OBDA scenario from figure \ref{fig:wholeprocessfig}  to the one that we can see in figure \ref{fig:wholeprocessfigii}.
Again the changes in the whole picture are subtle, simply adding the preprocessing stage to the whole OBDA scenario.
In this case there are also some changes inside the rewriting stage, as explained in figure \ref{fig:stagestwo}.
We can see that the mappings are not used this time, allowing for a better comparison of the impact of these optimisations without considering mappings.
Since mappings are not used, the results obtained after applying these optimisations are the perfect rewriting \citep{calvanese2000} when possible.
This means that the output is the same as previous approaches when the ontology is not beyond the expressiveness that approaches other than REQUIEM may handle.
In those cases where a recursive Datalog may be obtained, as in REQUIEM, the strategy for the unfolding is different.
REQUIEM produces a linear Datalog program, meaning that the recursive clauses have at most one intensional database (IDB) predicate in the body.
However in {\kyrietwo} the unfolding goes further and minimizes the number of different IDB predicates that appear in the head of some clause, instead of those that appear in some body.
This way less subqueries are produced whenever the query has to be fragmented into subqueries.
The results obtained and the conclusions that can be derived from them will be evaluated and analysed in chapter~\ref{sec:eva}.

\begin{figure}[!htb]%
\centering
\begin{tikzpicture}[node distance = 25mm and 35mm ,  thick, every node/.style={transform shape}, auto, on grid=true]
\node [ontology] (ontology) {ontology};
\node [block, below= of ontology] (preprocessing) {preprocess};
\node [cloud, right= of preprocessing] (clauses) {clauses};
\node [block, below= of clauses] (rewriting) {\textbf{\textsc{query rewriting}}};
\node [cloud, right= of rewriting] (rewrittenquery) {rewritten query};
\node [doc, below= of rewriting] (mappings) {Mappings};
\node [cloud, right= of clauses] (query) {query};
\node [block, right= of mappings] (translation) {query translation};
\node [cloud, right= of translation] (translatedquery) {translated query};
\node [block, below= of translatedquery] (execution) {query execution};
\node [database, below= of execution] (database) {data source};
\node [cloud, below= of translation] (results) {results};
\node [block, below= of mappings] (translationres) {results translation};
\node [cloud, below= of translationres] (transresults) {translated results};
\path [line] (query) -- (rewriting);
\path [line] (ontology) -- (preprocessing);
\path [line] (preprocessing) -- (clauses);
\path [line] (clauses) -- (rewriting);
\path [line] (rewriting) -- (rewrittenquery);
\path [line] (rewrittenquery) -- (translation);
\path [line] (translation) -- (translatedquery);
\path [line] (translatedquery) -- (execution);
\path [line] (execution) -- (database);
\path [line] (database) -- (execution);
\path [line] (execution) -- (results);
\path [line] (results) -- (translationres);
\path [line] (translationres) -- (transresults);
\path [line] (mappings) -- (translation);
\path [line] (mappings) -- (translationres);
\end{tikzpicture}
\caption{OBDA process with query rewriting and a preprocessing stage.}%
\label{fig:wholeprocessfigii}%
\end{figure}

In the following sections we go through the different optimisations proposed.
We will provide a running example in section \ref{sec:runexopt} and illustrate each of these optimisations with a conventional ontology in the domain of university studies. % hospital domain\footnote{\url{https://gist.github.com/2217737}}.
First, we consider the preprocessing in section \ref{sec:pre}.
The subsumption check, used to remove subsumed atoms and clauses, is explained in section \ref{sec:sub}.
The heuristic priorization of some inferences is explained in section \ref{sec:ord}.
Finally, in section~\ref{sec:cons} we explain how some searches for valid inferences can be constrained. For comparisons we focus especially on REQUIEM, which is the only approach that covers the {\elhio} expressiveness.

% In this chapter we see that some of these resolution rules can be applied before having any query, that subsumed clauses can be deleted, hence optimising the process, and that resolution with free selection has still a degree of freedom that allows introducing some heuristics to optimise the process further, along with other optimisations for query rewriting that can be done at the implementation level of this resolution method.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
