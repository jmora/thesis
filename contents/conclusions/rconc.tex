
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{General Conclusions}\label{sec:rconcp}

%``In theory, there is no difference between theory and practice. But, in practice, there is.'' --- source unknown.

We have considered the problem of query rewriting in OBDA and made proposals on its three main aspects.
\begin{description}
  \item[Input] We have considered additional elements that could be used as part of the input.
        This requires looking at the problem from a \textit{broader} perspective.
        The mappings are required for OBDA, they add no additional requirements or constraints to the whole problem.
        However the mappings are not used in the query rewriting process.
        By including the mappings as part of the input we obtain additional benefits, as we have seen.
        Additionally, an EBox may provide additional information about the extensional containment of predicates and projections of predicates in the queries, with consequent optimisations.
  \item[Process] The process is dependent on the logics that are used.
        In our case we have chosen the very expressive logic $\mathcal{ELHIO}$.
        With this logic fixed as a requirement we have performed optimisations in the query rewriting process by looking at the process in a \textit{detailed} way.
        % These optimisations may be incorporated in other systems and extended with further optimisations.
        The impact of these optimisations is comparable to the impact that can be obtained by modifying the expressiveness.
        This shows the relevance of engineering optimisations and research in algorithms for automated resolution, in this case applied to the specific context of query rewriting.
  \item[Output] The output is improved by both previous elements.
        A shorter output in equal complexity conditions means less work load for the systems processing the query.
        The output has been improved and has been analysed.
        Special attention has been paid to the evaluation that can be done of these systems and how it can be done, making contributions in this area as well.
\end{description}

These tasks have been done mostly with a pragmatical and applied approach in mind.
At the same time, query rewriting approaches in the state of the art are mostly focused on theoretical considerations.
Due to these facts we consider to have a balanced proposal with respect to theoretical and pragmatical considerations.
A theoretical evaluation (completeness and soundness) is required in these cases and it has been included in the corresponding sections.
Additionally, we have given the first steps towards a qualitative evaluation.
We have collected assets and used them to derive these qualitative conclusions that allow for a more pragmatical evaluation.
We have also proposed some guidelines for the extension and improvement of this qualitative evaluation.
As we have seen and as we would like to remark and summarise, in this thesis attention has been paid and contributions have been made to the specific details, to the broader picture, to the theoretical aspects and to the pragmatical aspects of the problem.

Through several chapters, we have seen several points that advise the estimation of the capabilities and characteristics of the whole OBDA system to optimise the process and results of query rewriting further.
We have seen that the lack of mappings for some predicates can help to produce shorter rewritings in shorter times.
In a modular system, one particular point to consider is the module responsible of processing the rewritten query, its efficiency for different types of queries and its capabilities, e.g. the possibility of processing UCQs, Datalog or recursive Datalog.
In addition, having information about the extensional containment of some predicates may help to produce much shorter rewritings in shorter times reducing the redundancy in the answers.

While this further optimisation could be performed in one single way, it is also possible that addressing specific characteristics means adapting to different domains for each cluster of characteristics.
For this reason, we also point the convenience of further analysis of the domains in which OBDA is used and the characteristics they may present.


\begin{comment}
Finally, I would like to include some lessons learned from the mistakes and the successes.
My experience on research is limited, therefore these lessons learned will probably be expanded, improved, corrected or even replaced.
The lessons I would like to mention are two, one on the definition of the problem and another one on the definition of the evaluation.

\subsection{The definition of the problem.}

The definition of the problem is a problem on itself and one that should not be underestimated.
The problem should be defined clearly to avoid putting effort on solving a different and irrelevant problem.
At the same time, there should not boundaries or limitations added to the problem a priori.
Therefore we have two goals that are conflicting to some extent, and the goal is to find a balance that cannot be clearly estimated.

Research topics are complex and require an extensive background to start working on them.
This requires reading many papers on the area to understand the current state of the art.
However, papers have a high level of abstraction and are concise due to their format.
This can be solved with a good bibliography and a good set of books that provide examples and additional guidance.
Probably this is good, sufficient and necessary.
Nevertheless, having the source code is convenient for a better understand of the concepts.
It is only helpful to check how things work and to understand how the abstract ideas in the papers are materialized into code.
And it is very helpful.

Moreover, when the funding of the project is public, it is only legitimate to release the product of that funding to the public that funded it.
This is why the source code of the system developed during this thesis and described in this book is available as open source
\footnote{here: \smaller\url{https://github.com/jmora/kyrie}}.

\subsection{The definition of the evaluation.}

When the problem has been defined we have a starting point.
However, to make the trip safe we do also need a goal and a way to ascertain to what extent we reached to that goal.
This requires a framework for the evaluation.
With such a framework we can ascertain the properties of the solution.
We can also ascertain the quality of the solution if the goal is specific.

Having such a goal is crucial to be able to know whether we are progressing towards that goal and to know the direction.
Otherwise big amounts of work could be wasted as their results could be pointless.
An evaluation framework is necessary in any case to judge the research that is done and to publish it.
It is convenient to progress on the research and necessary to finish it, for these reasons it should be prioritized.
Similarly to test-driven development\citep{astels2003}, I would like to speak about evaluation-driven research.
The cases of research and development are clearly different, that is the reason to consider tests in one case and evaluation in the other.
However, there is also some parallelism.

Having the evaluation framework before starting the research helps to define the boundaries and the objectives.
The whole process and judging its progress become easier when applying the evaluation framework.
Obviously this applies when there is some empirical evaluation to be performed, without requiring it to be the only evaluation.
But when this is the case, no additional requirements are added and the benefits of putting the evaluation first are many.
Due to these reasons, I can only recommend prioritizing the evaluation as much as possible and let it drive the research process.
\end{comment}

\begin{comment}
This section should be about what has changed in the OBDA area with this work.
Basic information theory can tell us that the emission of information provokes no change if it is not received or if it is ignored.
The relevance and impact of the information condensed in this book is left at the discretion of the reader.
This section focuses on conclusions in research, specially focusing on engineering and science as a tool for both.
Due to the limited experience in research, the conjectures here should be taken with a grain of salt.
Despite of the lack of accuracy, we consider this section worth writing because an explicit conjecture is better than (or not as bad as) an implicit one.

First we can state a difference between science and engineering by considering the focus of these two disciplines.
Science has an aspect that consists in focusing on understanding and improving the knowledge available about the world.
Engineering has an aspect that consists in focusing on creating devices, tools, algorithms, methodologies and solutions in general to some specific problem or set of problems.
Obviously engineering uses profusely the knowledge obtained by science and the methods of science to obtain additional knowledge.
This knowledge is a tool to solve problems and therefore it can be considered product itself as well.

We can put an hypothetical example in the context of OBDA.
We can consider the difference between the ontologies that users would like to utilise in OBDA approaches and the expressiveness that OBDA systems can handle.
Science can study this difference and obtain many interesting results in this direction.
Engineering can focus on solving this problem by shortening or eliminating this difference.
There are two obvious (and compatible) ways to do this.
One way is developing new OBDA systems that can handle an expressiveness more similar to what users are already utilising.
The other way is creating methodologies to help users develop ontologies that are better suited to what OBDA systems can handle.

\deprecated{Venn diagram - What is written in ontologies, what systems can handle, }

\deprecated{Difference between knowing reality and knowing some part of reality that is private and only some people can access. For instance databases and queries. If inside a project then it's possible to get access to some resources within that project, but that gives an important bias to the use cases and the context in which the project is being evaluated. In short, openness helps, a lot, accessing resources, evaluating stuff, etc. Obviously this costs some money but... economics and politics are a different thesis.}

\deprecated{Race to a wrong goal. Speculation. If there are not good use cases, benchmarks and evaluation then there is no feedback about how well you are doing, so how can you know whether you are doing anything well or not. Objective evaluation needs an objective that is objective, otherwise arbitrary goals lead to arbitrary results that cannot be compared.}

\deprecated{The goal today are publications. What is publishable and what is useful are two different things that may overlap, hopefully. And anyway, what is useful cannot even be known, check previous things.}

\deprecated{Stick to the standards. Stick to the real problems. Evaluation before bullshitting some pointless implementation.}

\deprecated{A veces es más fácil hacer una demostración formal de algo pequeño y tonto que buscarla y referenciarla. No debería obligársenos a buscarlas, puesto que a veces puede ser también más complicado encontrar el paper citado (y mirarlo por separado) en lugar de ver una demostración pequeña de algo que igualmente es evidente. Eso significa que el otro paper no consigue citas y así es exactamente como debería funcionar, las contribuciones irrelevantes no deberían ser prevented de ser publicadas, sino dejar que se publiquen y que lo cite quien lo necesite, si lo necesita alguien, las citas y no la venue medirían la relevancia.}
\end{comment}