
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Future work}\label{sec:futures}

% http://www.youtube.com/watch?v=xO1TScQwu6g

In this thesis we have focused on optimising query rewriting for ontology based data access.
The challenges to overcome in the future will depend on the use that is given to this technology.
Query rewriting is an approach that allows abstracting from the data sources, while considering some of their characteristics (e.g. the predicates that are mapped).
For example, other approaches for OBDA rely on the possibility to modify the schema of the database (e.g. Ultrawrap~\citep{sequeda2013}) or the mappings (e.g. OnTop~\citep{rodriguez-muro2012}).
In the case of query rewriting some of the most interesting lines to explore in the future are those where the modification of the schema of the data source or the mappings is not possible.
One example in this case is querying web services~\citep{thakkar2002,arpinar2005,gray2011}, the so-called Deep Web~\citep{he2007}.
Another example is querying data streams~\citep{Calbimonte2010} and the so-called Internet of Things~\citep{atzori2010}.
Basically, not requiring modifications on the data sources and abstracting from them as much as possible eases a future integration with a wider range of data source types.

If we want to keep a good generality and applicability with this abstraction one aspect that requires special focus are the interfaces.
A good way to ensure interoperability is complying with well established standards.
While Datalog is a general language that has been and is currently widely used, a standard with particular interest for queries in the semantic web is SPARQL.
SPARQL presents new challenges and opportunities to explore when used as the target or source language for the process of query rewriting.

SPARQL has an expressiveness comparable to Datalog~\citep{angles2008,perez2009}, but SPARQL 1.1 has some additional features beyond those offered by SPARQL.
Due to the expressiveness in SPARQL 1.1 and more specifically the property paths, some recursion is allowed, which is powerful and complex at the same time~\citep{arenas2012,losemann2012}.
Additionally, subqueries are possible, with similar consequences~\citep{angles2011}.
Both features allow to rewrite a linearly recursive Datalog query into a SPARQL 1.1 query, with subqueries for the intensional predicates and property paths for some recursive predicates.
Queries can also contain operators for comparisons, filtering and aggregation among others, which are not supported by conventional Datalog.
This opens several challenges and possibilities to explore that are enabled by the expressiveness in SPARQL 1.1. 
For example the type of recursion in $\mathcal{ELHIO}$ seems to be translatable to property paths.
This means that possibly more expressive logics could also be handled in the rewriting generating SPARQL 1.1 queries.
Possibly some features in the Horn-$\mathcal{SHIQ}$ that Clipper handles could be supported by rewriting to SPARQL 1.1.
The expressiveness of SPARQL could extend further beyond Datalog, since SPARQL offers an additional possibility beyond first-order rewritability, the predicates in the triples can be variables and can be unified.

Besides the language of the rewritten queries, we can consider as well the language for the original queries.
If the rewritten queries are in SPARQL, one perfect match for the original queries would be SPARQL as well.
In the present thesis we have rewritten unions of conjunctive queries, but SPARQL queries pose new challenges and opportunities.
We can clearly see that the structure in SPARQL queries is different and includes additional operators (e.g. aggregation).
In the case of SPARQL, the semantics of SPARQL entailment are expected, but they can be extended with richer semantics like OWL~\citep{polleres2007,kollia2011,glimm2011, glimm2012}.
However this work and the work done on OBDA have progressed separately.
Bridging this gap should mean an improvement for both communities, increase usability, applicability and adoption.

Additionally, due to the abstraction from the mappings, these mappings could map several data sources, allowing data integration and federated queries~\citep{buil-aranda2013}.
In such a context, further optimisations could be considered, for example join operations between predicates in a data source would probably be less expensive (in computational terms) than join operations between predicates in different data sources.
When considering several data sources additional considerations could be done with respect to provenance and data quality.
These considerations would definitively require additional metadata and would impose strong modifications in the algorithms.
In such a scenario, the correctness of the result and the efficiency of the process and the rewritten query would remain relevant. However, the focus would be on the quality of the query and its answers, which would certainly require new assets for the evaluation.

We have also seen from the optimisations and the evaluation that a quantitative and empirical approach may be beneficial for query rewriting.
We have given a few steps in that direction, but there is still much work to be done in that line.
Better benchmarks could be developed and better tools for benchmarking as well.
On the side of the benchmarks, they could be improved with a good compilation of use cases including ontologies, mappings, queries and even datasets if possible.
In this sense initiatives like Mappingpedia\footnote{\url{http://mappingpedia.linkeddata.es/}} could be useful.
On the side of the tools, we can consider the creation of an automated suite of tests to be run on a platform like the one provided by SEALS~\citep{garcia-castro2010}.
There are two aspects that are particularly interesting in such a context.
On the one hand, different systems for query rewriting could be compared with each other.
Having a full OBDA system or a set of OBDA systems where query rewriting would be integrated would allow to determine the characteristics of the rewritten queries that work better with other modules of the OBDA system, and at the same time the characteristics of those modules that are relevant for query rewriting.
On the other hand, OBDA systems that rely on query rewriting could be compared with OBDA systems that use different techniques (e.g. mapping saturation).
Certainly, not all OBDA systems can be applied in the same context, as some have requirements that other systems do not have (e.g. possibility of materialising some facts, modifying the database schema, etc.), however, this comparison would allow to check details as for example the impact on performance caused by a different set of requirements.
%These tests could provide a full stack allowing the rewritten queries to be evaluated and allowing the answers for them to be obtained.
%This would allow to compare different approaches for OBDA, like query rewriting or mapping saturation among others.
%That comparison is not exactly fair and each approach is better suited for a different set of scenarios.
%However, this would allow to check the difference in performance when an approach is used in an scenario for which it is not perfectly suited, and check whether the difference is negligible or several orders of magnitude away.

Finally, in a much more general perspective, OBDA allows accessing data sources using ontologies.
For this, some data is calculated, mainly performing operations on strings to compose the URIs for the individuals in the ontology.
This calculation could be taken much further, specially when considering datatype properties, numbers and vectors.
As the amount of data and their quality increase, the value of data analysis increases as well, and there is an important trend in the analysis of this data and data science, some authors do even talk about a ``Fourth Paradigm''~\citep{hey2009} when considering data-intensive science.
The combination of data analysis techniques with OBDA could allow to access datatype properties, concepts and values that are calculated numerically from data with a different granularity.
A data scientist in this case would not \textit{calculate} a mean, an average or search for outliers, but would \textit{query} for them.
There are many problems in the use of OBDA for big data~\citep{calvanese2013} that might need to be solved in satisfactory ways to allow the addition of a more complex analysis in an OBDA system.
Therefore, this is a line for a far future where much exploration needs to be done yet.
