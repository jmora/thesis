
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Preliminaries}

We introduce in this section some definitions that will be useful for the remainder of the chapter.
First of all, for some axiom $\axiom$ we will refer to its left hand side and right hand side as respectively $LHS(\axiom)$ and $RHS(\axiom)$.

We define the \emph{axiom graph} of a set of DL axioms and the \emph{clause graph} of a set of Horn clauses.
These definitions allow defining some relevant properties more concisely, as they make a more clear distinction on whether an axiom (or clause) is contained in a subgraph.

\begin{definition}\label{def:dlgraph}
We define $\dlgraph{\otbox}$, the \emph{axiom graph} for an $\ELHIO$ TBox $\otbox$, as the directed graph $\graph{\axiom} = \tup{\vertexes{\axiom}, \edges{\axiom}}$ such that:
(i) for each axiom $\axiom \in \otbox$, $\axiom\in \vertexes{\axiom}$;
and (ii) for each $\tup{\axiom_{a}, \axiom_{b}}$ such that $\axiom_{a}, \axiom_{b} \in \otbox$
  if there exists a predicate $\predicate$ such that $\predicate$ appears in ${RHS(\axiom_{b})}$ and $\predicate$ appears in ${LHS(\axiom_{a})}$
  then $\tup{\axiom_{a}, \axiom_{b}} \in \edges{\axiom}$.
\end{definition}

The definition of clause graph is analogous to axiom graph but defined over clauses, i.e. the differences are syntactical.
Both definitions are needed as analogous operations will be done in different contexts, after and before the syntactical conversion of DL axioms to Horn clauses is performed.

\begin{definition}\label{def:cgraph}
We define $\cgraph{\clauses}$, the \emph{clause graph} for a set of clauses $\clauses$, as the directed graph $\graph{\clause} = \tup{\vertexes{\clause}, \edges{\clause}}$ such that:
  (i) for each clause $\clause \in \clauses$, $\clause \in \vertexes{\clause}$; and
  (ii) for each $\tup{\clause_{a}, \clause_{b}}$
  such that $\clause_{a}, \clause_{b} \in \clauses$ if there exists a predicate $\predicate$ such that $\predicate$ appears in $\body{\clause_{b}}$ and $\predicate$ appears in 
  $\head{\clause_{a}}$ then $\tup{\clause_{a},\clause_{b}} \in \edges{\clause}$.
\end{definition}

We now introduce the definition of accessible clauses (from some other clause) which is similar to the definition of reachability.
Intuitively, we say that a clause $\clause_a$ is accessible from another clause $\clause_b$  when $\clause_b$ is reachable from $\clause_a$.
However, the context in which these definitions will be used is different, as reachability is defined in a set of clauses {\wrt} a query and accessibility is defined {\wrt} the values of the atoms in clauses, starting from the extensional values.
Therefore, we can intuitively consider that both reachability and accessibility are transitive properties, that differ in the way in which they propagate.
In particular, reachability propagates in a backward chaining fashion and accessibility propagates in a forward chaining fashion.

Another relevant concept when considering directed graphs is the notion of Strongly Connected Components (SCCs).
Analogously to accessible clauses, we say that a vertex $\vertex{i}$ is accessible from some other vertex $\vertex{j}$ in a graph $\graph{g} = \tup{\vertexes{g}, \edges{g}}$ when there is an edge $\paren{\vertex{j}, \vertex{i}}$ or there is some other vertex $\vertex{k}$, an edge $\paren{\vertex{j}, \vertex{k}}$ and $\vertex{i}$ is accessible from $\vertex{k}$.
In a directed graph $\graph{d}$, the SCCs are the maximal sets of vertexes such that for each SCC $\vertexes{c}$ every pair of vertexes $\vertex{i}, \vertex{j} \in \vertexes{c}$ are mutually accessible.
The vertexes that do not satisfy this property with any other vertex are considered SCCs of cardinality~1.
Therefore, the SCCs define a partition of the original graph.

\begin{definition}
We say that a clause $\clause_a$ is accessible from another clause $\clause_b$ in a set of clauses $\clauses$ when:
  (i) the head predicate of $\clause_b$ appears in the body of $\clause_a$, or
  (ii) there exists some other clause $\clause_c$ such that the head predicate of $\clause_b$ appears in the body of $\clause_c$ and $\clause_a$ is accessible from $\clause_c$.
\end{definition}

EBox and ABox extensional constraints have been previously defined, as we have seen in section~\ref{sec:ebox}.
However, we introduce a new and equivalent definition for EBox to relate it with the definitions used in the thesis, especially the definitions in section~\ref{sec:mappre}.

\begin{definition}\label{def:ebox}
A Horn EBox is a set of clauses $\ebox$ that specify containment relations between the values of the predicates in an ABox.
These relations are satisfied by an ABox $\abox$ when $\forall \clause \in \ebox. \contrib{\abox}{\clause} \subseteq \evalues{\abox}{\pred{\head{\clause}}}$.
\end{definition}

There are two things to note in this definition.
First, that an EBox is defined according to an ABox that satisfies it, i.e. that fulfills the relations between the predicates expressed by the EBox.
In the case of OBDA systems this ABox is normally virtual, i.e. composed of a set of mappings and a data source.
Second, that an EBox refers to the extensional values in the ABox and it must not be mistaken with a TBox.
An axiom (equivalently a clause) in the TBox expands the values for some predicate with intensional values, while an axiom (equivalently a clause) in the EBox constrains the possible extensional values for some predicate, and therefore the admissible ABoxes. 
Logically, the DL EBox is the syntactic conversion of the Horn EBox according to the rules in table~\ref{tab:conversion}, and its definition is analogous.

Finally, we introduce the definition of pure EBox.
Intuitively, the pure EBox is composed of the part of the EBox that is composed by predicates that have no intensional definition in the TBox.
We will see that this part of the EBox has especial characteristics and properties that we will use.
Note that the pure EBox $\eebox$ is defined not only {\wrt} an ABox but also {\wrt} a TBox.


\begin{definition}
Let $\ontology = \tup{\tbox, \abox}$ be an ontology and $\ebox$ an EBox satisfied by the ABox $\abox$.
The set of clauses $\set{\clause_e \in \ebox \mid \forall \clause_t \in \tbox. \pred{\head{\clause_t}} \notin \preds{\clause_e}}$ is the pure EBox, and we represent it with $\eebox$.
\end{definition}

Analogous considerations can be made by replacing the ontology $\ontology$ with an OBDA system $\obdadef$.