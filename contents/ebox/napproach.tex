\section{Optimisations} \label{sec:propos}

The algorithms described in the previous section group a series of operations that are described into more detail in this section.
These optimisations are formalised in the next section and their correctness is formally proven.

\subsection{Deletion of strongly connected components}

The strongly connected components (SCCs) that are entailed by the EBox and are not accessible from any other SCC can be deleted.
This operation is done in two different contexts.

In the first case, this deletion is the first operation that is performed in the process.
The deletion is performed directly on DL axioms, modeling them as a DL graph (definition~\ref{def:dlgraph}).
In the second case, this deletion is performed on the clauses in the Datalog program, which are modeled as a clause graph (definition~\ref{def:cgraph}).
This second context is considered in two different stages: the first one is considered after the Datalog rewriting has been performed, and the second one is considered after the unfolding to the UCQ has been attempted.
In both cases, the deletion of SCCs is done recursively, as deleting some SCC may enable the deletion of some other SCC.

For an intuitive explanation let us consider the deletion of a single clause.
If the clause cannot be accessed from other clauses, then the predicates in the body are purely extensional (they have no intensional definition).
If the clause is entailed by the EBox, then the extensional values for the head atom contain the extensional values for the body of the clause.
Considering the values for the atoms in the body of such a clause, none has an impact on the values for the head atom.
There are no intensional values (the SCC cannot be accessed) and the extensional ones are redundant with respect to the head atom (as implied by the EBox).
Therefore, the clause can be deleted safely, i.e. without losing answers to the queries.

We can extend the previous intuitive idea by induction.
If the considered clause $\clause_a$ is entailed by the EBox, but it can be accessed from some second clause $\clause_b$, then this second clause $\clause_b$ may imply some intensional values for the body atoms of $\clause_a$, and therefore also for the head atom of $\clause_a$.
However, if $\clause_b$ could be removed and no other clause accessed to $\clause_a$, then $\clause_a$ would not be accessed by any clause and could be removed as in the previous paragraph.
Consequently, we can apply this principle to sequences of clauses deleting removing finite sequences of them.
In the case of cycles (or strongly connected components), we can consider them as an infinite sequence of clauses at the intensional level.
In such a case, we have to remember that we are considering the extensional properties of these clauses.
In fact, cycles do not translate to infinite sequences of extensional implications, they translate to indefinitely long but necessarily finite sequences of implications at the extensional level.
Therefore, we can remove the clauses in such SCCs safely.

Note that theoretically the deletion of SCCs does not need to be recursive for the same reasons.
All SCCs with such characteristics can be removed in one step.
The reason to perform this deletion recursively is to keep the process simple, for implementation, execution, validation and explanation purposes.

The explanation for the case of DL axioms is analogous, considering the clause graph in such a case.


\subsection{Removal of predicates with empty extension}

We have already seen in chapter~\ref{sec:map} that the predicates that have an empty extension can be removed.
We have also seen that these optimisations have greater benefits the sooner they are applied, especially as seen in chapter~\ref{sec:opt}.
In this case we adapt the optimisation detailed in chapter~\ref{sec:map} for the removal of non-mapped predicates to apply it in the preprocessing stage added in chapter~\ref{sec:opt}.

In the preprocessing step the same resolution and optimisation that we have already seen for the Datalog programs in chapter~\ref{sec:map} can be applied to the FOL clauses.
The difference is that some clauses must be preserved.
More precisely, we can use these predicates that have an empty extension for the resolution.
After that, the clauses that contain in the body the predicates with an empty extension are subsumed by the clauses obtained through resolution, therefore they can be removed.
On the contrary, the clauses that contain these predicates in the head cannot be removed and must be preserved.
In chapter~\ref{sec:map} these clauses were removed after a reachability test.
However, it is not possible to perform a reachability test during the preprocessing, and therefore these clauses must remain.
For example, the user query may contain some of these predicates, in such a case these clauses would be used in the query rewriting.

The last difference is the possibility of cycles involving function symbols.
After the cycles have been detected, the same algorithms are applied over FOL as they would be applied in the case of Datalog.
In this case, we consider potentially cyclic recursion the same cases as in chapter~\ref{sec:map} and additionally all those cases in which the head of some clause involved in the recursion contains some function symbol.


\subsection{Deletion of extensionally subsumed clauses}

We have seen that removing subsumed clauses has a strong impact on the efficiency of the query rewriting process and its results (section~\ref{sec:sub}).
With the EBox we can detect clauses that are subsumed at an extensional level and remove them.
For this task, we will use the part of the EBox that only contains predicates with no intensional definition in the TBox.
This means that we will use the clauses in the EBox that do not contain any predicates in the head of any clause in the TBox.
Given some EBox $\cebox$, we will refer to this set of clauses as $\ext{\cebox}$.

If we can derive some clause $\clause_r$ using the clauses in the $\ext{\cebox}$ and some other clause $\clause$ in the TBox, then the clause $\clause_r$ is extensionally subsumed by $\clause$.
This extensional subsumption has the same practical implications as the regular subsumption, i.e. the values provided by this clause are contained in the values that its head has without it, either due to other clauses (as in regular subsumption) or due to the extensional values of the head predicate (in this case of extensional subsumption).
Therefore, the extensionally subsumed clause can be removed from the Datalog program as any other subsumed clause.
This operation is formalised in proposition~\ref{cor:subsumredo}, and applied in algorithm~\ref{alg:ealgoiii} at line~\ref{ln:replace}.

The entailment is checked by applying resolution.
The clauses in the purely extensional part of the EBox $\eebox$ are used as side premises.
The main premise for the resolution is either in the Datalog program or has been derived from a previous stage of the resolution.
In practice this is equivalent to first obtaining the closure from the $\eebox$ and then checking subsumption as espeficied in proposition~\ref{cor:subsumredo}.
The operations described here are more efficient than producing the closure of $\eebox$ due to the optimisations explained in chapter~\ref{sec:opt}.

Intuitively, we can see that for this kind of subsumption check we need to consider exclusively $\ext{\cebox}$ and not the full EBox.
The resolution derives clauses following containment relations that are only satisfied in the extensional level when using the EBox for resolution.
By imposing an empty intension, the properties that hold in the extensional level do also hold in the union of extensional and intensional values, i.e. absolutely.
In particular, this derivation will start from a clause and its head will never be unified, as it is an intensional predicate, not present in $\ext{\cebox}$.
The replacements that happen in the body due to resolution will lead to more specific clauses, thus if we derive some other clause $\clause_r$ that is in the Datalog program, then this clause $\clause_r$ can be removed.


\subsection{Extensional condensation of clauses}

Similarly to removing the clauses that are subsumed, we can remove the atoms that are subsumed.
This operation is similar to the clause condensation that we have seen in \ref{sec:backsum}, but in this case we will use the information in the EBox to remove the atoms that are redundant.
As in the previous optimisation, and for the same reasons, we will use the part of the EBox $\cebox$ that refers only to predicates that are exclusively extensional in the TBox, i.e. $\ext{\cebox}$.

In this case, we derive a clause $\clause_r$ from another clause $\clause$ and the extensional part of the EBox $\cebox$, with the property that $\clause_r$ subsumes some other clause $\clause_s$ in the Datalog program, where $\clause_s$ may be $\clause$ or not.
In such a case, the derived clause $\clause_r$ is extensionally implied, as in the previous case, which means that the values provided by this clause are redundant.
Therefore, the clause $\clause_r$ can be added to the Datalog program.
After the clause has been added, $\clause_s$ is subsumed by $\clause_r$, therefore, we can remove $\clause_s$.

This replacement of a clause $\clause_s$ with another clause $\clause_r$ that subsumes $\clause_s$ means replacing some clauses with more general ones, as for example those that have one less atom in the body.
This replacement is analogous to clause condensation as described in section~\ref{sec:backsum}, using relations of subsumption between atoms derived from the EBox.

This operation is formalised in proposition~\ref{cor:equivredo}, and applied in algorithm~\ref{alg:ealgoiii} at line~\ref{ln:remove}.
The operation is performed similarly to the previous, inside the same loop.
The only difference is that in this case we are not checking subsumption but equivalence.
To check equivalence, we check subsumption in both directions.

