
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\section{Introduction} \label{sec:introe}

As we have seen in section \ref{sec:ebox}, EBoxes provide metainformation about the extension of the predicates in the ontology, in particular about extensional containment relationships among predicates in the ontology.
This metainformation can be used to optimise the time required for the query rewriting process and its results (the size of the rewritings).
In this chapter we will see a set of optimisations that are enabled by using the information in an EBox.

As in the previous chapter, the optimisations that we perform in this case change again the OBDA general scenario that we have seen in figure \ref{fig:wholeprocessfig}.
In this case, the preprocessing stage of the previous chapter is preserved,
the dependency of the mappings for the rewriting is also present, with an intermediate step in the generation of the EBox.
Therefore the process in figure \ref{fig:wholeprocessfigiii} subsumes the ones that we have seen in previous chapters.


\begin{figure}[!htb]%
\centering
\begin{tikzpicture}[node distance = 25mm and 35mm ,  thick, every node/.style={transform shape}, auto, on grid=true]
\node [ontology] (ontology) {ontology};
\node [block, below= of ontology] (preprocessing) {preprocess};
\node [ontology, below= of preprocessing] (ebox) {EBox};
\node [cloud, right= of preprocessing] (clauses) {clauses};
\node [block, below= of clauses] (rewriting) {\textbf{\textsc{query rewriting}}};
\node [block, below= of ebox] (eboxgeneration) {EBox generation};
\node [cloud, right= of rewriting] (rewrittenquery) {rewritten query};
\node [doc, below= of rewriting] (mappings) {Mappings};
\node [cloud, right= of clauses] (query) {query};
\node [block, right= of mappings] (translation) {query translation};
\node [cloud, right= of translation] (translatedquery) {translated query};
\node [block, below= of translatedquery] (execution) {query execution};
\node [database, below= of execution] (database) {data source};
\node [cloud, below= of translation] (results) {results};
\node [block, below= of mappings] (translationres) {results translation};
\node [cloud, below= of translationres] (transresults) {translated results};
\path [line] (query) -- (rewriting);
\path [line, dashed] (ebox) -- (rewriting);
\path [line, dashed] (ebox) -- (preprocessing);
\path [line] (eboxgeneration) -- (ebox);
\path [line] (ontology) -- (preprocessing);
\path [line] (preprocessing) -- (clauses);
\path [line] (clauses) -- (rewriting);
\path [line] (rewriting) -- (rewrittenquery);
\path [line] (rewrittenquery) -- (translation);
\path [line] (translation) -- (translatedquery);
\path [line] (translatedquery) -- (execution);
\path [line] (execution) -- (database);
\path [line] (database) -- (execution);
\path [line] (execution) -- (results);
\path [line] (results) -- (translationres);
\path [line] (translationres) -- (transresults);
\path [line] (mappings) -- (translation);
\path [line, dashed] (mappings) -- (eboxgeneration);
\path [line] (mappings) -- (translationres);
\path [line, dashed] (mappings) -- (rewriting);
\path [line, dashed] (mappings) .. controls +(left:20mm) and +(right:20mm) .. (preprocessing);
\end{tikzpicture}
\caption[OBDA process with query rewriting in the presence of an EBox]{OBDA process with query rewriting, a preprocessing stage and alternatively the use of some mappings or an EBox.}%
\label{fig:wholeprocessfigiii}%
\end{figure}

As we will see, the optimisations presented here subsume the ones presented in chapter~\ref{sec:map}.
With the EBox we model how the extension of some predicates relates with some other predicates.
We can also model if the extension of some predicate $\predicate$ is empty for some OBDA system $\obdadef$, which we express as $\evalues{\obda}{\predicate} = \emptyset$ and can be modeled in the EBox as $\predicate \subclassof \bot$.
These predicates that have no extension can be treated in the same way as the predicates with no mappings in chapter \ref{sec:map}.

The optimisations that consider the EBox are independent from the ones presented in the previous chapter, i.e. their application is not necessary (it is optional).
In our case we will apply the optimisations from the previous chapter additionally to the optimisations that make use of an EBox.
We will see that there are several stages in the algorithm where resolution is applied.
Therefore the optimisations in the previous chapter are beneficial.
From a technical point of view, this chapter focuses on additional optimisations that use an EBox.

\begin{comment}

Ontology Based Data Access (OBDA) consists on superimposing a conceptual layer as a view to an underlying information system.
This layer abstracts away from how that information is maintained in the data layer and provides inference capabilities.
The capabilities of ontologies and underlying systems, for instance databases, are complemented and integrated into one system with OBDA \citep{calvanese2007-1}.

On the one hand users can pose queries that benefit from the expressiveness and inference provided by ontologies.
On the other hand underlying systems may be better suited to store large amounts of data (e.g. databases), obtain the information from other sources (e.g. mediators), query real time data from sensors, or may just want to be preserved as trustworthy legacy systems (e.g. banking).

OBDA has been used traditionally in information integration scenarios \citep{Wache2001,paton2000}
In these scenarios the use of a global schema for integration purposes favored the adoption of ontologies in the role of this global schema.
By using an ontology as a global schema the information integration is empowered
%\begin{comment}
, due to the reusable and linked nature associated with ontologies, paving the way for a global information integration scenario as Linked Data may be considered \citep{heath2011}.
Using an ontology for the global schema provides also 
%\end{comment}
providing additional query capabilities, allowing inferences on the query to provide richer answers, similarly to how deductive object-oriented databases work (as already pointed out in \citep{calvanese2007-1}).

Several logics have been used in OBDA with the most relevant ones for our current context briefly described in section \ref{sec:logics}.
OBDA has paid special attention to logics that are first-order rewritable \citep{calvanese2007,gottlob2011}.
$\mathcal{ELHIO^{\neg}}$ falls out of this expressiveness, but it still preserves the polynomial complexity in the rewriting \citep{perez-urbina2009}.
Despite of the polynomial complexity, the rewriting still requires longer times to be generated than in other systems.
Rewriting times and results (rewritten queries) can be improved with the EBox.
EBox stands for Extensional Box and is an additional axiom box that contains the ABox dependencies \citep{rodriguez-muro2011,rosati2012}.
Intuitively we can think that the EBox models axioms that are true in the ABox in extensional terms regardless of the TBox.
The rationale is that reducing redundancy in the rewritten queries (by eliminating \textit{de facto} subsumption according to some EBox) improves efficiency of these results and that the TBox can also be improved to lead to a reduced redundancy or no redundant results at all.
A number of these possible optimisations are explained in \citep{rodriguez-muro2012}. The EBox has been used further to successfully improve the rewriting process and its results in \citep{rosati2012} with $DL-Lite_\mathcal{A}$ expressiveness. Previous approaches show promising results to the application of this kind of knowledge to improve respectively the starting point and the results of query rewriting on $\mathcal{ELHIO}^\neg$.

In this section we focus on query rewriting on $\mathcal{ELHIO}^\neg$ ontologies and use an EBox to optimise their performance. Due to the additional expressiveness in $\mathcal{ELHIO}^\neg$, the challenges to address are greater than for a lesser expressiveness, but at the same time this expressiveness provides a greater room for improvement and opportunities to improve results in the state of the art either with the same expressiveness and without considering an EBox or considering an EBox for the query rewriting on less expressive logics.

%\begin{comment}
This paper will is structured as follows: in section \{background\} some background concepts are included to simplify the comprehension of the rest of the paper. In section \{state of the art\} the state of the art is briefly summarized. Section \{proposal\} contains the description of the current proposal. Finally, sections blank and blank contain respectively the evaluation of this proposal and conclusions drawn from the current work.
%\end{comment}

In our case we are handling the $\mathcal{ELHIO}^\neg$ expressiveness, however it is possible and convenient to extend this expressiveness with one additional type of axiom for the EBox. In this case we can include assertions of the form $p \sqsubseteq \bot$. These assertions can be obtained easily from the ABox and at least a subset of them can be obtained from the mappings, in this case if an ontology concept property $p$ is not mapped then the only individuals that we can obtain for it proceed from inference, we can say that $\upsilon^i(p) = \upsilon(p)$ and $\upsilon^e(p) = \emptyset$. As we will see, there is a special step in the proposed algorithm to deal with this kind of predicates.
\end{comment}

We have seen in chapter~\ref{sec:map} that we can use the information about the presence or absence of a mapping to improve the query rewriting at the present mappings.
We have also seen in chapter~\ref{sec:map} that we can consider mappings as complete or partial, stopping the reachability algorithm on the first mapping found or the last one.
So far, we have considered that the completeness of a mapping specification was a property of the whole specification, i.e. the complteness of a mapping specification could be guaranteed or otherwise it could not be assumed.
However, we can consider the completeness of mappings as well as other extensional inclusion dependencies between mapping assertions into more detail.
These dependencies are modeled as ABox dependencies \citep{rodriguez-muro2011}.
These dependencies can be expressed as DL axioms, and can be collected together into a new type of box, the so called EBox \citep{rosati2012}.
EBox assertions are written in the same syntactical form as TBox assertions.
In our case, we will use EBoxes that are expressed in $\elhio$.

Using an EBox provides additional information about the individuals (and thus answers) that can be provided by particular mapping assertions.
We extend the notion of {\ELHIO} EBoxes to include the information presented in chapter \ref{sec:map}, using the bottom entities to specify the absence of a mapping for some predicate.
Therefore, for the case of EBoxes we can consider some greater expressiveness that can be denoted as $\ELHIO_{\bot}$.

In $\ELHIO_{\bot}$, concept ($C$) and role ($R$) expressions are formed according to the following syntax (where $A$ denotes a concept name, $P$ denotes a role name, and $a$ denotes an individual name):
\[
\begin{array}{rcl}
C & ::= & A \mid C_1 \sqcap C_2 \mid \exists R. C \mid \{ a \} \\
R & ::= & P \mid P^-
\end{array}
\]
An $\ELHIO_{\bot}$ axiom is an expression of the form $C_1 \sqsubseteq C_2$, $B \sqsubseteq \bot$, $R_1\sqsubseteq R_2$ or $P \sqsubseteq \bot$ where $C_1,C_2$ are concept expressions and $R_1,R_2$ are role expressions.
As usual, $\bot$ denotes the bottom entity, i.e. \code{owl:Nothing}, \code{owl:bottomObjectProperty}, and \code{owl:bottomDataProperty} in the case of OWL2.
An $\ELHIO_{\bot}$ TBox $\tbox$ is a set of $\ELHIO_{\bot}$ axioms.
For all considerations in the remainder of this work there are no distinctions between $\ELHIO$ and $\ELHIO_{\bot}$ EBoxes, therefore we will refer to them simply as $\ELHIO$ EBoxes.

We have seen that the absence of a mapping for a predicate $\predicate$ can be modelled as $\predicate \sqsubseteq \bot$.
In addition, we can also consider axioms of the form $A_1 \sqsubseteq A_2$.
This means we do not need to specify a reachability algorithm globally for an ontology, the behaviour may vary from one predicate to another depending on these axioms.
We will see more examples and possibilities along the chapter.
In general, this information can be used to address more specifically the query rewriting to the set of mapping assertions that provide the entirety of the set of certain answers and reduce redundancy in the results.
By reducing this redundancy we expect to reduce the computational load in the system.
This means shorter rewriting times, in general, but especially shorter rewritten queries that can obtain all the certain answers more efficiently.

Once again, in this chapter we present the same sections.
We present the algorithm in section~\ref{sec:algoe}.
We present the optimisations implemented in this algorithm in section~\ref{sec:propos}.
We present the proofs for the correctness and completeness of these optimisations in section~\ref{sec:eeproofs}.
Finally, we present some conclusions in section~\ref{sec:conclusione}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
