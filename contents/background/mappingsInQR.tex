
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{OBDA mappings in query rewriting}\label{sec:ultraback}

As we have seen before, mappings are used for the translation of queries written according to an ontological model into queries written according to the model used to store and access data in the underlying data sources.
The intersection between the terms or constructs used in each type of model (e.g., concepts and their properties, and tables and their columns) normally corresponds to a small fraction of the set of terms in the ontology.
% I should remove the rest of this paragraph but I don't want to.
There are several reasons for this and they are schematically represented in figure \ref{fig:reasons}.
Ontologies in OBDA provide a richer (broader) terminology and a greater expressiveness.
This richer terminology means that users can pose queries more comfortably in several different ways
(e.g. a richer terminology allows using synonyms and providing multilingual access, so that users with different views can query the same underlying information).
A richer terminology means that one single concept can encapsulate a greater semantic load, summarising what would need several terms otherwise.
For instance, an ontology in a hospital can include the concept ``CancerChildPatient'' to refer to the patients that are in a certain age range and suffer from cancer, which may imply two joins in the underlying datasource.
This expressiveness means shorter and more compact queries in general.
Besides the richer terminology, there are some usual characteristics in ontologies that make them different from regular knowledge bases.
Among these characteristics are the generality of the knowledge captured by ontologies, the reusability of ontologies (\textit{a priori}) and the links between them (reusability \textit{a posteriori}).
Due to these characteristics ontologies have usually a broader scope than the application that is considered at a specific time, and thus a broader terminology.
As the terminology becomes broader, we will logically find in that terminology less terms mapped (in proportion) to the data sources.

\begin{figure}[!htb]\centering
	\begin{tikzpicture}%[mindmap]
		%\begin{scope}[
		%	every node/.style=concept, concept color=jmora!20, grow cyclic,
		%	level 1/.append style={level distance=4.5cm,sibling angle=120},
		%	level 2/.append style={level distance=3cm,sibling angle=60}
		%]
		\begin{scope}[font=\footnotesize,
			grow=right, level 1/.style={sibling distance=4.5em},
			level 2/.style={sibling distance=1.5em}, level distance=5cm
		]
		\node [root concept] {terminology mismatch} % root
			child { node {ease OBDA access}
				child { node {synonyms} }
				child { node {multilingual} }
				child { node {summarise} }
			}
			child { node {characteristics in ontologies}
				child { node {generality} }
				child { node {reusability} }
				child { node {links} }
			};
		\end{scope}
	\end{tikzpicture}
	\caption{Reasons for the lack of mappings for some terms.}\label{fig:reasons}
\end{figure}

\subsection{Mapping characterisation}\label{sec:mappingcharacterisation}

%Each one of the mappings in the set of mappings $\mappings$ maps a term --- in this case a concept or a property --- in the ontology with a query in the data source or --- vice versa depending on the type of mappings --- a query in the ontology with a term in the data source.

Mappings may be specified as global-as-view (GAV), local-as-view (LAV)~\citep{lenzerini2002}, or both global-and-local-as-view (GLAV)~\citep{friedman1999}.
In the case of GAV, the mappings can be considered to be a set of assertions of the form:

\[ m_A = (q_\tup{\data,A}(\tuple{x}) \leadsto A(f(\tuple{x}))) \]
\[ m_P = (q_\tup{\data,P}(\tuple{y}) \leadsto P(f_{1}(\tuple{y_1}), f_{2}(\tuple{y_2}))) \]

Where $\query_\tup{\data,A}$ and $\query_\tup{\data,P}$ are queries over the data source $\data$ to obtain the values necessary for the basic concept $A$ or the role predicate $P$ respectively, which belong to the ontology $\dtbox$, $\tuple{y_1} \cup \tuple{y_2} = \tuple{y}$ and $f$, $f_1$ and $f_2$ are the transformation functions that allow generating the values for the semantic upgrade that is performed when converting the information from the database to ontological instances.
In a LAV context, finding the terms in the ontology that are mapped is not as straightforward as in a GAV context, and LAV mappings require a slightly longer translation process.
LAV mappings are assertions of the form:

\[ m_\predicate = (\query_\tup{\dtbox,\predicate}(\tuple{x}) \leadsto p(f(\tuple{x}))) \]

Which means that for every $p_i$ mapped in the data source we will have a set of predicates in the body of $q_{\dtbox,P_i}$ which are the mapped predicates in the ontology for that query.
The combination of both types of mappings at the same time is possible, with GLAV mappings.
A single mapping assertion $\mapping$ can be considered GLAV if both head and body of $\mapping$ contain free variables, taking the form:

\[ m_\tup{\tuple{\predicate}, \tuple{A}, \tuple{P}} = \paren{\query_\tup{\data,\tup{\tuple{A}, \tuple{P}}}\paren{\tuple{x}, \tuple{y}} \leadsto \query_\tup{\dtbox,\tuple{\predicate}}\paren{\tuple{x}, \tuple{z}}}\]

This relates a query on the data source $\data$ to a query on the ontology $\dtbox$.
In any case the union of all mapped predicates is the set of predicates in the ontology that are mapped, and so in short we have that:

\[ ontologyPredicates(m_\predicate) = \left\{
    \begin{array}{ll}
      \set{\predicate} & : isGAV(m_\predicate) \land \predicate \in head(query(m_\predicate))  \\
      \set{\predicate_i} & : isLAV(m_\predicate) \; \forall \predicate_i \in body(query(m_\predicate)) \\
      \set{\predicate_i} & : isGLAV(m_\predicate) \; \forall \predicate_i \in head(query(m_\predicate)) \\
    \end{array}
  \right. \]
\[ mappedPredicates(\mappings) = \bigcup_{m_\predicate \in \mappings} ontologyPredicates(m_\predicate) \]

In all cases, a computationally simple syntactic processing of the mapping assertions is enough to obtain the ontology predicates that are mapped.
This information can be used for optimisation purposes, as detailed in chapter~\ref{sec:map}.
Intuitively, these optimisations work on the basis that rewritten queries that contain predicates that are not mapped are useless in a UCQ since they will lead to empty answers.
Therefore, such queries can be added or removed from the UCQ without impact on the certain answers.
The reason for this lack of impact is in the impossibility to translate these queries to obtain answers with the translated queries from the data source.
In fact, the lack of mappings for some predicate in the ontology will be unavoidably noticed in the translation process, which will have to abort the translation of that query.
We will see in chapter~\ref{sec:map} that it is possible to avoid the generation of such queries in much earlier stages in the query rewriting process.
This anticipation produces generally an improvement in the time needed for query rewriting, a shorter rewritten query that does not contain non-mapped predicates and a rewritten query that obtains the same certain answers from the data source.

%Thus, either by checking the predicates to which some mapping leads or the predicates that can be used in some mapping we obtain the set of predicates in the ontology that are mapped.
%We can deduce that the remainder of the set of predicates in the ontology is not mapped and we can use this set of predicates for optimisation purposes, as we will see in chapter~\ref{sec:map}.
%In this context the whole process is expanded to rewriting $\query$ with $\dtbox$ to $\query_\dtbox$ and then translating $\query_\dtbox$ into $\query_\tup{\dtbox,\mappings}$ by using these mappings that allow using the terminology in the data source.

%In our work we will deal with the problem of query rewriting using the ontology $\dtbox$ and not with the translation process performed with the mappings.
%However, given a set of mappings $\mappings$ we can find a set of rewritten queries $\query_{\dtbox}^{\prime}{\ldots}\query_{\dtbox}^{(n}$ such that for every $\query_{\dtbox}^{(i}$ the same translation is always obtained (in this case $\query_\tup{\dtbox,\mappings}$).
%Intuitively we can think of the opposite, we can think of adding conjunctive queries that cannot be translated to a UCQ.
%If we did so, the results would remain unaltered, because the added queries could not be translated.
%The queries that cannot be translated have no impact on the set of results obtained from a UCQ.
%Therefore, we can add them, and we can remove them as well.
%However, removing them has several advantages in the whole query rewriting process, as we will see in more detail in section \ref{sec:map}.
%In this case we need to consider that the concepts that appear in some mapping (\textit{mappedPredicates}) are the target for the rewriting, and any concept that does not participate in some mapping cannot be translated afterwards, and hence should be discarded. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
