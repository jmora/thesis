
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{OBDA-related logics}\label{sec:logics}
As aforementioned, OBDA-query rewriting has paid special attention to logics that are first-order rewritable \citep{calvanese2007,gottlob2011}.
In this section we mention those which have a special relevance for the related approaches in the state of the art, and summarise them in table \ref{tab:logicsyay}.
In the examples, $a$ refers to a constant (individual), $B_i$ refer to basic concepts (classes) and $R$ refers to roles (properties).


\begin{table}[!htb]
\rowcolors{2}{}{black!5}
\centering
\begin{tabular}{c c c c c c c c}
axiom\tablefootnote{here, $\forall i, j$, $B_i$ represents a basic concept and $R_j$ represents a role that may be basic or inverted.}
\textbackslash \rotatebox{\rotationdegrees}{logic}         & \rdllitecore&\rdllitef&\rdlliter&\rrapids&\relhion&\rdatalogpm&\rhornshiq\\
$B_1 \subclassof B_2, B_1 \subclassof \neg B_2$\estafa     & \yay        & \yay    & \yay    & \yay   & \yay   & \yay      & \yay     \\
$\geq 2 R_1 \subclassof \bot$                              & \nay        & \yay    & \nay    & \nay   & \nay   & \nay      & \yay     \\
$R_1 \subclassof R_2, R_1 \subclassof \neg R_2$            & \nay        & \nay    & \yay    & \yay   & \yay   & \yay      & \yay     \\
$B_1 \subclassof \exists R_1, \exists R_1 \subclassof B_1$ & \nay        & \nay    & \yay    & \yay   & \yay   & \yay      & \yay     \\
$B_1 \subclassof \exists R_1.B_2$                          & \nay        & \nay    & \nay    & \yay   & \yay   & \yay      & \yay     \\
$\exists R_1.B_1 \subclassof B_2$                          & \nay        & \nay    & \nay    & \nay   & \yay   & \nay      & \yay     \\
$B_1 \sqcap B_2 \subclassof B_3$                           & \nay        & \nay    & \nay    & \nay   & \yay   & \nay      & \yay     \\
$\{a\} \subclassof B, B \subclassof \{a\}, B(a)$           & \nay        & \nay    & \nay    & \nay   & \yay   & \nay      & \yay     \\
\textit{n-ary predicates}                                  & \nay        & \nay    & \nay    & \nay   & \nay   & \yay      & \nay     \\
$trans(R_1)$                                               & \nay        & \nay    & \nay    & \nay   & \nay   & \nay      & \yay     \\
\verylongforall                                            & \nay        & \nay    & \nay    & \nay   & \nay   & \nay      & \yay     \\
%faltan los funcionales y los de SHIQ y arreglar la tabla
% en el paper de 17 páginas Gottlob dice que los "negative constraints" no son un problema para la complejiad. Eso no significa que estén inluídos.
% Realmente Datalog\pm es una familia, habría que aclarar que miramos lo que hace nyaya 
\end{tabular}
\caption{Overview of the main logics in the state of the art.}\label{tab:logicsyay}
\end{table}


\begin{itemize}

\item{The $DL\mbox{-}Lite$ family is formed by $DL\mbox{-}Lite_{core}$ and its extensions, being the main ones $DL\mbox{-}Lite_{\mathcal{R}}$ and
$DL\mbox{-}Lite_{\mathcal{F}}$.
In $DL\mbox{-}Lite_{core}$ concept inclusions are
restricted to $B_1 \sqsubseteq B_2$ and $B_1 \sqsubseteq \neg B_2$.
$DL\mbox{-}Lite_{\mathcal{R}}$ includes subsumption (ISA) and disjointness assertions between roles and $DL\mbox{-}Lite_{\mathcal{F}}$ includes functionality restrictions on roles. These logics are first-order reducible with a tractable complexity \citep{calvanese2007}.}

\item{The $OWL2$ $QL$ profile was inspired by the $DL\mbox{-}Lite$ family and designed to keep the complexity of rewriting low, considering first-order rewritability. Among the main differences with $DL\mbox{-}Lite$ we can consider the
status of the unique name assumption (UNA).
UNA is adopted in DL-Lite but not in OWL,
which uses instead the explicit relations \code{sameAs} and \code{differentFrom} to describe that two symbols refer to the same or different entities, respectively.
OWL2 QL also lacks constructs that would conflict with
UNA and cause a greater complexity, like
number restrictions, functionality constraints and keys. Among the constructs in OWL 2 not supported in $DL\mbox{-}Lite$ we can remark nominals, that is concepts of the form $\{a\}$. For a more extensive comparison check \citep{artale2009}.}

\item{The $\mathcal{ELHIO}^\neg$ logic~\citep{perez-urbina2009} is more expressive than the previous ones.
It extends the expressiveness of $DL\mbox{-}Lite_\mathcal{R}$ by including basic concepts of the form $\set{a}$, $\top$, and $B_1 \sqcap B_2$,
as well as axioms of the form $\exists R.B_1 \sqsubseteq B_2$.
Depending on the query and the expressiveness in the ontology, rewriting to non-recursive Datalog may result in some loss of information,
thus some queries should be rewritten to recursive Datalog when considering $\mathcal{ELHIO}^\neg$
ontologies, what means that it is not first-order rewritable for languages like SQL.
In spite of that, the computational complexity of the rewriting process remains tractable (\textsc{PTime}-complete).}

\item{Some families in Datalog$\pm$ preserve the property of first-order rewritability to SQL equivalent languages while offering a greater expressiveness for rewritings to SQL or non-recursive Datalog, mainly because of the fact that Datalog$\pm$ predicates are n-ary.
Some of the Datalog paradigms that ensure decidability are chase termination, guardedness or stickiness, extended to weak-stickiness by Cal\`i et al. \citep{cali2010}.
Despite this greater expressiveness it should be noted that $\mathcal{ELHIO}^\neg$ is not covered by these Datalog$\pm$ families. For instance, some rewritings generate recursive Datalog, something that is avoided by these properties in Datalog$\pm$.}

\item{Finally, Horn-$\mathcal{SHIQ}$ includes the role hierarchies and inverse roles as $\mathcal{ELHIO}$. It does also include universal restrictions and transitive roles ($\mathcal{S}$) axioms of the form $A \sqsubseteq \forall R.B$ and $trans(R)$. This logic does also include qualified cardinality restrictions ($\mathcal{Q}$) axioms of the form $A \sqsubseteq \leq 1 R.B$. The Horn prefix refers to the Horn fragment of $\mathcal{SHIQ}$; this means that the axioms in this fragment can be converted to Horn clauses.}

\end{itemize}

\subsection{\elhio}\label{sec:backelhio}
For greater clarity, we specify {\ELHIO} into detail.
In $\ELHIO$, concept ($C$) and role ($R$) expressions are formed according to the following syntax (where $A$ denotes a concept name, $P$ denotes a role name, and $a$ denotes an individual name):
\[
\begin{array}{rcl}
C & ::= & A \mid C_1 \sqcap C_2 \mid \exists R. C \mid \{ a \} \\
R & ::= & P \mid P^-
\end{array}
\]
An $\ELHIO$ axiom is an expression of the form $C_1 \sqsubseteq C_2$ or $R_1\sqsubseteq R_2$ where $C_1,C_2$ are concept expressions and $R_1,R_2$ are role expressions.

Usually, the axioms in these logics will be converted to Datalog or in some cases FOL, for example in the case of $\elhio$ the correspondence between clauses and axioms would be the same as in table~\ref{tab:conversion} as described previously~\citep{perez-urbina2010} for the query rewriting system REQUIEM.

%\newcommand{\hputabviii}[8]{$#1$&$\qquad\leftarrow\qquad$&$#2,$&$#3$&$\qquad\leftarrow\qquad$&$#4$&$#5$&$\sqsubseteq$&$#6,$&$#7$&$\sqsubseteq$&$#8$\\}
\newcommand{\hputabvia}[6]{$#1$&$\qquad\qquad\leftarrow\qquad\qquad$&$#2,$&$#3$&$\qquad\qquad\leftarrow\qquad\qquad$&$#4$&&&&$#5$&$\qquad\qquad\sqsubseteq\qquad\qquad$&$#6$\\}
\newcommand{\hputabvib}[6]{$#1$&$\qquad\qquad\leftarrow\qquad\qquad$&$#2$&&&&$#3$&$\qquad\qquad\sqsubseteq\qquad\qquad$&$#4,$&$#5$&$\qquad\qquad\sqsubseteq\qquad\qquad$&$#6$\\}
\newcommand{\hputabiv}[4]{$#1$&$\qquad\qquad\leftarrow\qquad\qquad$&$#2$&&&&&&&$#3$&$\qquad\qquad\sqsubseteq\qquad\qquad$&$#4$\\}

\begin{table}%
\begin{center}
\setlength{\tabcolsep}{-1.8em}
\begin{tabular}{r c l r c l r c l r c l}
$A(a)$&&&&&&&&$A(a),\qquad$&$\{a\}$&$\qquad\qquad\sqsubseteq\qquad\qquad$&$A$ \\
$P(a, b)$&&&&&&&&&&$P(a, b)$&\\
\hputabiv{x{\approx}a}{A(x)}{A}{\set{a}}
\hputabiv{A_2(x)}{A_1(x)}{A_1}{A_2}
\hputabiv{A_3(x)}{A_1(x)\land A_2(x)}{A_1 \sqcap A_2}{A_3}
\hputabiv{P(x, f(x))}{A(x)}{A}{\osome{P}{}}
\hputabvia{P(x, f(x))}{A_1(x)}{{\enspace}A_2(f(x))}{A_1(x)}{A_1}{\osome{P}{A_2}}
\hputabiv{P(f(x),x)}{A}{A}{\osome{P^{-}}{}}
\hputabvia{P(f(x), x)}{A_1(x)}{{\enspace}A_2(f(x))}{A_1(x)}{A_1}{\osome{P^{-}}{A_2}}
\hputabiv{A(x)}{P(x, y)}{\osome{P}{}}{A}
\hputabiv{A_2(x)}{P(x, y), A_1(y)}{\osome{P}{A_1}}{A_2}
\hputabiv{A(x)}{P(y, x)}{\osome{P^{-}}{}}{A}
\hputabiv{A_2(x)}{P(y, x), A_1(y)}{\osome{P^{-}}{A_1}}{A_2}
\hputabvib{S(x, y)}{P(x,y)}{P}{S}{\inv{P}}{\inv{S}}
\hputabvib{S(x, y)}{P(y,x)}{P}{\inv{S}}{P}{\inv{S}}
\end{tabular}
\end{center}
\caption[Correspondence between {\ELHIO} axioms and FOL clauses]{Correspondence between {\elhio} axioms and FOL clauses. Note that the Skolem functions generated are local to each axiom.}
\label{tab:conversion}
\end{table}

Additionally to $\elhio$, we will refer in some cases to $\elhio^\neg$, as this is the logic usually mentioned for the REQUIEM system.
The difference between $\elhio$ and $\elhio^\neg$ are the negative inclusions, which are relevant for some operations, e.g. checking the consistency of a OBDA system.
However, these negative inclusions are not considered for query rewriting purposes neither in the REQUIEM system nor in the present work.
For practical considerations, in the current scope of query rewriting, there is no difference between both logics.
The $\elhio^\neg$ expressiveness is mentioned for coherence with the existing literature.


