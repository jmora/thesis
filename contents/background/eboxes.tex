
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{EBoxes and query rewriting}\label{sec:ebox}

Ontologies are usually decomposed into ABox (assertional box) and TBox (terminological box).
The former includes the assertions or facts, corresponding to the individuals, constants or values for the previously mentioned extensional predicates.
The latter describes the concepts or predicates in the ontology and how they relate among them with a set of axioms in description logics (DL).
These axioms can be converted to rules or implications (and viceversa) in first order logic (more expressive) and to some extent in Datalog (less expressive than unconstrained DL).

ABox dependencies~\citep{rodriguez-muro2011}, or extensional constraints~\citep{rosati2012}, are assertions that restrict the syntactic form of allowed or admissible ABoxes.
These assertions can be compiled into an extensional box (EBox), with potentially (and usually conveniently) the same expressiveness as the TBox.


For example we may have a system to manage the students in a university, and we may want to retrieve a list of all the students.
Consider for this system the following TBox:



\vspace{5pt}
\myadjustbox{%
\newcommand{\easytable}[4]{$#3$&$\quad\sqsubseteq\quad$&$#4$&$#1$&$\quad\sqsubseteq\quad$&$#2$\\}
\newcommand{\easyhtable}[2]{$#1$&$\quad\sqsubseteq\quad$&$#2$&&&\\}
  \setlength{\tabcolsep}{-4pt}
  \begin{tabular}{r r l r r l}
    \easytable{MasterStudent}{GradStudent}{UndergradStudent}{Student}
    \easytable{PhDStudent}{GradStudent}{IndustryMasterStudent}{MasterStudent}
    \easytable{GradStudent}{Student}{ResearchMasterStudent}{MasterStudent}
    \easyhtable{BachelorStudent}{UndergradStudent}
  \end{tabular}
}
\vspace{3pt}

The following EBox:

\vspace{5pt}
\myadjustbox{%
\newcommand{\easytable}[4]{$#1$&$\sqsubseteq$&$#2$&$#3$&$\sqsubseteq$&$#4$\\}
\newcommand{\easyhtable}[2]{$#1$&$\sqsubseteq$&$#2$&&&\\}
  \begin{tabular}{r r l  r r l}
    \easytable{IndustryMasterStudent}{GradStudent}{Student}{\bot}
    \easytable{ResearchMasterStudent}{GradStudent}{ \hspace{3pt} \code{    } \hspace{3pt} BachelorStudent}{\bot}
    \easytable{PhDStudent}{GradStudent}{MasterStudent}{\bot}
  \end{tabular}
}
\vspace{3pt}

And an ABox satisfying the previous EBox, for example an ABox with the following individuals:

  \begin{itemize}
    \setlength\itemsep{1pt}
    \item $UndergradStudent$: {\namei}
    \item $GradStudent$: {\nameii}, {\nameiii}, {\nameiv}, {\namev}
    \item $ResearchMasterStudent$: {\nameii}
    \item $IndustryMasterStudent$: {\nameiii}
    \item $PhdStudent$: {\nameiv}\\%, {\namevi}\\
  \end{itemize}
\vspace{-9pt}
Querying for the most general concept ($Student$) would yield no results.
Querying for the most specific concepts ($BachelorStudent$, $ResearchMasterStudent$, $IndustryMasterStudent$ and $PhdStudent$) requires four queries and yields an incomplete answer, missing {\namev} and {\namei} in the example.
Finally querying for all concepts would provide all answers, but that implies eight queries (one for each concept) and retrieving some duplicates.
In this case the duplicates are {\nameii}, {\nameiii} and {\nameiv}.
Duplicated answers have no impact on the correctness of the answer set, but they are a big burden in the efficiency of the process when considering more complex queries and ontologies.
In particular, in the example we only need three queries (as opposed to eight) to retrieve all answers, querying respectively for instances of $UndergradStudent$, $GradStudent$ and $IndustryMasterStudent$, since the EBox states that the ABox extension of every other concept is either empty or contained in $GradStudent$.
There are therefore six queries that are only a waste of computational resources in the query answering process.

A na\"ive algorithm may generate the perfect rewriting and then reduce it by checking for subsumption with the EBox.
However, such a na\"ive algorithm could have a prohibitive cost for large rewritings and would only be applicable over non-recursive rewritings.
In the following sections we will show that it is possible to face more complex scenarios and handle them better than with such a na\"ive algorithm.

This example illustrates that the combination of ABoxes that are already (partially) complete and a complete query rewriting on the TBox causes redundancy in the results, which is a burden for efficiency.
Hence, the characterization of ABox completeness as a set of dependencies can serve to optimise TBoxes, and create ABox repositories that appear to be complete \citep{rodriguez-muro2011}.
Additional optimisations can be done with the Datalog query before unfolding it into a UCQ, and finally with the UCQ, reducing redundancy at every step.
For instance, in our example we have in the EBox that $PhDStudent \sqsubseteq GradStudent$ just like in the TBox.
Therefore, we do not need to consider this axiom in the TBox when retrieving students: the ABox is complete in that sense and no $GradStudent$ needs to be obtained from $PhDStudent$.

Using the EBox, the perfect rewriting can be reduced along with the inference required for its generation.
We can redefine the perfect rewriting in the presence of EBoxes as follows \citep{rosati2012}: a perfect rewriting for a UCQ $\query$ and a TBox $\tbox$ under an EBox $\ebox$ is a query $\query^\prime$ such that, for every ABox $\abox$ that satisfies $\ebox$, $\cert{\query}{\OBDADL} = \cert{\query^\prime}{\tup{\emptyset,\abox}}$.

EBoxes can be manually described, or automatically extracted.
The automatic extraction can be done from an ABox (materialised or virtual), by describing the extensional containment relationships at a given time and updating the EBox if these relationships change.
Alternatively, EBoxes can be automatically extracted from the definition of an OBDA system, in particular from the mappings and the database schema, inferring from the implied intensional properties the constraints that the ABox will satisfy~\citep{console2013}.
Finally, combining several of the previous techniques for the production of EBoxes is technically feasible but that is a possibility not explored yet to the best of our knowledge.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

