
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{A general algorithm for query rewriting} \label{sec:genmethod}

In OBDA, when using the knowledge in some ontology to reformulate a query, one of the most common procedures is to convert both the query and the ontology to Datalog \citep{ceri1989}.
Then the rewriting process is done by performing inferences on the generated Datalog program, by iteratively applying deduction steps that lead from the original query to the rewritten query.
We can consider that the approaches mentioned in the previous section are derivations from a general algorithm, as represented in algorithm~\ref{fig:themostgeneral}.
The input for this general algorithm is an ontology and a query, and the output is the rewritten query.
We can see this algorithm simply as a succession of steps.

\begin{figure}[!htb]
\centering
\begin{tikzpicture}[->,>=stealth',node distance = 25mm and 35mm, auto, on grid=true]
    % Place nodes
		%\node [cloud] (ontology) {ontology};
    %\node [block, right of=ontology, node distance=5cm] (toDatalog) {convert to Datalog};
		%\node [block, below of=toDatalog, node distance=3cm] (reachability) {reachability test};
		%\node [cloud, left of=reachability, node distance=5cm] (query) {query};
    %\node [block, below of=reachability, node distance=3cm] (saturate) {rewrite into Datalog};
    %\node [cloud, left of=saturate, node distance=5cm] (Datalog) {Datalog};
		%\node [block, below of=saturate, node distance=3cm] (unfold) {unfold into UCQ};
		%\node [cloud, left of=unfold, node distance=5cm] (ucq) {UCQ};
    
		\node [cloud] (ontology) {ontology};
    \node [block, below of=ontology, node distance=2cm] (toDatalog) {convert to Datalog};		
		\node [cloud, right of=ontology, node distance=3.6cm] (query) {query};
    \node [block, below of=query, node distance=2cm] (reachability) {reachability test};
    \node [block, right of=reachability, node distance=3.6cm] (saturate) {rewrite into Datalog};
    \node [cloud, below of=saturate, node distance=2cm] (Datalog) {Datalog};
		\node [block, right of=saturate, node distance=3.6cm] (unfold) {unfold into UCQ};
		\node [cloud, below of=unfold, node distance=2cm] (ucq) {UCQ};
    
    % Draw edges
    \path [line] (ontology) -- (toDatalog);
    \path [line] (query) -- (reachability);
		\path [line] (toDatalog) -- (reachability);
		\path [line] (reachability) -- (saturate);
		\path [line] (saturate) -- (Datalog);
		\path [line] (saturate) -- (unfold);
		\path [line] (unfold) -- (ucq);
\end{tikzpicture}
\caption{Stages in the general algorithm}\label{fig:themostgeneral}
\end{figure}

\begin{enumerate}
\item The first step is \emph{parsing} the ontology from the serialization in which it is made available, for instance OWL in RDF/XML or turtle format.
After the ontology has been parsed it is converted into \emph{Datalog clauses}.
The part of the ontology that is out of the expressiveness handled by the algorithm at hand is usually ignored and discarded.
It must be noted that in the case where part of the ontology is discarded, the results from the query rewriting may be inconsistent with the ontology iff the ontology is inconsistent with the data source considered, otherwise the results may be incomplete but will preserve soundness.
\item The most general step is the \emph{rewriting} itself. This is performed through inferences on the Datalog obtained from the previous stage to obtain some different Datalog or a UCQ, depending on the case.
The inference is always based on the traditional resolution as can be done in Datalog, for instance backward chaining.
To this basic inference several modifications are made depending on the expressiveness handled, the input accepted and the output produced.
These modifications are done with the purpose of reducing the time needed for these inferences and obtaining better (usually shorter) queries from the rewriting process.
During this stage many subsumed and subsuming clauses can be produced, which may represent a problem in terms of efficiency.
Different systems cope with them in different ways, e.g. performing a subsumption check after resolution to remove all subsumed clauses.
\item The final step consists in the \emph{evaluation} of the produced query by the underlying systems.
The variability in this step is greater than in the other two.
Some systems may use the query in Datalog, some may convert it to SPARQL, SQL or other languages and pose it to the system storing the data or to another system for query translation with some mappings. In most approaches, this step is omitted from the descriptions provided in their corresponding papers, and the evaluation focuses on the properties of the Datalog query produced, the process for its production and possibly additional inputs that can be considered for further optimisation.
\end{enumerate}

Different algorithms make different modifications to this general schema.
\begin{itemize}
  \item In the case of REQUIEM:
  \begin{itemize}
    \item An additional stage is added between steps one and two in order to reduce the size of the ontology that is being used to only the part of the ontology that is reachable.
		This is because the inference done in the following stage is done through saturation of resolution with free selection instead of using backward chaining from the query.
		This means that all the clauses available at that stage are used in the inference among all the other clauses available and produced during that stage.
		Thus a reduction in the number of clauses provides an important reduction in the later inferences and processing.
    \item The second step is split into two different resolution steps. In the case of REQUIEM the ontology does not generate a Datalog program to unfold but a logic program (with functional terms). The first resolution step performs the inferences needed to convert the logic program into a Datalog program. The Datalog program is generated by performing the resolutions that lead to new clauses that do not contain the functional terms and dropping clauses that contain some functional term. The second resolution step unfolds the Datalog program into a UCQ if it is not recursive, otherwise it is impossible. This is considered a pay-as-you-go approach because depending on the expressiveness of the ontology different results can be obtained, either a Datalog program or a UCQ.
    \item between steps two and three some additional optimisations are performed to reduce the size of the query, as subsumption check and condensation of the produced queries.
  \end{itemize}
	\item In the case of Presto:
  \begin{itemize}
    \item Presto is the system in the comparison whose algorithm differs to a greater extent from the general algorithm previously described. The second step does not consist on inference but mainly on splitting the query into smaller fragments through the elimination of existential joins (EJ) when a most general subsumee (MGS) can be found. The inference is performed to obtain the MGS, while the subsumption checks and removals between clauses are implicit in the process to replace the atoms containing the EJ with those in the MGS.
  \end{itemize}
  \item In the case of Rapid:
  \begin{itemize}
    \item The second step can perform the inference in two different ways, either to generate a Datalog program or a UCQ. The difference is that in the case of the UCQ generation more inference rules are added to the resolution to remove functional terms and unfold the program.
		\item Rapid accepts a logic program which may contain functional terms, as REQUIEM does. The inferences that are possible with these functional terms are very reduced due to the limited presence of functional terms, therefore the inference is done with an additional rule that groups vertically several inferences to handle more efficiently clauses that contain functional terms. This is done with the step named ``query shrinking''.
		\item The third main difference in the resolution step for Rapid is how the generation of some subsumed clauses is prevented. This is done again by grouping several inference steps into one, but this time horizontally. The rule applied in this case is named ``query unfolding''. This groups inferences in two ways.
		First, all the atoms that can replace a given atom are grouped, this is possible due to the linear nature of the Datalog used.
		Second, all the sets of atoms that can replace atoms in a single query are grouped. With this, all the combinations of atoms in these sets can be generated according to the atoms present in some given query. The generation of some subsumed queries is avoided by checking subsumption between these sets of atoms before producing the combinations.
  \end{itemize}
	\item In the case of Nyaya:
	\begin{itemize}
	  \item The main difference in Nyaya consists on applying a stratified strategy to the inferences performed during the second step in a similar fashion to what Rapid does with the two resolution rules. In this case the names for the stages are ``factorization'' and ``rewriting''.% as we have seen in section \ref{sec:related}.
	\end{itemize}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

