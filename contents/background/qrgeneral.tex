
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Query rewriting in query answering} \label{sec:qrgeneral}

As discussed in the introduction, Ontology Based Data Access (OBDA) consists on superimposing a conceptual layer as a view to an underlying information system, which abstracts away from how that information is maintained in the data layer and provides inference capabilities \citep{calvanese2007-1}.

It should be noted that query rewriting is previous to OBDA and that the superimposition of a view to underlying information systems has been traditionally done in information integration scenarios.
These views may be defined as global as view or local as view rules (as we will see in more detail in section \ref{sec:ultraback}) or may be the intensional part of a deductive database in Datalog.
OBDA inherits aspects of both.

Among the classical approaches for data integration we can consider algorithms that use relational schemata.
These approaches are usually based on Datalog and two good examples are Razor \citep{friedman1997} and Internet Softbot \citep{etzioni1994}.
We can also find algorithms that use extensions of Datalog, such as the inverse rules algorithm used in Infomaster \citep{genesereth1997}.
In Information Manifold \citep{levy1996} some predicates in the rules may be concepts defined by using description logics constructors.
The ``query reformulation'' done with the bucket algorithm \citep{levy2000} considers the need of recursive Datalog queries to access sources that can only be accessed with certain patterns.
PICSEL with the language CARIN \citep{rousset2003} uses the description logic $\mathcal{ALN}$ in its description logic component, which is complemented by a rule component more similar to Datalog. At that time and with that state of the art several authors pointed at the use of a rich representation for the global (or mediated) schema by using description logics \citep{arens1996,catarci1993,levy1996-1,goasdoue1999-1}.
The way to use description logics at that time consisted primarily in combining DL axioms with Horn rules \citep{levy1998,donini1991,macgregor1994}.
The combination of Datalog or Horn rules with DL axioms provided a greater expressiveness but at the cost of a greater computational complexity as well \citep{cadoli1997}.

Other solutions took an object oriented approach. This is the case of TSIMMIS \citep{papakonstantinou1995,garcia-molina1997}, which uses the object-oriented language OEM for the description of the mediated schema and the views while the queries are represented with OEM-QL.
MOMIS \citep{beneventano2000} uses the DL ODL-I3 for the description of the schemata of the sources to integrate.

The use of objects, where the focus is set on the classes, objects, members and attributes, paved to some extent the way for a smooth transition to description logics, where the focus is set on concepts, individuals, roles and properties.
SIMS \citep{arens1993,arens1998} and OBSERVER \citep{mena2000} use description logics for the definition of the mediated schema, the views and the queries.
SIMS uses LOOM \citep{macgregor1987} while OBSERVER is based on CLASSIC \citep{borgida1989}.
In both latter systems the problem of query rewriting is handled as a planning problem.
This planning problem is solved by rewriting the query plan in the Planning-by-Rewriting approach \citep{ambite2000}.
This rewriting of the query plan should not be confused with the rewriting of the query that we will describe soon.

We can find a general classification of the types of ontology based data integration systems in \citep{Wache2001} as well as a general perspective of the problem.
We can see that the problem can be divided into several smaller problems, each of them addressed in one of the several steps used in the integration of the information and its access as a materialized or virtual ontology.
One of these problems is the so-called \textit{impedance mismatch}.
The impedance mismatch problem refers to the mismatch between the way in which data is (and can be) represented in a relational database, and the way in which the corresponding information is rendered in an ontology \citep{poggi2008}.
The semantic upgrade of data deals with this problem so that the integration can happen naturally in the semantic level.
At the semantic level, information integration approaches may allow the access to distributed or federated data sources \citep{quilitz2008,buil-aranda2013}. 
Additionally, at the semantic level, the reasoning corresponding to the rich semantics in description logics can vary substantially between approaches, for example by materializing the inferences \citep{kontchakov2010,rodriguez-muro2012} or by rewriting the queries, as we see next.

\begin{ocorchocut}
The process of query rewriting in OBDA systems consists in using ontologies to transform ontology-based queries (e.g., written in SPARQL or in ad-hoc query languages) into queries that the underlying data sources are able to process (e.g., written in SQL, Datalog, etc.).
This allows obtaining answers for the original queries as if they had been posed to an ontology that contains instances, where such instances are obtained from the data available in those data sources. 
\end{ocorchocut}
\begin{jmoracut}
OBDA allows users to access data using some interface based on ontologies, where the knowledge model of the user matches the one from the ontologies. The queries are processed by the OBDA system to obtain the answers in a way that is transparent to the user. This means that for the user the data that is being accessed seems to be part of the ontology.
\end{jmoracut}
Query rewriting gained attention due to the first-order rewritability property.
This property intuitively means that the queries posed to an OBDA system can be rewritten into a first order language (FOL), e.g. SQL.
These rewritten queries can then be used with systems that support those languages, e.g. relational databases.
We see next the formal definition for this property.

Given an ontology language $\mathcal{L}$ and a query language $\mathcal{Q}$ for a data source $\data$, we say $\mathcal{L}$ is \textbf{first-order rewritable} if any query $\query$ expressed over an ontology $\Sigma$ written in the ontology language $\mathcal{L}$ can be rewritten into a perfect rewriting \citep{calvanese2000} $\query_\Sigma$, written according to the query language $\mathcal{Q}$, such that when $\query_\Sigma$ is evaluated over the data source $\data$ the results obtained --- $\cert{\query_\dtbox}{\data}$ --- are the same as evaluating $\query$ over $\dtbox$ and $\data$ --- $\cert{\query}{\dtbox, \data}$. Due to this rewritability property it is possible to separate the complexity associated with the inference required to rewrite $\query_\dtbox$ from $\query$ and $\dtbox$ and the complexity derived from obtaining the answers to $\query_\dtbox$ from $\data$. Several description logic languages have been identified among the languages that present this property for SQL or equivalently expressive query languages. Among
the languages that present the first-order rewritability property, the ones with a special relevance in our context are: 
the $DL\mbox{-}Lite$ family \citep{calvanese2007}, which includes $DL\mbox{-}Lite_{core}$, $DL\mbox{-}Lite_{\mathcal{F}}$ and $DL\mbox{-}Lite_{\mathcal{R}}$; the $QL$ profile of $OWL2$ \citep{cuenca_grau2008}; and some families in Datalog$\pm$ \citep{cali2011}. These logics are detailed further in section \ref{sec:logics} along with the $\mathcal{ELHIO}^\neg$ family \citep{perez-urbina2009}, which is more expressive and not first order rewritable, but has been used in query rewriting. 

Nowadays, in addition to information integration scenarios, OBDA is interesting in a broader range of scenarios where its applicability has been boosted thanks to the tractability provided by the first-order language reducibility \citep{calvanese2007} or first-order rewritability property \citep{gottlob2011}, identified in some logic families. This property improves the capabilities of query answering through the use of query rewriting techniques \citep{calvanese2000}. In the present work we focus on query rewriting approaches and techniques, leaving aside previously described approaches that may in general take an approach closer to expert systems, with the knowledge provided by an ontology or a set of ontologies and a set of rules, or take a different approach to OBDA by focusing on providing access to documents, discovery of data sources, ontology population, etc.

We will not consider either other systems that may be more similar in the purpose and approach (e.g. using \textit{DL-Lite}) but that work on a different context, for instance using materialization approaches, either on the data~\citep{kontchakov2010} or the mappings~\citep{rodriguez-muro2012}.

We will focus on query rewriting without modifying the original data sources and mappings, and as we will see, we will focus on the description logic $\mathcal{ELHIO}$.
We will compare our approach to approaches using this logic or similar logics, under similar assumptions in terms of properties of the sources and the query language.
This description logic is not first order rewritable, but the computational complexity of the rewriting in this case is still tractable and it is possible to implement a ``pay-as-you-go'' approach~\citep{perez-urbina2009}.
This means that depending on the ontology and the query, if the rewriting does not need to be recursive, then a UCQ or a non-recursive Datalog program can be produced. Therefore we have that:


\begin{itemize}
	\item{for ontologies in usual FOL-reducible logics the output will be a non-recursive Datalog program or a UCQ, depending on the selection made by the user.}
	\item{for any rewriting that requires recursive Datalog, approaches that rely on a non-recursive first-order rewriting would produce an incomplete answer. In such cases, the rewritten query must be a recursive Datalog query.}
\end{itemize}


