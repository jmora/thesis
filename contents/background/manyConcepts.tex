
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Preliminaries}\label{sec:wtf}
\begin{jmoracut}
In the spirit of \citep{perez-urbina2009-1} we briefly introduce some necessary terminology on logic programs, specially Datalog programs, OBDA systems and resolution.
\end{jmoracut}

\begin{ocorchocut}
We briefly introduce some necessary terminology on logic programs (with a stress on Datalog), OBDA systems and resolution.
\end{ocorchocut}

Given $\fpreds$, $\ffs$ and $\fvals$ the finite or countable sets of respectively predicate, function and constant symbols, and $\fvars$ a countably infinite set of variables, where predicate and function symbols have some positive integer associated as their arity.
We denote a \textbf{first-order language} by $\flanguage$, and the language determined by $\fpreds$, $\ffs$, $\fvals$ and $\fvars$ as $\flanguage(\fpreds,\ffs,\fvals,\fvars)$, which may be omitted when $\fpreds$, $\ffs$, $\fvals$ and $\fvars$ are clear from the context.

The set of terms $\terms{\flanguage}$ is the smallest set such that (i) $\fvals \cup \fvars \subseteq \terms{\flanguage}$, and (ii) if $\func \in \ffs$ with arity $n$, and $\term_1,\dots,\term_n \in \terms{\flanguage}$ then $\func(\term_1,\dots,\term_n) \in \terms{\flanguage}$.
This latter type of terms are called functional terms.
We will consider only functional terms of arity 1 in the present work, since those are the only functional terms that we generate from axioms in an ontology.
Terms with no variables are ground terms and the set of all ground terms in $\terms{\flanguage}$ is the Herbrand Universe ($U_\flanguage$).

The set of atoms $\atoms{\flanguage}$ is the smallest set containing every expression $\Predicate(\term_1,\ldots,\term_n)$
where $P \in \fpreds$ with arity $n$ and $\term_1,\ldots,\term_n \in \terms{\flanguage}$. The Herbrand base $\base{\flanguage}$ is the set containing exactly every ground atom in $\atoms{\flanguage}$.
We will use $\pred{\atom}$ to refer to the predicate $\Predicate$ of an atom $\atom = \Predicate\paren{\seq{\term}{n}{,}}$.

A \textbf{Horn clause} is an expression of the form $\atom_0 \leftarrow \atom_1 \land \ldots \land \atom_n$, where $\atom_0$ is an atom called the head, the set of atoms $\atom_1, \ldots, \atom_n$ is the body of the clause and each $\atom_i$ is an atom. Variables in the head are called distinguished variables and all other variables are called nondistinguished.
For a clause $\clause$ we will refer to its body and its head with $\body{\clause}$ and $\head{\clause}$ respectively.
The set of predicates of a clause $\preds{\clause}$ is the set composed by the predicates of each of the atoms $\pred{\atom_i}$ for all atom $\atom_i$ in $\clause$ (in the head or body).
Nondistinguised variables that occur in the body at most once are called unbound variables, all other variables are called bound variables. Unbound variables may be denoted as the anonymous variable '\textunderscore'.
A Horn clause $\clause$ is safe if all distinguished variables occur in the body.
We consider only safe Horn clauses in our work, since we fully translate ontologies to safe Horn clauses.
Alternatively a Horn clause can be written as $\atom_0 \lor \neg \atom_1 \lor \ldots \lor \neg \atom_n$. A Horn clause $\clause_1$ 
%$\atom_0 \leftarrow \atom_1 \land \ldots \atom_n$
subsumes another clause $\clause_2$
%$\atom_0 \leftarrow B^{\prime}_1 \land \ldots B^{prime}_m$
if and only if there is some most general unifier (MGU) $\unifier{}$ of clauses $\clause_1$ and $\clause_2$ such that $\unifier{\clause_1} \subseteq \clause_2$.

A \textbf{logic program} LP is a finite set of Horn clauses. A logic program LP is considered a Datalog program if no functional terms are present in LP. In a logic program we distinguish two parts:
\begin{itemize}
\item{an \textbf{intensional} part, composed of Horn rules of the form $\atom_0 \leftarrow \atom_1 \land \ldots \land \atom_n$, which corresponds to a TBox (terminological box) in an ontological context.}
\item{an \textbf{extensional} part, composed of facts of the form $\atom$ (an atom), which corresponds to an ABox (assertional box) in an ontological context.}
\end{itemize}
Datalog distinguishes between intensional and extensional symbols.
This classifies the set of predicates that appear in the head of some clause in the intensional part and the set of predicates in the extensional.
It is sometimes assumed that both sets of predicates are disjoint.
We do not make that assumption here, we consider that some predicates may have an extensional and an intensional description.
%From a different perspective, we may consider the intensional part as tuple generating dependencies \citep{beeri1984}. This means that the tuples generated (the values for the head predicate in the clause) may overlap with the values obtained from the extensional description of the ontology. We will thus consider as intensional predicates those that appear in the head of some rule and as extensional predicates those that do not appear in the head of any rule, regardless of the extension of these predicates, which may or may not be empty for intensional and for extensional predicates.

%\subsection{Additional considerations with mappings}

%In this section we introduce some additional considerations that diverge slightly from the usual definitions in the state of the art due to the introduction of mappings in the definitions.
%More precisely, in our model for OBDA systems we will also consider \textbf{sets of mappings}.
A set of mappings $\mappings$ maps predicates\footnote{Please note that we will refer to concepts and properties in a TBox as ``predicates''.} in the ontology $\ontology$ (usually only a TBox $\tbox$) with data in the database $\data$, providing the extensional part for these predicates.
Thus an OBDA system $\obda$ is defined by a triple $\obdaotriple$ where:


\begin{itemize}
	\item The TBox $\tbox$ provides inference capabilities on the data.
  In the future we will use $\tbox$ to refer to the TBox as a set of ontological axioms in description logics and $\dtbox$ to refer to a set of Horn clauses in first order logic (FOL), as corresponding in table \ref{tab:conversion} (page \pageref{tab:conversion}).
  % Ideally both are semantically equivalent, but depending on the characteristics of the conversion some systems may loose some axioms.
	%in the conversion.
	The unary and binary predicates in $\dtbox$ will correspond to concepts and properties in $\tbox$, respectively.
	\item The data source $\data$ contains the data that is accessed with the OBDA system.
  The predicates in $\data$ will generally be different from those present in $\tbox$, for example they may have any arity.
  Both sets of predicates will be mapped with $\mappings$.
	\item Finally, $\mappings$ provides the mappings from the predicates in $\dtbox$ to the predicates in $\data$, possibly performing some transformations on the data.
  Mappings are characterised in section~\ref{sec:mappingcharacterisation}.
  Both the mappings $\mappings$ and the data $\data$ together form an ABox $\abox$.
  This ABox can be materialised or virtual, depending respectively on whether the ABox assertions are generated and stored (e.g. in a triplestore) or are simulated for query answering purposes.
\end{itemize}

In some other cases in the state of the art, an OBDA system $\obda$ is presented as an abstraction of an ontology $\ontology$, therefore composed of a TBox and an ABox as usual $\tup{\otbox, \oabox}$.
In the case of OBDA systems, the ABox $\oabox$ is virtual (i.e. $\vabox$) and composed by the data source $\data$ and the set of mappings $\mappings$, between the predicates in $\otbox$ and $\data$, i.e. $\vaboxdef$.
Both perspectives are equivalent in practice, therefore we can define the OBDA system $\obda$ as $\obdatriple,\:\tup{\dtbox, \vabox},\:\tup{\otbox, \vabox},$ or $\tup{\otbox, \vabox}$.
Where the differences between $\otbox$ and $\dtbox$ are syntactical, i.e. $\otbox$ is a set of terminological DL axioms and $\dtbox$ is the semantically equivalent set of Horn clauses.
In some cases, it will be relevant to consider the mappings, and the first perspective will be used.
In some other cases, usual techniques for ontology reasoning will be useful for OBDA systems, and descriptions using the last perspective will be more clear and conventional.
Therefore, we will use both characterisations interchangeably, as they are equivalent in all relevant aspects, and one or another may be preferable depending on the context.

A \textbf{conjunctive query}, $\conjunctivequery$ in this case, is a Horn clause where the head predicate $\pred{\head{\conjunctivequery}}$ does not clash with any predicate in the Datalog program that is to be queried.
A union of conjunctive queries (UCQ) is a set of conjunctive queries $\ucqquery$ such that every query in it has the same head predicate $\forall \query_i, \query_j \in \ucqquery. \pred{\head{\query_i}}= \pred{\head{\query_j}}$.
% We consider the head predicate in every query in a UCQ as the head predicate for the UCQ and denote it as $Q_h$.
% Summing up, we have $Q_h = h(Q) = h(q^c_i) \forall q^c_i \in Q$.
A Datalog query is the union of a given UCQ $\ucqquery$ with a given Datalog program $\dtbox$ defined on $\flanguage(\fpreds,\ffs,\fvals,\fvars)$.
In our case $\dtbox$ will be provided by the OBDA system $\obdatriple \simeq \obdaotriple$.
To avoid the previously mentioned clashes, we will consider that $\pred{\head{\ucqquery}} \notin \fpreds$.
Given a query $\query$ (either a conjunctive query $\conjunctivequery$ or a UCQ $\ucqquery$), we will denote the atom built with the predicate $\pred{\head{\query}}$ applied to the n-tuple of constants $\tuple{\constant}$ as $\qp{\tuple{\constant}}$ and equivalently as $\qp{\seq{\constant}{n}{,}}$.

Given a query $\query$ over an OBDA system $\obdadef$ the certain answers of $\query$ with respect to $\obda$, denoted as $\cert{\query}{\obda}$, is the set containing exactly every tuple $\tuple{\constant}$ of constants in $\obda$ such that $\dtbox \cup \mappings \cup \data \cup \query \models \qp{\tuple{\constant}}$.
We will use $\dtbox$ and $\query$ to obtain a new query\footnote{
In case all clauses in $\query_\dtbox$ have the same head predicate we will have a UCQ.} $\query_\dtbox$
such that $\query_\dtbox \cup \mappings \cup \data \models \qp{\tuple{\constant}}$, i.e. this new query obtains the same certain answers when evaluated on the OBDA system without the TBox.
This means that we will be interested in the transformations $\Delta$ that can be performed on $\dtbox \cup \query$ to derive $\dtbox \cup \query \overset{\Delta}{\leadsto} \query_\dtbox$ while preserving the satisfiability and thus the possible tuples $\tuple{\constant}$, the answers for the query $\query$ on the system $\obdadef$, so that $\cert{\query}{\obdatriple} = \cert{\query_\dtbox}{\tup{\mappings, \data}}$.
 % Even after combining the clauses in the ontology and the UCQ we can differentiate between query clauses, the ones with $Q_h$ as the predicate in the head, and ontology clauses, the remaining ones.\draft{puede parecer una tonter\'ia, pero esto lo vamos a usar, como todo lo dem\'as}

\subsection{Subsumption}\label{sec:backsum}

Especially noteworthy is the concept of subsumption, both among clauses and among atoms in a clause.


Given a clause $\atom_0 \leftarrow \seq{\atom}{n}{\land}$, an atom $\atom_{a}$ subsumes another atom $\atom_{b}$ if the predicate $\predicate_{a}$ in $\atom_{a}$ equals (in name and arity) to the predicate $\predicate_{b}$ in $\atom_{b}$ and there is a unification $\unifier{}$ from the free variables in $\atom_{a}$ to the terms in $\atom_{b}$ such that $\unifier{\atom_{a}} = \atom_{b}$.
From a semantic perspective, an atom $\atom_a$ subsumes another atom $\atom_{b}$ in a clause $\clause_i$ such that $\atom_a, \atom_b \in \body{\clause_i}$ if for a clause $\clause_j$ such that $\head{\clause_i} = \head{\clause_j}$, $\body{\clause_i} \backslash \set{\atom_a} = \body{\clause_j}$ and $\clause_j \models \clause_i$.
Intuitively, an atom subsumes another if the subsumed atom can be removed from the clause without making it more general.

Note that any term that is not a free variable (e.g. existential variables) cannot be unified and must remain unaltered.
When an atom $\atom_{a}$ subsumes some other atom $\atom_{b}$ in a conjunction (e.g. a clause body) the subsuming atom can be removed as the subsumed atom is more specific.
The operation of removing the subsuming atoms in a clause is usually named as \emph{clause condensation}.


A clause $\clause_a$ \emph{subsumes} some other clause $\clause_b$, denoted by $\clause_a \subsumes \clause_b$, if and only if $\clause_a \models \clause_b$. 

Clauses form disjunctions in Datalog programs, therefore, when a pair of subsuming and subsumed clauses is found, the clause that can be removed is the subsumed clause, which is more specific than the subsuming clause.


\subsection{Resolution}\label{sec:rerereresolution}

Resolution is a refutationally complete theorem proving method \citep{robinson1965,bachmair2001}, this means that a contradiction (i.e., the empty clause) can be deduced from any unsatisfiable set of clauses.
The search for a contradiction proceeds by saturating the given clause set, that is, systematically (and exhaustively) applying all inference rules.

In our work we use resolution to rewrite queries as sets of clauses by proving and adding new clauses to these sets.
If resolution is sound then we can guarantee, by the definition of soundness, that for any provable set of clauses $\clauses$ if $\dtbox \cup \query \entails \clauses$ then $\forall \tuple{\constant}.\paren{\dtbox \cup \query \cup \clauses \models \qp{\tuple{\constant}}} \iff \paren{\dtbox \cup \query \models \qp{\tuple{\constant}}}$.
If the resolution is complete then we can also guarantee, by the definition of completeness, that for any $\clauses^{(1)}, \ldots, \clauses^{(n)}$ derivations of $\dtbox \cup \query$ it holds that: $$\forall i \in \set{1,\ldots,n}. \paren{\clauses^{(i)} \models \qp{\tuple{\constant}}} \Rightarrow \paren{\clauses^{(i)} \entails \qp{\tuple{\constant}}}$$
As far as these transformations generate a set of $\clauses^{(i)}$ where every $\clauses^{(i)} \equiv \dtbox \cup \query$, by the definition of logical equivalence, we will have that $\clauses^{(i)} \models \qp{\tuple{\constant}}$.
This means that in the process of query rewriting we can also use any derivation that allows obtaining $\clauses^{(i+1)}$ from $\clauses^{(i)}$ such that $\clauses^{(i+1)} \equiv \clauses^{(i)}$.
In fact, for query answering purposes we can use any transformation $\delta$ such that $\clauses^{(i)} \overset{\delta}{\leadsto} \clauses^{(j)}$ if and only if it preserves the answers obtained $\cert{\query}{\clauses^{(i)}} = \cert{\query}{\clauses^{(j)}}$ for a given query $\query$, since this happens if and only if $\clauses^{(i)} \models \qp{\tuple{\constant}}$ and $\clauses^{(j)} \models \qp{\tuple{\constant}}$ for the same set of $\tuple{\constant}$ by the definition of certain answers, which happens (as previously explained) if and only if $\clauses^{(i)} \entails \qp{\tuple{\constant}}$ and $\clauses^{(j)} \entails \qp{\tuple{\constant}}$ for the same set of constant values $\tuple{\constant}$ for complete and correct resolution methods. % Specifically we will use deletion of subsumed clauses, which are deemed as redundant in our resolution.

In our case we will use resolution with free selection (RFS). The essence of resolution calculus can be described with two inference rules \citep{bachmair2001}:

\[\mbox{(Binary) Resolution: } \frac{\clause_C \lor \clause_A \hspace{15pt} \clause_D \lor \neg \clause_B}{(\clause_C \lor \clause_D)\mu} \]
\[\mbox{(Positive) Factoring: } \frac{\clause_C \lor \clause_A \lor \clause_B}{\unifier{\clause_C \lor \clause_A}} \]

Where $\unifier{}$ is in both cases the most general unifier (MGU) of atomic formulas $\clause_A$ and $\clause_B$. Both rules can be combined into one:

\[\mbox{Binary resolution with factoring: } \frac{\clause \lor \clause_A \lor \ldots \lor \clause_A \hspace{15pt} \clause_D \lor \neg \clause_A}{\unifier{\clause \lor \clause_D}} \]

As can be seen in resolution rules two atoms are unified, one on each clause. In the case of resolution with free selection, for two atoms to be unified they have to be selected by some selection function. A selection function for Horn clauses selects either the head of a clause or a non empty set of body atoms. For atoms to be selected they have to meet certain criteria. This allows prioritizing some inferences with respect to others, by selecting only some atoms, or types of atoms and giving a higher priority to the inferences that involve those atoms. Therefore resolution with Horn clauses takes the form:

\[\frac{\atom_A \leftarrow \atom_1 \land \ldots \land \atom_{i-1} \land \underline{\atom_i} \land \atom_{i+1} \land \ldots \land \atom_n \hspace{15pt} \underline{\clause} \leftarrow \atom_{D_1} \land \ldots \land \atom_{D_n}}{\unifier{\atom_A \leftarrow \atom_1 \land \ldots \land \atom_{i-1} \land \atom_{D_1} \land \ldots \land \atom_{D_n} \land \atom_{i+1} \land \ldots \land \atom_n}}\]

Where $\unifier{}$ is the MGU of atomic formulas $\atom_i$ and $\clause$ and the underlined atoms are the ones selected and resolved. The clause for which a body atom is selected is considered the main premise and the clause for which the head is selected is considered the side premise.

Resolution with free selection has been proved to be correct and complete for Horn clauses \citep{lynch1997}.


\subsection{Cycles and recursion}\label{sec:acycle}

A particularly relevant aspect to consider in query answering in description logics is the possibility of recursion and cycles~\citep{grau2012}.
This recursion and cycles may be statically detected and analysed using the dependency graph, described as follows.

A \emph{dependency graph} for a Datalog program has a vertex for each predicate $\predicate$ in the program and an edge from $\predicate_i$ to $\predicate_j$ for each clause containing $\predicate_i$ in the body and $\predicate_j$ in the head, for every $\predicate_i, \predicate_j$ in the Datalog program.
The dependency graph will be used for the static analysis of a Datalog program, for example to check the reachability of some predicate from another predicate.
A predicate $\predicate_i$ is \emph{reachable} from a predicate $\predicate_j$ if and only if there is an edge from $\predicate_i$ to $\predicate_j$ in the dependency graph, or there is some predicate $\predicate_k$ such that there is an edge from $\predicate_k$ to $\predicate_j$ in the dependency graph and $\predicate_i$ is reachable from $\predicate_k$.

This static analysis can reveal some infinite recursion in a process associated with the corresponding Datalog program.
One of the common tasks in query answering is the \emph{chase procedure}~\citep{johnson1975,maier1979}.
The chase procedure applies forward chaining resolution, i.e. given a set of clauses and a set of assertions, the bodies of the clauses are unified with the assertions producing new assertions.
It is easy to see that this can lead to an infinite process when the clauses contain existentially quantified variables in the head.

For query rewriting purposes we can restrict our attention to Datalog programs that do not contain existentially quantified variables nor constants in the in the head.
We can also focus on backward chaining resolution, as seen in section~\ref{sec:rerereresolution}, where clauses representing the query are unified among themselves, generating a rewritten query finally be unified with the assertions for query answering.
However, in this context infinite recursion is also possible.

A Datalog program is said to be \emph{recursive} when there is some cycle in its dependency graph, and nonrecursive otherwise. 
The recursion in a cycle $\cycle$ may lead to termination of the chase procedure or not.
In general, deciding the termination of the recursion in a cycle is an undecidable problem.

To cope with this problem, some \emph{acyclicity conditions} are often defined.
These conditions are usually sufficient but not necessary to ensure that there is no infinite recursion.
Given a cycle $\cycle$ in the dependency graph, one of such conditions is whether all the variables in the vertexes of $\cycle$ are distinguished, according to the edges of $\cycle$.
This means that for each edge in $\cycle$ it corresponds to a clause $\clause$ containing some predicate $\predicate$ in the body and all the variables to which $\predicate$ applies are distinguished in the head of $\clause$.
If all these variables are distinguished, then the recursion terminates, i.e. the recursion is \emph{safe}.
If there is some variable that is not distinguished, then there may be an infinite recursion, i.e. the recursion is not safe.

An example of safe recursion can be created with the axioms $P \sqsubseteq S$ and $S \sqsubseteq P^-$, being $S$ and $P$ two object properties.
These two axioms generate the following two Datalog clauses: \dlc{S(x,y)}{P(x,y)} and \dlc{P(x,y)}{S(y,x)}.
Here we can see that there are two variables \code{x} and \code{y} that are distinguished in both edges, i.e. for all the clauses in which they appear in the body, they are also in the head.
Therefore, the recursion is safe.

Given an object property $P$ and a concept $A$, an example of recursion that is not safe is created with the axiom $\exists P.A\sqsubseteq A$.
This axiom translates to the clause \dlc{A(x)}{P(x,y),\dls A(y)}.
Here we can see that a single clause is enough to cause recursion and that the edge that is generated from and to this clause through \code{A} contains variables that are not distinguished (exactly \code{y}).
For this reason, the recursion is not safe.
In this particular example an infinite set of clauses can be derived taking the form of \dlc{A(x)}{P(x,y),\dls P(y,z),\dls A(z)}, then \dlc{A(x)}{P(x,y),\dls P(y,z),\dls P(z,v),\dls A(v)}, and so on.

%We will also consider sequences of clauses, sequences of clauses through some type of predicates (e.g. auxiliary predicates) and sequences of clauses that form loops.
%A sequence of clauses is a list of clauses $\clause_1, \ldots, \clause_n$ such that for every pair $(\clause_i, \clause_{i+1})$ there is some predicate $\predicate$ such that $\predicate \in \body{\clause_i}, \predicate \in \head{\clause_{i+1}}$.
%If the sequence is through some type of predicates $Type$ (e.g. auxiliary predicates) then it is a list of clauses $\clause_1, \ldots, \clause_n$ such that for every pair $(\clause_i, \clause_{i+1})$ there is some predicate $p:Type$ ($p$ is of type $Type$) such that $p \in body(\clause_i), p \in head(\clause_{i+1})$.
%A loop of clauses is a sequence of clauses $\clause_1, \ldots, \clause_n$ such that there is some $r$ such that $r \in body(\clause_n), r \in head(\clause_1)$.
%Finally a loops of clauses through some type of predicates $Type$ is a sequence of clauses $\clause_1, \ldots, \clause_n$ through the type of predicates $Type$ such that there is some $r:Type$ such that $r \in body(\clause_n), r \in head(\clause_1)$.


