
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The REQUIEM Algorithm} \label{sec:rqrmethod}

More precisely, we take as the starting point the general OBDA query rewriting algorithm in REQUIEM (RQR), due to the decisions taken in several aspects, including engineering aspects (e.g. modularity) and scientific aspects (e.g. expressiveness), political aspects (e.g. open source), and legal aspects (e.g. license for the code).
RQR is structured in several resolution steps.
Every resolution step follows the general algorithm for resolution (algorithm \ref{alg:rresol}).
We reproduce it here as originally specified~\citep{perez-urbina2010}.

\begin{algorithm}
\caption{REQUIEM resolution algorithm (\code{RQR})}
\label{alg:rresol}
\DontPrintSemicolon
\KwIn{Conjunctive query $Q$, DL-Lite$_{_R}$ ontology $\mathcal{O}$ }
$R = \Xi(\mathcal{O}) \cup \{Q\}$\;
\Repeat{no query unique up to variable renaming can be added to R}{
  (saturation) \ForAll{$clauses$ $C_1, C_2$ $in$ $R$}{$R = R$ $\cup$ \FuncSty{resolve}$(C_1, C_2)$}
}
$Q_\mathcal{O} = \{C\mbox{ }\mid\mbox{ } C \in \mbox{ \FuncSty{unfold}}(\FuncSty{ff}(R)), \mbox{ and }C\mbox{ has the same head predicate as } Q\}$
\Return $Q_\mathcal{O}$
\end{algorithm}

As we can see, there is a first stage (\code{resolve}) that generates the Datalog program by resolving the present clauses up to saturation, and a second stage (\code{unfold}) that unfolds this Datalog program into a UCQ. % We refer to the reader to the original paper (\citep{perez-urbina2010}) for a more detailed description.

This algorithm in REQUIEM can be specified in pseudocode with a greater level of detail, as in algorithm~\ref{alg:grresol}.
This description is more detailed and similar to the algorithms that we will show in following chapters.

{\begin{algorithm}[htpb]
\caption{General REQUIEM algorithm}
\label{alg:grresol}
\DontPrintSemicolon
\KwIn{Conjunctive query $Q$, DL-Lite$_{_R}$ ontology $\mathcal{O}$, working mode $mode$}
\KwOut{Rewritten query $q_\Sigma$}
$R = \Xi(\mathcal{O}) \cup \{Q\}$\;
$R = reachable(\codebox{'Q'}, R)$\;
$q_\Sigma = \codebox{requiemSaturate}(R, sfRQR, mode)$\;
\uIf{$mode = \codebox{greedy}$}{
	$predicates = IDBpredicates(q_\Sigma)$\;
  \For{$p \in predicates$}{
		$q_\Sigma = \codebox{requiemSaturate}(q_\Sigma, sfUnfoldGreedy(p), \codebox{\textit{greedy}})$\;
  }
}
\Else{
	$q_\Sigma = \codebox{requiemSaturate}(q_\Sigma, sfUnfoldNaive, \codebox{\textit{na\"ive}})$\;
}
\If{$mode \neq \codebox{na\"ive}$}{
	$q_\Sigma = subsumptionCheck(pruneAUX(reachable(\codebox{'Q'}, q_\Sigma)))$\;
	$q_\Sigma = \codebox{map}(condensate, q_\Sigma)$\;
}
\Return $q_\Sigma$\;
\end{algorithm}}


{\begin{algorithm}[htpb]
\caption{REQUIEM saturation algorithm (\code{requiemSaturate})}
\label{alg:rsat}
\DontPrintSemicolon
\KwIn{input program $P$, selection function $sf$, working mode $mode$}
\KwOut{saturated program}
$unprocessed = P$\;
$workedOff = \emptyset$\;
\While{$unprocessed \neq \emptyset$}{
	$currentClause = unprocessed.pop()$\;
	$sf.select(currentClause)$\;
	$workedOff.add(currentClause)$\;
	\For{$previousClause \in workedOff$}{
		\For{$\clause \in resolve(currentClause, previousClause)$}{
			\If{$\neg isRedundant(\clause, \codebox{mode}, workedOff \cup unprocessed)$}{
				$unprocessed.add(\clause)$
			}
		}
	}
}
\Return{$\codebox{filter}(sf.prune, workedOff)$\;}
\end{algorithm}}

{\begin{algorithm}[htpb]
\caption{REQUIEM redundancy detection algorithm (\code{isRedundant})}
\label{alg:rred}
\DontPrintSemicolon
\KwIn{clause $clause$, working mode $mode$, set of clauses $P$}
\KwOut{true or false}
\uIf{$mode = \codebox{na\"ive}$}{
	\Return{$\exists \clause \in P.equivalent(clause, \clause)$}
}
\Else{
	\Return{$\exists \clause \in P. \clause \subsumes clause$}
}
\end{algorithm}}

{\begin{algorithm}[htpb]
\caption{REQUIEM reachability algorithm (\code{reachable})}
\label{alg:rrea}
\DontPrintSemicolon
\KwIn{Root predicate $Q$, set of clauses $P$}
\KwOut{set of clauses $P$}
$pending = \{Q\}$\;
$reachable = \emptyset$\;
\While{$pending \neq \emptyset$}{
  $pred = pending.pop()$\;
	\If{$pred \notin reachable$}{
		$reachable.add(pred)$\;
		$pending = pending \cup \{p \in P \mid \exists \clause \in P. pred \in head(\clause) \land p \in body(\clause)\}$\;
	}
}
\Return{$\{c \in P \mid head(\clause) \in reachable\}$}
\end{algorithm}}

It is worth mentioning that REQUIEM accepts \textit{DL-Lite$_{_R}$} for the production of a rewriting that is a UCQ as stated in algorithm \ref{alg:grresol}. However $\mathcal{ELHIO}^{\neg}$ ontologies are accepted with the exception that if the Datalog produced is recursive, the recursion will be preserved after the unfolding, and thus the result will be a (linear and recursive) Datalog program instead a UCQ.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

